TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

INCLUDEPATH += $$PWD/../../AnalyticalTools
DEPENDPATH += $$PWD/../../AnalyticalTools

CONFIG(release, debug|release) {
    macx: LIBS += -L$$PWD/../../AnalyticalTools/Release/ -lAnalyticalTools
    macx: PRE_TARGETDEPS += $$PWD/../../AnalyticalTools/Release/libAnalyticalTools.a
}

CONFIG(debug, debug|release) {
    macx: LIBS += -L$$PWD/../../AnalyticalTools/Debug/ -lAnalyticalTools
    macx: PRE_TARGETDEPS += $$PWD/../../AnalyticalTools/Debug/libAnalyticalTools.a
}

LIBS += -lboost_program_options -lopencv_core
QMAKE_CXXFLAGS += -std=c++11
