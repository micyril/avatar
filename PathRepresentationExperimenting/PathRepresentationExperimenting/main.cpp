#include <iostream>
#include <map>
#include <boost/assign/list_of.hpp>
#include <AnalyticalTools/FilePathRepresentation/Tools/RepresentationMakers/RepresentationMakers.h>
#include <AnalyticalTools/FilePathRepresentation/Experimenting/Drivers/ExperimentDriver.h>
#include <AnalyticalTools/FilePathRepresentation/Experimenting/Drivers/TestDriver.h>
#include <AnalyticalTools/FilePathRepresentation/Experimenting/ErrorCalculators/ErrorCalculators.h>
#include <AnalyticalTools/FilePathRepresentation/RepresentationProvider/RepresentationProviders.h>

using namespace std;
using namespace boost::program_options;
using namespace RepresentationProvider;

options_description getGenericOptionsDescriptions()
{
    options_description genericOptionsDescription("Generic options");
    genericOptionsDescription.add_options()
            ("help", "Produce help message")
            ("method,M", value<string>()->required(), "Used method\n"
                                                      "Values:\n"
                                                      "  BAM: \tRepresentation based on Bidirectorial Associative Memory")
            ("output-format,F", value<string>()->required(), "The way in which output will be provided\n"
                                                             "Actually, you need provide such string: \"ect1:ect2:...:ectN\", "
                                                             "where \"ect\" means error calculator's type. The result of execution of "
                                                             "this program will be a string like \"mean1 sd1 mean2 sd2 ... meanN sdN\", "
                                                             "where \"mean\" and \"sd\" are mean and standart deviation calculated with an"
                                                             "appropriate error calculator\n"
                                                             "Allowed error calculator's types:\n"
                                                             "  BEC: \tstands for bits error calculator - calculating how much bits of binary "
                                                             "representations are not correlative\n"
                                                             "  LEC: \tstands for length error calculator - calculating length difference "
                                                             "between expected and actual strings\n"
                                                             "  LDC: \tstands for Levenshtein distance calculator - calculating Levenshtein distance between bits representations "
                                                             "of strings\n"
                                                             "  BIEC: \tstands for bits identification error calculator - calculating if expected string fails to be identified"
                                                             "with bits error calculator\n"
                                                             "  LIEC: \tstands for Levenshtein identification error calculator - calculating if expected string fails to be identified"
                                                             "with Levenshtein distance calculator\n"
                                                             "  EEC: \tstands for equal error calculator - returning 0 if strings are equal and 1 otherwise")
            ("teaching-samples,S", value<string>()->required(), "Path to a file containing samples used for teaching")
            ("testing-samples,T", value<string>()->required(), "Path to a file containing samples used for testing");
    return genericOptionsDescription;
}

options_description getBAMOptionsDescriptions()
{
    options_description bamOptionsDescription("BAM options");
    bamOptionsDescription.add_options()
            ("repr-maker", value<string>()->default_value("boost"), "Representation maker, used to represent teaching samples\n"
                                                                    "Values:\n"
                                                                    "  boost: \thash maker based on hash function from the popular library\n"
                                                                    "  srm: \tstands for simple representation maker, makes simple representation with binary view"
                                                                    "like 0...010...0, where place of \"1\" indicates number of string\n"
                                                                    "  rrm: \tstands for random representation maker, makes random ascii string of desirable size"
                                                                    )
            ("orig-size", value<int>()->default_value(-1), "Max size of strings in bytes, for which representations are built, by default it is the max size of the samples")
            ("repr-size", value<int>()->default_value(4), "Desirable size of representations in bytes");
    return bamOptionsDescription;
}

void printResults(const vector<TestingResult> &testingResults)
{
    if(testingResults.size() == 0)
        return;
    cout << testingResults[0].meanError << " " << testingResults[0].standardDeviation;
    for(int i = 1; i < testingResults.size(); i++)
        cout << " " << testingResults[i].meanError << " " << testingResults[i].standardDeviation;
    cout << endl;
}

int main(int ac, char **av)
{
    map< string, shared_ptr< ICreater<Tools::IRepresentationMaker> > > representationMakersMap =
            boost::assign::map_list_of("boost", shared_ptr< ICreater<Tools::IRepresentationMaker> >(new Tools::BoostHashMakerCreater()))
                                      ("srm", shared_ptr< ICreater<Tools::IRepresentationMaker> >(new Tools::SimpleRepresentationMakerCreater()))
                                      ("rrm", shared_ptr< ICreater<Tools::IRepresentationMaker> >(new Tools::RandomRepresentationMakerCreater()));
    map< string, shared_ptr< ICreater<IRepresentationProvider> > > representationProviderCreatorsMap =
            boost::assign::map_list_of("BAM", shared_ptr< ICreater<IRepresentationProvider> >(new BAMRepresentationProviderCreater(&representationMakersMap)));
    map< string, shared_ptr< ICreater<IErrorCalculator> > > errorCalculatorsCreatorsMap =
            boost::assign::map_list_of("BEC", shared_ptr< ICreater<IErrorCalculator> >(new BitsErrorCalculatorCreater()))
                                      ("LEC", shared_ptr< ICreater<IErrorCalculator> >(new LengthErrorCalculatorCreater()))
                                      ("LDC", shared_ptr< ICreater<IErrorCalculator> >(new LevenshteinDistanceCalculatorCreater()))
                                      ("EEC", shared_ptr< ICreater<IErrorCalculator> >(new EqualErrorCalculatorCreater()));
    errorCalculatorsCreatorsMap.insert(make_pair("BIEC",
                                                 shared_ptr< ICreater<IErrorCalculator> >(new IdentificationErrorCalculatorCreater(errorCalculatorsCreatorsMap["BEC"].get()))));
    errorCalculatorsCreatorsMap.insert(make_pair("LIEC",
                                                 shared_ptr< ICreater<IErrorCalculator> >(new IdentificationErrorCalculatorCreater(errorCalculatorsCreatorsMap["LDC"].get()))));


    options_description genericOptionsDescription = getGenericOptionsDescriptions();
    options_description bamOptionsDescription = getBAMOptionsDescriptions();
    options_description visibleOptionsDescription("Allowed options");
    visibleOptionsDescription.add(genericOptionsDescription)
                             .add(bamOptionsDescription);

    variables_map vm;
    try
    {
//        auto strs = split_unix(string("PathRepresentationExperimenting/Release/PathRepresentationExperimenting -M BAM -F BEC:LEC:BIEC -S ../../samples/teachingSamples.txt -T ../../samples/testingSamples.txt --repr-maker srm"));
//        const char* argv[strs.size()];
//        for(int i = 0; i < strs.size(); i++)
//            argv[i] = strs[i].c_str();
//        store(parse_command_line(strs.size(), argv, visibleOptionsDescription), vm);
        store(parse_command_line(ac, av, visibleOptionsDescription), vm);
    }
    catch(boost::exception_detail::clone_impl<boost::exception_detail::error_info_injector<boost::program_options::invalid_option_value> > ex)
    {
        cout << "The specified value for the option \"" << ex.get_option_name() << "\" is invalid. For help use the option --help." << endl;
        return 1;
    }

    if (vm.count("help")) {
        cout << visibleOptionsDescription << endl;
        return 0;
    }

    try
    {
        notify(vm);
    }
    catch(boost::exception_detail::clone_impl<boost::exception_detail::error_info_injector<boost::program_options::required_option> > ex)
    {
        cout << "The option \"" << ex.get_option_name() << "\" is required, please specify it in your command line. For help use the option --help." << endl;
        return 1;
    }
    TestDriver testDriver;
    ExperimentDriver<TestDriver> experimentDriver(&testDriver, &representationProviderCreatorsMap, &errorCalculatorsCreatorsMap);
    try
    {
        vector<TestingResult> results = experimentDriver.Start(vm);
        printResults(results);
    }
    catch(exception &ex)
    {
        cerr << ex.what() << endl;
    }
}

