#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <UserEventsHandler/Core/Tools.h>
#include <UserEventsHandler/Core/TaskPerformer.h>

class MockTask : public ITask
{
public:
    MOCK_METHOD0(Perform, void());
};

class TaskPerformerTest : public ::testing::Test
{
private:
    const unsigned long millisecondsWaitingStep = 50;
protected:
    TaskPerformer taskPerfomer;

    bool WaitWhileTaskNotPerformedExists(const unsigned long millisecondsMax)
    {
        unsigned long waitedTime = millisecondsWaitingStep;
        while(taskPerfomer.GetNotExecutedTasksCount() > 0 && waitedTime <= millisecondsMax)
        {
            Tools::sleepMilliseconds(millisecondsWaitingStep);
            waitedTime += millisecondsWaitingStep;
        }
        return taskPerfomer.GetNotExecutedTasksCount() == 0;
    }
};

TEST_F(TaskPerformerTest, StartTest)
{
    int tasksCount = 1000;
    ITask **tasks = new ITask*[tasksCount];
    for(int i = 0; i < tasksCount; i++)
    {
        tasks[i] = new MockTask();
        EXPECT_CALL(*((MockTask*)tasks[i]), Perform()).Times(1);
        taskPerfomer.AddTask(tasks[i]);
    }
    ASSERT_TRUE(WaitWhileTaskNotPerformedExists(2500));
}
