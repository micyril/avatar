#include <gtest/gtest.h>
#include <UserEventsHandler/Core/Tools.h>

class ToolsTest : public ::testing::Test { };

TEST_F(ToolsTest, isRelationTrueTest)
{
    std::string path1 = "/path1";
    std::string path2 = "/path1/path2";
    std::string path3 = "/path1path2";
    std::string path4 = "/path3";

    ASSERT_FALSE(Tools::isRelationTrue(path1, path1));
    ASSERT_FALSE(Tools::isRelationTrue(path2, path1));
    ASSERT_FALSE(Tools::isRelationTrue(path1, path3));
    ASSERT_FALSE(Tools::isRelationTrue(path4, path2));
    ASSERT_TRUE(Tools::isRelationTrue(path1, path2));
}
