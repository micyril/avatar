#include <QDir>
#include <iostream>
#include <list>
#include <gtest/gtest.h>
#include <UserEventsHandler/FileSystem/FileTreeStorage.h>
#include <Stuff/FileSystem.h>

class FileTreeStorageTest : public ::testing::Test
{
protected:
    std::string testDirPath;
    FileTreeStorage *fileTreeStorage;
    std::string files[6];

    FileTreeStorageTest() : testDirPath(QDir::currentPath().toStdString() + "/testdir")
    {
        if(!FileSystem::Exists(testDirPath))
            FileSystem::MakeDir(testDirPath);

        files[0] = testDirPath;
        files[1] = testDirPath + "/subdir";
        files[2] = testDirPath + "/subdir/.hiddenSubdir";
        files[3] = testDirPath + "/file1";
        files[4] = testDirPath + "/.hiddenFile";
        files[5] = testDirPath + "/subdir/file3";
    }

    virtual void SetUp()
    {
        for(int i = 1; i < 3; i++)
            FileSystem::MakeDir(files[i]);
        for(int i = 3; i < 6; i++)
            FileSystem::CreateFileWithSomeContent(files[i]);

        fileTreeStorage = new FileTreeStorage();
        fileTreeStorage->Init(testDirPath);
    }

    virtual void TearDown()
    {
        FileSystem::RemoveDirContent(testDirPath);
        delete fileTreeStorage;
    }
};

TEST_F(FileTreeStorageTest, GetTest)
{
    ino_t inode;
    for(int i = 0; i < 6; i++)
    {
        std::list<std::string> paths;
        ASSERT_TRUE(fileTreeStorage->TryGetInode(files[i], inode));
        ASSERT_TRUE(fileTreeStorage->TryGetPaths(inode, paths));
        ASSERT_EQ(1, paths.size());
        ASSERT_EQ(files[i], *(paths.begin()));
    }

    std::string notExistingFile = files[2] + "/notExistingFile";
    ASSERT_FALSE(fileTreeStorage->TryGetInode(notExistingFile, inode));
    std::string notExistingFile2 = files[2] + "/notExistingDir/notExistingFile";
    ASSERT_FALSE(fileTreeStorage->TryGetInode(notExistingFile2, inode));
}

TEST_F(FileTreeStorageTest, AddTest)
{
    std::string newFilePath = testDirPath + "/newFile";
    FileSystem::CreateFileWithSomeContent(newFilePath);
    static struct stat st;
    lstat(newFilePath.c_str(), &st);
    ino_t newFileInode = st.st_ino;

    std::list<std::string> paths;
    ino_t inode;
    ASSERT_FALSE(fileTreeStorage->TryGetInode(newFilePath, inode));
    ASSERT_FALSE(fileTreeStorage->TryGetPaths(newFileInode, paths));
    ASSERT_TRUE(fileTreeStorage->Add(newFilePath, newFileInode));
    ASSERT_TRUE(fileTreeStorage->TryGetInode(newFilePath, inode));
    ASSERT_EQ(newFileInode, inode);
    ASSERT_TRUE(fileTreeStorage->TryGetPaths(newFileInode, paths));
    ASSERT_EQ(1, paths.size());
    ASSERT_EQ(newFilePath, *(paths.begin()));
}

TEST_F(FileTreeStorageTest, AddForTheSameInodeTest)
{
    std::string newFilePath = testDirPath + "/newFile";
    ino_t file1Inode;
    ASSERT_TRUE(fileTreeStorage->TryGetInode(files[3], file1Inode));

    ASSERT_FALSE(fileTreeStorage->Add(files[3], file1Inode));
    ASSERT_FALSE(fileTreeStorage->Add(files[5], file1Inode));
    ASSERT_TRUE(fileTreeStorage->Add(newFilePath, file1Inode));

    std::list<std::string> paths;
    ino_t inode;
    ASSERT_TRUE(fileTreeStorage->TryGetInode(files[3], inode));
    ASSERT_EQ(file1Inode, inode);
    ASSERT_TRUE(fileTreeStorage->TryGetInode(newFilePath, inode));
    ASSERT_EQ(file1Inode, inode);
    ASSERT_TRUE(fileTreeStorage->TryGetPaths(inode, paths));
    ASSERT_EQ(2, paths.size());
    std::list<std::string>::iterator it = paths.begin();
    ASSERT_EQ(files[3], *it);
    ASSERT_EQ(newFilePath, *(++it));
}

TEST_F(FileTreeStorageTest, RemoveTest)
{
    ino_t file1Inode;
    ino_t file3Inode;
    ASSERT_TRUE(fileTreeStorage->TryGetInode(files[3], file1Inode));
    ASSERT_TRUE(fileTreeStorage->TryGetInode(files[5], file3Inode));

    std::list<std::string> paths;
    ino_t inode;
    ASSERT_TRUE(fileTreeStorage->Remove(files[3]));
    ASSERT_FALSE(fileTreeStorage->TryGetInode(files[3], inode));
    ASSERT_FALSE(fileTreeStorage->TryGetPaths(file1Inode, paths));
}

TEST_F(FileTreeStorageTest, MoveTest)
{
    std::string oldPath = files[3];
    std::string newFilePath = testDirPath + "/newFile";
    ino_t oldInode;
    ASSERT_TRUE(fileTreeStorage->TryGetInode(oldPath, oldInode));

    ino_t inode;
    std::string path;
    ASSERT_TRUE(fileTreeStorage->Move(oldPath, newFilePath));
    ASSERT_FALSE(fileTreeStorage->TryGetInode(oldPath, inode));
    ASSERT_TRUE(fileTreeStorage->TryGetInode(newFilePath, inode));
    ASSERT_EQ(oldInode, inode);
}
