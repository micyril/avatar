#include "TestingFSObserver.h"

TestingFSObserver::~TestingFSObserver()
{
    for(auto it = receivedEvents.begin(); it != receivedEvents.end(); it++)
        delete *it;
}

void TestingFSObserver::Update(const FileSystemEvent &data)
{
    if (data.type == FileSystemEvent::Moved)
        receivedEvents.push_back(new MovedEvent((MovedEvent&)data));
    else
        receivedEvents.push_back(new FileSystemEvent(data));
    receivedEventsCount++;
}
