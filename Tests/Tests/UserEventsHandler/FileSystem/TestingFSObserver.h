#ifndef TESTFSOBSERVER_H
#define TESTFSOBSERVER_H

#include <vector>
#include <UserEventsHandler/Core/Observer.h>
#include <UserEventsHandler/FileSystem/FileSystemEvent.h>

class TestingFSObserver : public Observer<FileSystemEvent>
{
public:
    std::vector<FileSystemEvent*> receivedEvents;
    int receivedEventsCount = 0;

    ~TestingFSObserver();
    virtual void Update(const FileSystemEvent &data);
};

#endif // TESTFSOBSERVER_H
