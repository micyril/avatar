#include <vector>
#include <QDir>
#include <gtest/gtest.h>
#include <UserEventsHandler/FileSystem/MacFSConnector.h>
#include <UserEventsHandler/Core/Observer.h>
#include "../../Stuff/FileSystem.h"
#include "TestingFSObserver.h"

using namespace std;

class MacFSConnectorTest : public ::testing::Test
{
private:
    const unsigned long millisecondsWaitingStep = 50;

protected:
    TestingFSObserver *observer;
    std::string testdirPath;
    std::string files[9];

    MacFSConnectorTest() : testdirPath(QDir::currentPath().toStdString() + "/testdir")
    {
        if(!FileSystem::Exists(testdirPath))
            FileSystem::MakeDir(testdirPath);

        files[0] = testdirPath;
        files[1] = testdirPath + "/subdir";
        files[2] = testdirPath + "/subdir/.hiddenSubdir";
        files[3] = testdirPath + "/subdir2";
        files[4] = testdirPath + "/file1";
        files[5] = testdirPath + "/.hiddenFile";
        files[6] = testdirPath + "/subdir/files3";
        files[7] = testdirPath + "/subdir/files4";
        files[8] = testdirPath + "/subdir/files5";
    }

    virtual void SetUp()
    {
        for(int i = 1; i < 4; i++)
            FileSystem::MakeDir(files[i]);
        for(int i = 4; i < 9; i++)
            FileSystem::CreateFileWithSomeContent(files[i]);
        //sleep(1);

        observer = new TestingFSObserver();
        MacFSConnector::GetConnector()->SetWatchedPath(testdirPath);
        MacFSConnector::GetConnector()->RegisterObserver(observer);
        MacFSConnector::GetConnector()->Run();
    }

    virtual void TearDown()
    {
        MacFSConnector::GetConnector()->Stop();
        MacFSConnector::GetConnector()->RemoveObserver(observer);
        delete observer;

        FileSystem::RemoveDirContent(testdirPath);
    }

    bool WaitForTotalNumberEvents(int number, unsigned long millisecondsMax)
    {
        unsigned long waitedTime = millisecondsWaitingStep;
        while(observer->receivedEventsCount < number && waitedTime <= millisecondsMax)
        {
           if(MacFSConnector::GetConnector()->WaitWhileRunning(millisecondsWaitingStep))
               break;
           waitedTime += millisecondsWaitingStep;
        }
        return number <= observer->receivedEventsCount;
    }
};



TEST_F(MacFSConnectorTest, CreateRemoveTest)
{
    std::string newFilePath = testdirPath + "/newFile";
    EXPECT_FALSE(FileSystem::Exists(newFilePath));

    EXPECT_TRUE(FileSystem::CreateFileWithSomeContent(newFilePath));
    ASSERT_TRUE(WaitForTotalNumberEvents(1, 2500));
    ASSERT_EQ(FileSystemEvent::Created, observer->receivedEvents[0]->type);
    ASSERT_EQ(newFilePath, observer->receivedEvents[0]->path);

    EXPECT_TRUE(FileSystem::RemoveFile(newFilePath));
    ASSERT_TRUE(WaitForTotalNumberEvents(2, 2500));
    ASSERT_EQ(FileSystemEvent::Removed, observer->receivedEvents[1]->type);
    ASSERT_EQ(newFilePath, observer->receivedEvents[1]->path);

    EXPECT_TRUE(FileSystem::RemoveFile(files[4]));
    EXPECT_TRUE(FileSystem::CreateFileWithSomeContent(files[4]));
    ASSERT_TRUE(WaitForTotalNumberEvents(4, 2500));
    ASSERT_EQ(FileSystemEvent::Removed, observer->receivedEvents[2]->type);
    ASSERT_EQ(files[4], observer->receivedEvents[2]->path);
    ASSERT_EQ(FileSystemEvent::Created, observer->receivedEvents[3]->type);
    ASSERT_EQ(files[4], observer->receivedEvents[3]->path);

    EXPECT_TRUE(FileSystem::RemoveFile(files[8]));
    EXPECT_TRUE(FileSystem::RemoveFile(files[4]));
    EXPECT_TRUE(FileSystem::CreateFileWithSomeContent(files[4]));
    ASSERT_TRUE(WaitForTotalNumberEvents(7, 2500));
    ASSERT_EQ(FileSystemEvent::Removed, observer->receivedEvents[4]->type);
    ASSERT_EQ(files[8], observer->receivedEvents[4]->path);
    ASSERT_EQ(FileSystemEvent::Removed, observer->receivedEvents[5]->type);
    ASSERT_EQ(files[4], observer->receivedEvents[5]->path);
    ASSERT_EQ(FileSystemEvent::Created, observer->receivedEvents[6]->type);
    ASSERT_EQ(files[4], observer->receivedEvents[6]->path);
}

TEST_F(MacFSConnectorTest, MoveTest)
{
    std::string file3NewPath = testdirPath + "/subdir2/file3";
    EXPECT_TRUE(FileSystem::MoveFile(files[6], file3NewPath));
    ASSERT_TRUE(WaitForTotalNumberEvents(1, 5500));
    ASSERT_EQ(FileSystemEvent::Moved, observer->receivedEvents[0]->type);
    ASSERT_EQ(files[6], observer->receivedEvents[0]->path);
    ASSERT_EQ(file3NewPath, ((MovedEvent*)(observer->receivedEvents[0]))->newPath);

    std::string subdir2NewPath = testdirPath + "/subdir/subdir2";
    EXPECT_TRUE(FileSystem::MoveFile(files[3], subdir2NewPath));
    ASSERT_TRUE(WaitForTotalNumberEvents(2, 2500));
    ASSERT_EQ(FileSystemEvent::Moved, observer->receivedEvents[1]->type);
    ASSERT_EQ(files[3], observer->receivedEvents[1]->path);
    ASSERT_EQ(subdir2NewPath, ((MovedEvent*)(observer->receivedEvents[1]))->newPath);

    std::string file3changedPath = testdirPath + "/subdir/subdir2/file3";
    EXPECT_TRUE(FileSystem::MoveFile(file3changedPath, files[4]));
    ASSERT_TRUE(WaitForTotalNumberEvents(4, 2500));
    ASSERT_EQ(FileSystemEvent::Removed, observer->receivedEvents[2]->type);
    ASSERT_EQ(files[4], observer->receivedEvents[2]->path);
    ASSERT_EQ(FileSystemEvent::Moved, observer->receivedEvents[3]->type);
    ASSERT_EQ(file3changedPath, observer->receivedEvents[3]->path);
    ASSERT_EQ(files[4], ((MovedEvent*)(observer->receivedEvents[3]))->newPath);
}

TEST_F(MacFSConnectorTest, RemoveMoveTest)
{
    EXPECT_TRUE(FileSystem::RemoveFile(files[7]));
    ASSERT_TRUE(WaitForTotalNumberEvents(1, 2500));
    ASSERT_EQ(FileSystemEvent::Removed, observer->receivedEvents[0]->type);
    ASSERT_EQ(files[7], observer->receivedEvents[0]->path);

    std::string file3NewPath = testdirPath + "/subdir2/file3";
    EXPECT_TRUE(FileSystem::MoveFile(files[6], file3NewPath));
    ASSERT_TRUE(WaitForTotalNumberEvents(2, 2500));
    ASSERT_EQ(FileSystemEvent::Moved, observer->receivedEvents[1]->type);
    ASSERT_EQ(files[6], observer->receivedEvents[1]->path);
    ASSERT_EQ(file3NewPath, ((MovedEvent*)(observer->receivedEvents[1]))->newPath);

    EXPECT_TRUE(FileSystem::RemoveFile(files[5]));
    ASSERT_TRUE(WaitForTotalNumberEvents(3, 2500));
    ASSERT_EQ(FileSystemEvent::Removed, observer->receivedEvents[2]->type);
    ASSERT_EQ(files[5], observer->receivedEvents[2]->path);

    std::string subdir2NewPath = testdirPath + "/subdir/subdir2";
    EXPECT_TRUE(FileSystem::MoveFile(files[3], subdir2NewPath));
    ASSERT_TRUE(WaitForTotalNumberEvents(4, 2500));
    ASSERT_EQ(FileSystemEvent::Moved, observer->receivedEvents[3]->type);
    ASSERT_EQ(files[3], observer->receivedEvents[3]->path);
    ASSERT_EQ(subdir2NewPath, ((MovedEvent*)(observer->receivedEvents[3]))->newPath);

    std::string file3changedPath = testdirPath + "/subdir/subdir2/file3";
    EXPECT_TRUE(FileSystem::RemoveFile(files[2]));
    EXPECT_TRUE(FileSystem::MoveFile(file3changedPath, files[4]));
    ASSERT_TRUE(WaitForTotalNumberEvents(7, 2500));
    ASSERT_EQ(FileSystemEvent::Removed, observer->receivedEvents[4]->type);
    ASSERT_EQ(files[2], observer->receivedEvents[4]->path);
    ASSERT_EQ(FileSystemEvent::Removed, observer->receivedEvents[5]->type);
    ASSERT_EQ(files[4], observer->receivedEvents[5]->path);
    ASSERT_EQ(FileSystemEvent::Moved, observer->receivedEvents[6]->type);
    ASSERT_EQ(file3changedPath, observer->receivedEvents[6]->path);
    ASSERT_EQ(files[4], ((MovedEvent*)(observer->receivedEvents[6]))->newPath);
}


