#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <UserEventsHandler/FileSystem/FSEventsObserver/FSEventsFilter.h>

class MockObserver : public Observer<FileSystemEvent>
{
public:
    MOCK_METHOD1(Update, void(const FileSystemEvent& event));
};

class MockFilter : public FSEventsFilter
{
public:
    MockFilter(Observer<FileSystemEvent> *observer) : FSEventsFilter(observer) {}
    MOCK_METHOD1(Passes, bool(const FileSystemEvent& event));
};

class FSEventsFiltersTest : public ::testing::Test
{
};

TEST_F(FSEventsFiltersTest, FSEventsFilterTest)
{
    using namespace testing;

    MockObserver observer;
    MockFilter filter(&observer);
    EXPECT_CALL(filter, Passes(_)).WillOnce(Return(false));
    FileSystemEvent event(FileSystemEvent::Created, std::string("path"), 0);
    filter.Update(event);

    EXPECT_CALL(filter, Passes(_)).WillOnce(Return(true));
    EXPECT_CALL(observer, Update(_)).Times(1);
    filter.Update(event);
}

TEST_F(FSEventsFiltersTest, AllowWhatAllowedFilterTest)
{
    std::string allowedPath = "/processable/path";
    std::string notAllowedPath = "/not/processable/path";
    FileSystemEvent passableEvent(FileSystemEvent::Created, allowedPath + std::string("/file"), 1);
    FileSystemEvent notPassableEvent1(FileSystemEvent::Removed, notAllowedPath + std::string("/file"), 2);
    FileSystemEvent notPassableEvent2(FileSystemEvent::Removed, allowedPath + std::string("file"), 3);

    AllowWhatAllowedFilter filter(allowedPath + std::string("/.*"));
    ASSERT_TRUE(filter.Passes(passableEvent));
    ASSERT_FALSE(filter.Passes(notPassableEvent1));
    ASSERT_FALSE(filter.Passes(notPassableEvent2));

    ASSERT_EQ(1, 1);
}

TEST_F(FSEventsFiltersTest, AllowWhatNotRefusedFilterTest)
{
    FileSystemEvent passableEvent1(FileSystemEvent::Created, "/somepath/file", 1);
    FileSystemEvent passableEvent2(FileSystemEvent::Created, "/somepath/file.download.ext", 2);
    FileSystemEvent notPassableEvent(FileSystemEvent::Removed, "/somepath/file.download", 3);

    AllowWhatNotRefusedFilter filter(".*\.download");
    ASSERT_TRUE(filter.Passes(passableEvent1));
    ASSERT_TRUE(filter.Passes(passableEvent2));
    ASSERT_FALSE(filter.Passes(notPassableEvent));
}

TEST_F(FSEventsFiltersTest, AndFiltersTest)
{
    std::string allowedPath = "/processable/path";
    std::string notAllowedPath = "/not/processable/path";

    AllowWhatAllowedFilter filter1(allowedPath + std::string("/.*"));
    AllowWhatNotRefusedFilter filter2(".*\.download");
    AndFilter andFilter;
    andFilter.AddFilter(&filter1);
    andFilter.AddFilter(&filter2);

    FileSystemEvent passableEvent(FileSystemEvent::Created, allowedPath + std::string("/file"), 1);
    FileSystemEvent notPassableEvent1(FileSystemEvent::Removed, notAllowedPath + std::string("/file"), 2);
    FileSystemEvent notPassableEvent2(FileSystemEvent::Created, allowedPath + std::string("/file.download"), 3);
    FileSystemEvent notPassableEvent3(FileSystemEvent::Created, notAllowedPath + std::string("/file.download"), 4);

    ASSERT_TRUE(andFilter.Passes(passableEvent));
    ASSERT_FALSE(andFilter.Passes(notPassableEvent1));
    ASSERT_FALSE(andFilter.Passes(notPassableEvent2));
    ASSERT_FALSE(andFilter.Passes(notPassableEvent3));
}

TEST_F(FSEventsFiltersTest, OrFiltersTest)
{
    std::string allowedPath = "/processable/path";
    std::string notAllowedPath = "/not/processable/path";

    AllowWhatAllowedFilter filter1(allowedPath + std::string("/.*"));
    AllowWhatNotRefusedFilter filter2(".*\.download");
    OrFilter orFilter;
    orFilter.AddFilter(&filter1);
    orFilter.AddFilter(&filter2);

    FileSystemEvent passableEvent1(FileSystemEvent::Created, allowedPath + std::string("/file"), 1);
    FileSystemEvent passableEvent2(FileSystemEvent::Removed, notAllowedPath + std::string("/file"), 2);
    FileSystemEvent passableEvent3(FileSystemEvent::Created, allowedPath + std::string("/file.download"), 3);
    FileSystemEvent notPassableEvent(FileSystemEvent::Created, notAllowedPath + std::string("/file.download"), 4);

    ASSERT_TRUE(orFilter.Passes(passableEvent1));
    ASSERT_TRUE(orFilter.Passes(passableEvent2));
    ASSERT_TRUE(orFilter.Passes(passableEvent3));
    ASSERT_FALSE(orFilter.Passes(notPassableEvent));
}
