#include <gtest/gtest.h>
#include <UserEventsHandler/FileSystem/FSEventsObserver/MacDownloadedFileEventConverter.h>
#include "../TestingFSObserver.h"


class MacDownloadedFileEventConverterTest : public ::testing::Test
{
protected:
    TestingFSObserver observer;
    std::string downloadsFolderPath;
    MacDownloadedFileEventConverter resultObserver;

    MacDownloadedFileEventConverterTest() :
        downloadsFolderPath("/path/to/downloads"), resultObserver(&observer, downloadsFolderPath) { }

    void checkConverting(const FileSystemEvent& inputEvent, const FileSystemEvent& outputEvent)
    {
        int expectedEventsCount = observer.receivedEventsCount + 1;
        int index = expectedEventsCount - 1;
        resultObserver.Update(inputEvent);
        ASSERT_EQ(expectedEventsCount, observer.receivedEventsCount);
        ASSERT_EQ(outputEvent.type, observer.receivedEvents[index]->type);
        ASSERT_EQ(outputEvent.path, observer.receivedEvents[index]->path);
        ASSERT_EQ(outputEvent.timestamp, observer.receivedEvents[index]->timestamp);

        if(outputEvent.type == FileSystemEvent::Moved)
            ASSERT_EQ(((const MovedEvent&)outputEvent).newPath, ((const MovedEvent*)observer.receivedEvents[index])->newPath);
    }
};

TEST_F(MacDownloadedFileEventConverterTest, UpdateTest)
{
    std::string expectedPath = downloadsFolderPath + std::string("/file.ext");
    long long expectedTimestamp = 11;

    MovedEvent convertedEvent(downloadsFolderPath + std::string("/file.ext.download/file.ext"), expectedPath, expectedTimestamp);
    checkConverting(convertedEvent, FileSystemEvent(FileSystemEvent::Created, expectedPath, expectedTimestamp));

    MovedEvent notConvertedEvent(downloadsFolderPath + std::string("/file.ext.download/file.ext"),
                                 downloadsFolderPath + std::string("/file.ext.download/file2.ext"), expectedTimestamp + 1);
    FileSystemEvent notConvertedEvent2(FileSystemEvent::Created, std::string("some/path/file"), expectedTimestamp + 2);
    checkConverting(notConvertedEvent, notConvertedEvent);
    checkConverting(notConvertedEvent2, notConvertedEvent2);
}
