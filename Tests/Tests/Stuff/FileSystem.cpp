#include <cstdlib>
#include <QFile>
#include "FileSystem.h"

bool FileSystem::MakeDir(std::string &path)
{
    return system(("mkdir " + path).c_str()) == 0;
}

bool FileSystem::RemoveDirContent(std::string &path)
{
    return system(("rm -rf " + path + "/*").c_str()) == 0;
}

bool FileSystem::CreateFileWithSomeContent(std::string &path)
{
    return system(("echo 123 > " + path).c_str()) == 0;
}

bool FileSystem::RemoveFile(std::string &path)
{
    return system(("rm -r " + path).c_str()) == 0;
}

bool FileSystem::MoveFile(std::string &oldPath, std::string &newPath)
{
    return system(("mv " + oldPath + " " + newPath).c_str()) == 0;
}

bool FileSystem::Exists(std::string &path)
{
    QFile file(QString::fromUtf8(path.c_str()));
    return file.exists();
}
