#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <string>

class FileSystem
{
public:
    static bool MakeDir(std::string& path);
    static bool RemoveDirContent(std::string& path);
    static bool CreateFileWithSomeContent(std::string& path);
    static bool RemoveFile(std::string& path);
    static bool MoveFile(std::string& oldPath, std::string& newPath);
    static bool Exists(std::string& path);
};

#endif // FILESYSTEM_H
