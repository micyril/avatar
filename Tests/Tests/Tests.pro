QT += core
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

LIBS += -framework CoreServices

SOURCES += main.cpp \
    UserEventsHandler/FileSystem/MacFSConnectorTest.cpp \
    Stuff/FileSystem.cpp \
    UserEventsHandler/FileSystem/FileTreeStorageTest.cpp \
    UserEventsHandler/Core/TaskPerformerTest.cpp \
    UserEventsHandler/Core/ToolsTest.cpp \
    UserEventsHandler/FileSystem/FSEventsObserver/FSEventsFiltersTests.cpp \
    UserEventsHandler/FileSystem/FSEventsObserver/MacDownloadedFileEventConverter.cpp \
    UserEventsHandler/FileSystem/TestingFSObserver.cpp


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../UserEventsHandler/Release/release/ -lUserEventsHandler
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../UserEventsHandler/Release/debug/ -lUserEventsHandler
else:unix: LIBS += -L$$PWD/../../UserEventsHandler/Debug/ -lUserEventsHandler

INCLUDEPATH += $$PWD/../../UserEventsHandler
DEPENDPATH += $$PWD/../../UserEventsHandler

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../UserEventsHandler/Release/release/UserEventsHandler.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../UserEventsHandler/Release/debug/UserEventsHandler.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../UserEventsHandler/Debug/libUserEventsHandler.a

HEADERS += \
    Stuff/FileSystem.h \
    UserEventsHandler/FileSystem/TestingFSObserver.h

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../gmock/Release/release/ -lGMockBuilder
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../gmock/Release/debug/ -lGMockBuilder
else:unix: LIBS += -L$$PWD/../../gmock/Release/ -lGMockBuilder

INCLUDEPATH += $$PWD/../../gmock/gmock-1.7.0/fused-src
DEPENDPATH += $$PWD/../../gmock/gmock-1.7.0/fused-src

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../gmock/Release/release/libGMockBuilder.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../gmock/Release/debug/libGMockBuilder.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../gmock/Release/release/GMockBuilder.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../gmock/Release/debug/GMockBuilder.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../gmock/Release/libGMockBuilder.a
