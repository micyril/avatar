QT       -= core gui

TARGET = GMockBuilder
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    ../gmock-1.7.0/src/gmock-all.cc \
    ../gmock-1.7.0/gtest/src/gtest-all.cc

INCLUDEPATH += ../gmock-1.7.0/include
INCLUDEPATH += ../gmock-1.7.0
INCLUDEPATH += ../gmock-1.7.0/gtest/include
INCLUDEPATH += ../gmock-1.7.0/gtest
