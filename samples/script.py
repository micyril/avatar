f = open('filesList3.txt', 'r')
lines = f.readlines()
f.close()
print min(lines, key=len), len(min(lines, key=len))
print max(lines, key=len), len(max(lines, key=len))

acceptableSize = 65
with open('filesList2.txt', 'w') as f:
    f.writelines(s for s in lines if len(s) >= acceptableSize)
