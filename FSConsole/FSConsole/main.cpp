#include <iostream>
#include <signal.h>
#include <UserEventsHandler/FileSystem/FSEventsObserver/FileSystemXmlLogger.h>
#include <UserEventsHandler/FileSystem/FSEventsObserver/FSEventsFilter.h>
#include <UserEventsHandler/FileSystem/FSEventsObserver/MacDownloadedFileEventConverter.h>
#include <UserEventsHandler/FileSystem/MacFSConnector.h>
#include <UserEventsHandler/Core/Exceptions.h>

void signalHandler(int signal)
{
    if(signal == SIGINT)
    {
        std::cout << "Terminating execution...";
        MacFSConnector::GetConnector()->Stop();
        std::cout << " Done" << std::endl;
        return;
    }
    std::cout << "Got not handling signal" << std::endl;
}

int main()
{
    signal(SIGINT, signalHandler);

    FileSystemXmlLogger fsXmlLogger("fileSystemEventsLog.xml");

    AllowWhatAllowedFilter filter1("/Users/kirill/Downloads/.*");
    AllowWhatNotRefusedFilter filter2(".*\.download.*");
    AllowWhatNotRefusedFilter filter3(".*\.DS_Store");
    AndFilter fsXmlLoggerWithFiltering(&fsXmlLogger);
    fsXmlLoggerWithFiltering.AddFilter(&filter1);
    fsXmlLoggerWithFiltering.AddFilter(&filter2);
    fsXmlLoggerWithFiltering.AddFilter(&filter3);
    MacDownloadedFileEventConverter fsXmlLoggerWithConvertingAndFiltering(&fsXmlLoggerWithFiltering, "/Users/kirill/Downloads");

    MacFSConnector::GetConnector()->RegisterObserver(&fsXmlLoggerWithConvertingAndFiltering);
    MacFSConnector::GetConnector()->SetWatchedPath("/Users/kirill");
    MacFSConnector::GetConnector()->Run();

    std::cout << "Starting..." << std::endl;

    MacFSConnector::GetConnector()->WaitWhileRunning();

    return 0;
}

