QT += core xml
QT -= gui
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

LIBS += -framework CoreServices

SOURCES += main.cpp

INCLUDEPATH += $$PWD/../../UserEventsHandler
DEPENDPATH += $$PWD/../../UserEventsHandler

CONFIG(release, debug|release) {
    macx: LIBS += -L$$PWD/../../UserEventsHandler/Release/ -lUserEventsHandler
    macx: PRE_TARGETDEPS += $$PWD/../../UserEventsHandler/Release/libUserEventsHandler.a
}

CONFIG(debug, debug|release) {
    macx: LIBS += -L$$PWD/../../UserEventsHandler/Debug/ -lUserEventsHandler
    macx: PRE_TARGETDEPS += $$PWD/../../UserEventsHandler/Debug/libUserEventsHandler.a
}
