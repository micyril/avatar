/****************************************************************************
**
**
** This header file defines class used to test a particular representation
** provider for strings
**
**
****************************************************************************/

#ifndef TESTDRIVER_H
#define TESTDRIVER_H

#include <vector>
#include <string>
#include "../ErrorCalculators/ErrorCalculator.h"
#include "../../RepresentationProvider/RepresentationProvider.h"
#include "../TestingResult.h"

class TestDriver
{
public:
    std::vector<TestingResult> Test(const RepresentationProvider::IRepresentationProvider* representationProvider,
                                    const std::vector<std::string> &testingSamples,
                                    const std::vector< std::shared_ptr<IErrorCalculator> > &errorCalculators);
};

#endif // TESTDRIVER_H
