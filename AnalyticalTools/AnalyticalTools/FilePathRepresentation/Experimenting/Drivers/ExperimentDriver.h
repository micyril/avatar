/****************************************************************************
**
**
** This header file defines a class used to do experiments with given method
** and error calculators
**
**
****************************************************************************/

#ifndef EXPERIMENTDRIVER_H
#define EXPERIMENTDRIVER_H

#include <map>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include "../../Tools/IO.h"
#include "../../Core/ICreater.h"
#include "../ErrorCalculators/ErrorCalculator.h"
#include "../../RepresentationProvider/RepresentationProvider.h"
#include "../TestingResult.h"
#include "Exceptions.h"

template<class TestDriverT>
class ExperimentDriver
{
private:
    TestDriverT *testDriver;
    const std::map< std::string, std::shared_ptr< ICreater<RepresentationProvider::IRepresentationProvider> > > *representationProviderCreaters;
    const std::map< std::string, std::shared_ptr<ICreater<IErrorCalculator> > > *errorCalculatorsCreaters;

public:
    ExperimentDriver(TestDriverT *testDriver,
                     const std::map< std::string, std::shared_ptr< ICreater<RepresentationProvider::IRepresentationProvider> > > *representationProviderCreaters,
                     const std::map< std::string, std::shared_ptr<ICreater<IErrorCalculator> > > *errorCalculatorsCreaters) :
        testDriver(testDriver), representationProviderCreaters(representationProviderCreaters), errorCalculatorsCreaters(errorCalculatorsCreaters) {}

    std::vector<TestingResult> Start(const boost::program_options::variables_map &vm)
    {
        std::vector< std::shared_ptr<IErrorCalculator> > usedErrorCalculators;
        std::vector<std::string> errorCalculatorTypes;
        boost::split(errorCalculatorTypes, vm["output-format"].as<std::string>(), boost::is_any_of(":"));
        for(size_t i = 0; i < errorCalculatorTypes.size(); i++)
        {
            auto it = errorCalculatorsCreaters->find(errorCalculatorTypes[i]);
            if(it == errorCalculatorsCreaters->end())
                throw NotFoundErrorCalculatorException(errorCalculatorTypes[i]);
            usedErrorCalculators.push_back(std::shared_ptr<IErrorCalculator>(it->second->Create(vm)));
        }

        auto representationProviderCreaterIt = representationProviderCreaters->find(vm["method"].as<std::string>());
        if(representationProviderCreaterIt == representationProviderCreaters->end())
            throw NotFoundMethodException(vm["method"].as<std::string>());

        std::vector<std::string> testingSamples;
        if(!Tools::ReadAllLines(vm["testing-samples"].as<std::string>(), testingSamples))
            throw Tools::ReadingException(vm["testing-samples"].as<std::string>());

        std::auto_ptr<RepresentationProvider::IRepresentationProvider> representationProvider = representationProviderCreaterIt->second->Create(vm);
        representationProvider->Teach();

        return testDriver->Test(representationProvider.get(), testingSamples, usedErrorCalculators);
    }
};

#endif // EXPERIMENTDRIVER_H
