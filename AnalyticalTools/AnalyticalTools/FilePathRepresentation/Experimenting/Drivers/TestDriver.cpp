#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include "TestDriver.h"

std::vector<TestingResult> TestDriver::Test(const RepresentationProvider::IRepresentationProvider *representationProvider,
                                            const std::vector<std::string> &testingSamples,
                                            const std::vector<std::shared_ptr<IErrorCalculator> > &errorCalculators)
{
    using namespace boost::accumulators;
    std::vector< accumulator_set<float, stats<tag::variance> > > errorAccumulators;
    for(size_t i = 0; i < errorCalculators.size(); i++)
        errorAccumulators.push_back(accumulator_set<float, stats<tag::variance> >());

    for(size_t i = 0; i < testingSamples.size(); i++)
    {
        auto representation = representationProvider->GetRepresentation(testingSamples[i]);
        std::string original = representationProvider->GetOriginal(representation.get());
        for(size_t j = 0; j < errorCalculators.size(); j++)
            errorAccumulators[j](errorCalculators[j]->CalculateError(testingSamples[i], original));
    }
    std::vector<TestingResult> results;
    for(size_t i = 0; i < errorAccumulators.size(); i++)
        results.push_back(TestingResult(mean(errorAccumulators[i]), sqrt(variance(errorAccumulators[i]))));
    return results;
}
