/****************************************************************************
**
**
** This header file defines exceptions related to experiments
**
**
****************************************************************************/

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>
#include <sstream>

class NotFoundErrorCalculatorException : public std::exception
{
private:
    std::string message;
public:
    NotFoundErrorCalculatorException(const std::string &errorCalculatorType)
    {
        std::ostringstream ss;
        ss << "Can't find error calculator of the type \"" << errorCalculatorType << "\"";;
        message = ss.str();
    }

    virtual ~NotFoundErrorCalculatorException() throw() {}

    virtual const char* what() const throw()
    {
        return message.c_str();
    }
};

class NotFoundMethodException : public std::exception
{
private:
    std::string message;
public:
    NotFoundMethodException(const std::string &methodType)
    {
        std::ostringstream ss;
        ss << "Can't find method of the type \"" << methodType << "\"";
        message = ss.str();
    }

    virtual ~NotFoundMethodException() throw() {}

    virtual const char* what() const throw()
    {
        return message.c_str();
    }
};

#endif // EXCEPTIONS_H
