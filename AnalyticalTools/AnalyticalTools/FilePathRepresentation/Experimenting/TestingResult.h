/****************************************************************************
**
**
** This header file defines a structure containing testing results: mean and
** standart deviation of the error
**
**
****************************************************************************/

#ifndef TESTINGRESULT_H
#define TESTINGRESULT_H

struct TestingResult
{
    float meanError;
    float standardDeviation;
    TestingResult(float meanError, float standardDeviation) :
        meanError(meanError), standardDeviation(standardDeviation) { }
};

#endif // TESTINGRESULT_H
