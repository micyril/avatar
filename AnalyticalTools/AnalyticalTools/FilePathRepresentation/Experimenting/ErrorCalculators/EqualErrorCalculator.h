/****************************************************************************
**
**
** This header file defines an error calculator which returns 0 if strings
** are equal and 1 otherwise
**
**
****************************************************************************/

#ifndef EQUALERRORCALCULATOR_H
#define EQUALERRORCALCULATOR_H

#include "ErrorCalculator.h"
#include "../../Core/ICreater.h"

class EqualErrorCalculator: public IErrorCalculator
{
public:
    virtual float CalculateError(const std::string &expectedString, const std::string &actualString);
};

class EqualErrorCalculatorCreater : public ICreater<IErrorCalculator>
{
public:
    virtual std::auto_ptr<IErrorCalculator> Create(const boost::program_options::variables_map &vm);
};

#endif // EQUALERRORCALCULATOR_H
