#include "LengthErrorCalculator.h"

float LengthErrorCalculator::CalculateError(const std::string &expectedString, const std::string &actualString)
{
    return abs(expectedString.size() - actualString.size());
}

std::auto_ptr<IErrorCalculator> LengthErrorCalculatorCreater::Create(const boost::program_options::variables_map &vm)
{
    return std::auto_ptr<IErrorCalculator>(new LengthErrorCalculator());
}
