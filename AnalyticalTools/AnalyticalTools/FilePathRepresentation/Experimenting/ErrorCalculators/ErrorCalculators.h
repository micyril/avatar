/****************************************************************************
**
**
** This header file includes headers of error calculators and their factory
** classes
**
**
****************************************************************************/

#ifndef ERRORCALCULATORS_H
#define ERRORCALCULATORS_H

#include "BitsErrorCalculator.h"
#include "LengthErrorCalculator.h"
#include "IdentificationErrorCalculator.h"
#include "LevenshteinDistanceCalculator.h"
#include "EqualErrorCalculator.h"

#endif // ERRORCALCULATORS_H
