/****************************************************************************
**
**
** This header file defines abstract class used to calculate how much two
** string are distinguished
**
**
****************************************************************************/

#ifndef ERRORCALCULATOR_H
#define ERRORCALCULATOR_H

#include <boost/program_options.hpp>

class IErrorCalculator
{
public:
    virtual ~IErrorCalculator() {}
    virtual float CalculateError(const std::string &expectedString, const std::string &actualString) = 0;
};

#endif // ERRORCALCULATOR_H
