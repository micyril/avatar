#include <boost/dynamic_bitset.hpp>
#include "BitsErrorCalculator.h"
#include "../../Tools/BinaryCoding.h"

float BitsErrorCalculator::CalculateError(const std::string &expectedString, const std::string &actualString)
{
    boost::dynamic_bitset<unsigned char> bits1 = Tools::BinaryCoding::Code(expectedString);
    boost::dynamic_bitset<unsigned char> bits2 = Tools::BinaryCoding::Code(actualString);

    int result = 0;
    int maxCommonBitsCount = std::min(bits1.size(), bits2.size());
    for(int i = 0; i < maxCommonBitsCount; i++)
        if(bits1[i] != bits2[i])
            result++;
    const boost::dynamic_bitset<unsigned char> &bitsetWithLargerSize = bits1.size() > bits2.size()? bits1 : bits2;
    for(size_t i = maxCommonBitsCount; i < bitsetWithLargerSize.size(); i++)
        if(bitsetWithLargerSize[i])
            result++;
    return (float)result / bitsetWithLargerSize.size();
}


std::auto_ptr<IErrorCalculator> BitsErrorCalculatorCreater::Create(const boost::program_options::variables_map &vm)
{
    return std::auto_ptr<IErrorCalculator>(new BitsErrorCalculator());
}
