#include "EqualErrorCalculator.h"

float EqualErrorCalculator::CalculateError(const std::string &expectedString, const std::string &actualString)
{
    return expectedString == actualString? 0.0F : 1.0F;
}


std::auto_ptr<IErrorCalculator> EqualErrorCalculatorCreater::Create(const boost::program_options::variables_map &vm)
{
    return std::auto_ptr<IErrorCalculator>(new EqualErrorCalculator());
}
