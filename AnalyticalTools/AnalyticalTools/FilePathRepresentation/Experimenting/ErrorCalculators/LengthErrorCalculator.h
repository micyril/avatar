/****************************************************************************
**
**
** This header file defines an error calculator which calculates length
** difference between expected and actual strings
**
**
****************************************************************************/

#ifndef LENGTHERRORCALCULATOR_H
#define LENGTHERRORCALCULATOR_H

#include "ErrorCalculator.h"
#include "../../Core/ICreater.h"

class LengthErrorCalculator : public IErrorCalculator
{
public:
    virtual float CalculateError(const std::string &expectedString, const std::string &actualString);
};

class LengthErrorCalculatorCreater : public ICreater<IErrorCalculator>
{
public:
    virtual std::auto_ptr<IErrorCalculator> Create(const boost::program_options::variables_map &vm);
};

#endif // LENGTHERRORCALCULATOR_H
