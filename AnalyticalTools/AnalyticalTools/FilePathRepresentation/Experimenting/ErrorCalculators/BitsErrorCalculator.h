/****************************************************************************
**
**
** This header file defines an error calculator which calculates how much
** bits of binary representations of strings are not correlative
**
**
****************************************************************************/

#ifndef BITSERRORCALCULATOR_H
#define BITSERRORCALCULATOR_H

#include "ErrorCalculator.h"
#include "../../Core/ICreater.h"

class BitsErrorCalculator : public IErrorCalculator
{
public:
    virtual float CalculateError(const std::string &expectedString, const std::string &actualString);
};

class BitsErrorCalculatorCreater : public ICreater<IErrorCalculator>
{
public:
    virtual std::auto_ptr<IErrorCalculator> Create(const boost::program_options::variables_map &vm);
};

#endif // BITSERRORCALCULATOR_H
