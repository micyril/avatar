/****************************************************************************
**
**
** This header file defines an error calculator which calculates if string
** can be identified by a particular error calculator
**
**
****************************************************************************/

#ifndef IDENTIFICATIONERRORCALCULATOR_H
#define IDENTIFICATIONERRORCALCULATOR_H

#include "ErrorCalculator.h"
#include "../../Core/ICreater.h"

class IdentificationErrorCalculator : public IErrorCalculator
{
private:
    std::auto_ptr<IErrorCalculator> errorCalculatorForIdentification;
    std::vector<std::string> stringsForComparison;

public:
    IdentificationErrorCalculator(std::auto_ptr<IErrorCalculator> errorCalculatorForIdentification, const std::vector<std::string> &stringsForComparison);
    virtual float CalculateError(const std::string &expectedString, const std::string &actualString);
};

class IdentificationErrorCalculatorCreater : public ICreater<IErrorCalculator>
{
protected:
    ICreater<IErrorCalculator> *errorCalculatorForIdentificationCreater;
public:
    IdentificationErrorCalculatorCreater(ICreater<IErrorCalculator> *errorCalculatorForIdentificationCreater);
    virtual std::auto_ptr<IErrorCalculator> Create(const boost::program_options::variables_map &vm);
};

#endif // IDENTIFICATIONERRORCALCULATOR_H
