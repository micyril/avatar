#include <boost/dynamic_bitset.hpp>
#include "../../Tools/BinaryCoding.h"
#include "LevenshteinDistanceCalculator.h"

float LevenshteinDistanceCalculator::CalculateError(const std::string &expectedString, const std::string &actualString)
{
    boost::dynamic_bitset<unsigned char> bits1 = Tools::BinaryCoding::Code(expectedString);
    boost::dynamic_bitset<unsigned char> bits2 = Tools::BinaryCoding::Code(actualString);

    const size_t len1 = bits1.size(), len2 = bits2.size();
    std::vector<std::vector<unsigned int> > d(len1 + 1, std::vector<unsigned int>(len2 + 1));

    d[0][0] = 0;
    for(unsigned int i = 1; i <= len1; ++i) d[i][0] = i;
    for(unsigned int i = 1; i <= len2; ++i) d[0][i] = i;

    for(unsigned int i = 1; i <= len1; ++i)
        for(unsigned int j = 1; j <= len2; ++j)
              d[i][j] = std::min( std::min(d[i - 1][j] + 1, d[i][j - 1] + 1),
                                           d[i - 1][j - 1] + (bits1[i - 1] == bits2[j - 1] ? 0 : 1) );

    return (float) d[len1][len2] / std::max(len1, len2);
}


std::auto_ptr<IErrorCalculator> LevenshteinDistanceCalculatorCreater::Create(const boost::program_options::variables_map &vm)
{
    return std::auto_ptr<IErrorCalculator>(new LevenshteinDistanceCalculator());
}
