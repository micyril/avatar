#include "IdentificationErrorCalculator.h"
#include "../../Tools/IO.h"

IdentificationErrorCalculator::IdentificationErrorCalculator(std::auto_ptr<IErrorCalculator> errorCalculatorForIdentification,
                                                             const std::vector<std::string> &stringsForComparison) :
    errorCalculatorForIdentification(errorCalculatorForIdentification), stringsForComparison(stringsForComparison) { }

float IdentificationErrorCalculator::CalculateError(const std::string &expectedString, const std::string &actualString)
{
    float error = errorCalculatorForIdentification->CalculateError(expectedString, actualString);
    for(size_t i = 0; i < stringsForComparison.size(); i++)
        if (errorCalculatorForIdentification->CalculateError(stringsForComparison[i], actualString) <= error &&
            stringsForComparison[i] != expectedString)
            return 1.0F;
    return 0.0F;
}

IdentificationErrorCalculatorCreater::IdentificationErrorCalculatorCreater(ICreater<IErrorCalculator> *errorCalculatorForIdentificationCreater) :
    errorCalculatorForIdentificationCreater(errorCalculatorForIdentificationCreater) { }

std::auto_ptr<IErrorCalculator> IdentificationErrorCalculatorCreater::Create(const boost::program_options::variables_map &vm)
{
    std::vector<std::string> samples;
    if(!Tools::ReadAllLines(vm["teaching-samples"].as<std::string>(), samples))
        throw Tools::ReadingException(vm["teaching-samples"].as<std::string>());
    if(!Tools::ReadAllLines(vm["testing-samples"].as<std::string>(), samples))
        throw Tools::ReadingException(vm["testing-samples"].as<std::string>());
    return std::auto_ptr<IErrorCalculator>(new IdentificationErrorCalculator(errorCalculatorForIdentificationCreater->Create(vm), samples));
}



