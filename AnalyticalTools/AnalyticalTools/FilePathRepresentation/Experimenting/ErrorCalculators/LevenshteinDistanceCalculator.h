/****************************************************************************
**
**
** This header file defines an error calculator which calculates a
** Levenshtein distance between bits representations of strings
**
**
****************************************************************************/

#ifndef LEVENSHTEINDISTANCECALCULATOR_H
#define LEVENSHTEINDISTANCECALCULATOR_H

#include "ErrorCalculator.h"
#include "../../Core/ICreater.h"

class LevenshteinDistanceCalculator : public IErrorCalculator
{
public:
    float CalculateError(const std::string &expectedString, const std::string &actualString);
};

class LevenshteinDistanceCalculatorCreater : public ICreater<IErrorCalculator>
{
public:
    std::auto_ptr<IErrorCalculator> Create(const boost::program_options::variables_map &vm);
};

#endif // LEVENSHTEINDISTANCECALCULATOR_H
