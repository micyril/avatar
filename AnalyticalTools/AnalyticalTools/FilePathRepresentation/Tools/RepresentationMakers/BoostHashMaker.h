/****************************************************************************
**
**
** This header file defines class making hash based on hash function
** of boost library
**
**
****************************************************************************/

#ifndef BOOSTHASHMAKER_H
#define BOOSTHASHMAKER_H

#include <boost/functional/hash.hpp>
#include "IntBaseHashMaker.h"
#include "../../Core/ICreater.h"

namespace Tools
{

class BoostHashMaker : public IntBaseHashMaker<int>
{
public:
    BoostHashMaker(const int desirableLength) : IntBaseHashMaker(boost::hash<std::string>(), desirableLength) {}
};

class BoostHashMakerCreater : public ICreater<IRepresentationMaker>
{
public:
    virtual std::auto_ptr<IRepresentationMaker> Create(const boost::program_options::variables_map &vm)
    {
        return std::auto_ptr<IRepresentationMaker>(new BoostHashMaker(vm["repr-size"].as<int>()));
    }
};

}

#endif // BOOSTHASHMAKER_H
