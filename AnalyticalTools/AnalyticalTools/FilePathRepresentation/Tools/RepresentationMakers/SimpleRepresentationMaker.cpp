#include <cmath>
#include "SimpleRepresentationMaker.h"
#include "../BinaryCoding.h"
#include "../IO.h"

Tools::SimpleRepresentationMaker::SimpleRepresentationMaker(const size_t samplesCount) : currentPlace(0)
{
    representationSize = ceil(samplesCount / 8.0) * 8;
}

std::string Tools::SimpleRepresentationMaker::Make(const std::string &str)
{
    if(currentPlace >= representationSize)
        throw TooMuchMakeCallsException(representationSize);

    boost::dynamic_bitset<unsigned char> bits(representationSize);
    bits.set(currentPlace++);

    return BinaryCoding::Decode(bits);
}


std::auto_ptr<Tools::IRepresentationMaker> Tools::SimpleRepresentationMakerCreater::Create(const boost::program_options::variables_map &vm)
{
    std::vector<std::string> teachingSamples;
    if(!Tools::ReadAllLines(vm["teaching-samples"].as<std::string>(), teachingSamples))
        throw Tools::ReadingException(vm["teaching-samples"].as<std::string>());
    return std::auto_ptr<IRepresentationMaker>(new SimpleRepresentationMaker(teachingSamples.size()));
}
