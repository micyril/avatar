/****************************************************************************
**
**
** This header file defines class making simple representation with binary view
** like 0...010...0, where place of "1" indicates number of string
**
**
****************************************************************************/

#ifndef SIMPLEREPRESENTATIONMAKER_H
#define SIMPLEREPRESENTATIONMAKER_H

#include <exception>
#include <boost/dynamic_bitset.hpp>
#include "IRepresentationMaker.h"
#include "../../Core/ICreater.h"

namespace Tools
{

class SimpleRepresentationMaker : public IRepresentationMaker
{
private:
    size_t representationSize;
    int currentPlace;

public:
    SimpleRepresentationMaker(const size_t samplesCount);
    virtual std::string Make(const std::string &str);
};

class SimpleRepresentationMakerCreater : public ICreater<IRepresentationMaker>
{
public:
    virtual std::auto_ptr<IRepresentationMaker> Create(const boost::program_options::variables_map &vm);
};

class TooMuchMakeCallsException : public std::exception
{
private:
    std::string message;
public:
    TooMuchMakeCallsException(const size_t representationSize)
    {
        std::ostringstream ss;
        ss << "Can't make more than representation size " << representationSize;
        message = ss.str();
    }

    virtual ~TooMuchMakeCallsException() throw() {}

    virtual const char* what() const throw()
    {
        return message.c_str();
    }
};

}


#endif // SIMPLEREPRESENTATIONMAKER_H
