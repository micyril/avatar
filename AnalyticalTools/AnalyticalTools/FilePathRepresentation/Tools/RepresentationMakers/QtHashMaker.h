#ifndef QTHASHMAKER_H
#define QTHASHMAKER_H

#include "IntBaseHashMaker.h"
#include "../../Core/ICreater.h"

namespace Tools
{

class QtHashMaker : public IntBaseHashMaker<int>
{
public:
    QtHashMaker(const int desirableLength);
};

}

#endif // QTHASHMAKER_H
