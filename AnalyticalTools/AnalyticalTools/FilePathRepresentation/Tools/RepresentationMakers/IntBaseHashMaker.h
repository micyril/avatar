/****************************************************************************
**
**
** This header file defines class making hash based on function like interface
** returning hash of integer type for string
**
**
****************************************************************************/

#ifndef INTBASEHASHMAKER_H
#define INTBASEHASHMAKER_H

#include <functional>
#include <cmath>
#include "IRepresentationMaker.h"

namespace Tools
{

template<typename IntType>
class IntBaseHashMaker : public IRepresentationMaker
{
private:
    std::function<IntType(const std::string&)> intTypeHashProvider;
    int intTypeSize;
    int binaryTreeHeight;

    int makeHashAndReturnCurrentHashPosition(char currentHash[], int currentHashPosition, const char *str, int currentStrPosition,
                                             int currentStrLength, const int maxDepth, int currentDepth)
    {
        if(currentStrLength <= 0 || currentDepth > maxDepth)
            return currentHashPosition;
        IntType hash = intTypeHashProvider(std::string(str + currentStrPosition, currentStrLength));
        const char* charPtrToHash = (const char*)&hash;
        for(int i = 0; i < intTypeSize; i++, currentHashPosition++)
            currentHash[currentHashPosition] = charPtrToHash[i];

        currentHashPosition = makeHashAndReturnCurrentHashPosition(currentHash, currentHashPosition, str, currentStrPosition,
                                                                   currentStrLength / 2, maxDepth, currentDepth + 1);
        return makeHashAndReturnCurrentHashPosition(currentHash, currentHashPosition, str, currentStrPosition + currentStrLength / 2,
                                                    currentStrLength - currentStrLength / 2, maxDepth, currentDepth + 1);
    }

public:
    IntBaseHashMaker(std::function<IntType(const std::string&)> intTypeHashProvider, const int desirableLength) :
        intTypeHashProvider(intTypeHashProvider), intTypeSize(sizeof(IntType))
    {
        binaryTreeHeight = ceil(log2((double)desirableLength / intTypeSize + 1.0));
    }

    virtual std::string Make(const std::string &str)
    {
        int maxSize = ((1 << binaryTreeHeight) - 1) * intTypeSize;
        char hash[maxSize];
        int actualHashSize = makeHashAndReturnCurrentHashPosition(hash, 0, str.c_str(), 0, str.size(), binaryTreeHeight, 1);
        return std::string(hash, actualHashSize);
    }
};

}



#endif // INTBASEHASHMAKER_H
