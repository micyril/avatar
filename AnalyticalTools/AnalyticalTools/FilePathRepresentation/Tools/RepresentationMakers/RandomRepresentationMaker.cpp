#include <random>
#include "RandomRepresentationMaker.h"

Tools::RandomRepresentationMaker::RandomRepresentationMaker(int stringLength) :
    stringLength(stringLength) { }

std::string Tools::RandomRepresentationMaker::Make(const std::string &str)
{
    char result[stringLength];
    for(int i = 0; i < stringLength; i++)
        result[i] = (char)(rand() % (0x7E - 0x21) + 0x21); // use just visiable ascii characters
    return std::string(result, stringLength);
}

std::auto_ptr<Tools::IRepresentationMaker> Tools::RandomRepresentationMakerCreater::Create(const boost::program_options::variables_map &vm)
{
    return std::auto_ptr<IRepresentationMaker>(new RandomRepresentationMaker(vm["repr-size"].as<int>()));
}
