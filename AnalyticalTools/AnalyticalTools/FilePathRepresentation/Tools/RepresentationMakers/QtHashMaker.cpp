#include <QHash>
#include "QtHashMaker.h"

static int getQtHash(const std::string &str)
{
    QString qStr(str.c_str());
    return (int)qHash(qStr);
}

Tools::QtHashMaker::QtHashMaker(const int desirableLength) :
    IntBaseHashMaker(getQtHash, desirableLength) {  }
