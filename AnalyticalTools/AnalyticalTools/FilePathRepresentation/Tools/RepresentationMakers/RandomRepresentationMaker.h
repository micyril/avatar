/****************************************************************************
**
**
** This header file defines class making random string of desirable length
**
**
****************************************************************************/

#ifndef RANDOMREPRESENTATIONMAKER_H
#define RANDOMREPRESENTATIONMAKER_H

#include "IRepresentationMaker.h"
#include "../../Core/ICreater.h"

namespace Tools
{

class RandomRepresentationMaker : public IRepresentationMaker
{
private:
    int stringLength;

public:
    RandomRepresentationMaker(int stringLength);
    virtual std::string Make(const std::string &str);
};

class RandomRepresentationMakerCreater : public ICreater<IRepresentationMaker>
{
public:
    virtual std::auto_ptr<IRepresentationMaker> Create(const boost::program_options::variables_map &vm);
};

}

#endif // RANDOMREPRESENTATIONMAKER_H
