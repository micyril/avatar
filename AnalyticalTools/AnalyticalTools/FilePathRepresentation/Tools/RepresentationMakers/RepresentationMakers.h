/****************************************************************************
**
**
** This header file includes headers of real hash makers
**
**
****************************************************************************/

#ifndef HASHMAKERS_H
#define HASHMAKERS_H

#include "BoostHashMaker.h"
#include "SimpleRepresentationMaker.h"
#include "RandomRepresentationMaker.h"

#endif // HASHMAKERS_H
