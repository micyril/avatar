/****************************************************************************
**
**
** This header file defines an interface for making representation classes
**
**
****************************************************************************/

#ifndef IHASHMAKER_H
#define IHASHMAKER_H

#include <string>

namespace Tools
{

class IRepresentationMaker
{
public:
    virtual ~IRepresentationMaker() {}
    virtual std::string Make(const std::string &str) = 0;
};

}

#endif // IHASHMAKER_H
