#include <fstream>
#include <sstream>
#include "IO.h"

bool Tools::ReadAllLines(const std::string &path, std::vector<std::string> &result)
{
    std::ifstream inputStream(path);
    if(!inputStream.good())
        return false;
    std::string buffer;
    while(inputStream.good())
    {
        std::getline(inputStream, buffer);
        if(inputStream.good() || buffer != "")
            result.push_back(buffer);
    }
    return true;
}


Tools::ReadingException::ReadingException(const std::string &path)
{
    std::ostringstream ss;
    ss << "Can't read file " << path;
    message = ss.str();
}

const char *Tools::ReadingException::what() const throw()
{
    return message.c_str();
}


bool Tools::WriteAllLines(const std::string &path, const std::vector<std::string> &lines)
{
    std::ofstream outputStream(path);
    if(!outputStream.good())
        return false;
    for(size_t i = 0; i < lines.size(); i++)
        outputStream << lines[i] << std::endl;
    return true;
}
