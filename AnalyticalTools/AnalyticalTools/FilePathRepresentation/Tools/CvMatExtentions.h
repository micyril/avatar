/****************************************************************************
**
**
** This header file defines some additional functions working with cv::Mat
** type
**
**
****************************************************************************/

#ifndef CVMATEXTENTIONS_H
#define CVMATEXTENTIONS_H

#include <opencv2/core/core.hpp>

namespace Tools
{

bool AreEqual(const cv::Mat &floatMat1, const cv::Mat &floatMat2);

}

#endif // CVMATEXTENTIONS_H
