/****************************************************************************
**
**
** This header file defines some additional functions working with std
** containers
**
**
****************************************************************************/

#ifndef CONTAINERSEXTENTIONS_H
#define CONTAINERSEXTENTIONS_H

#include <algorithm>

namespace Tools
{

template<class ElementsType, class ForwardIterator>
size_t GetMaxSizeOfElements(const ForwardIterator &begin, const ForwardIterator &end)
{
    return std::max_element(begin, end, [](const ElementsType &a, const ElementsType &b)
                                        { return a.size() < b.size(); })->size();
}

}

#endif // CONTAINERSEXTENTIONS_H
