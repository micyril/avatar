#include "BinaryCoding.h"
#include <iostream>
using namespace std;

const unsigned char Tools::BinaryCoding::bits[8] = {1, 2, 4, 8, 16, 32, 64, 128};

void Tools::BinaryCoding::decode(const boost::dynamic_bitset<unsigned char> &bs, char result[], int bytesCount)
{

    for(int i = 0; i < bytesCount; i++)
    {
        result[i] = 0;
        for(int j = 0; j < 8; j++)
            if(bs[i * 8 + j])
                result[i] = result[i] | bits[j];
    }
}

boost::dynamic_bitset<unsigned char> Tools::BinaryCoding::Code(const std::string &str)
{
    return boost::dynamic_bitset<unsigned char>(str.begin(), str.end());
}

std::string Tools::BinaryCoding::Decode(const boost::dynamic_bitset<unsigned char> &bs)
{
    int bytesCount = bs.size() / 8;
    char bytes[bytesCount];
    decode(bs, bytes, bytesCount);
    return std::string(bytes, bytesCount);
}

std::string Tools::BinaryCoding::DecodeWithoutLastZeros(const boost::dynamic_bitset<unsigned char> &bs)
{
    int bytesCount = bs.size() / 8;
    char bytes[bytesCount];
    decode(bs, bytes, bytesCount);
    int lastIndexForNotZero = bytesCount;
    while(lastIndexForNotZero >= 0 && bytes[--lastIndexForNotZero] == 0);

    return std::string(bytes, lastIndexForNotZero + 1);
}

int Tools::BinaryCoding::CalculateCountOfDifferentBits(const boost::dynamic_bitset<unsigned char> &bits1, const boost::dynamic_bitset<unsigned char> &bits2)
{
    int result = 0;
    int maxCommonBitsCount = std::min(bits1.size(), bits2.size());
    for(int i = 0; i < maxCommonBitsCount; i++)
        if(bits1[i] != bits2[i])
            result++;
    const boost::dynamic_bitset<unsigned char> &bitsetWithLargerSize = bits1.size() > bits2.size()? bits1 : bits2;
    for(size_t i = maxCommonBitsCount; i < bitsetWithLargerSize.size(); i++)
        if(bitsetWithLargerSize[i])
            result++;
    return result;
}
