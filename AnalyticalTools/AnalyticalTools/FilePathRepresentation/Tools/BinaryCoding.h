/****************************************************************************
**
**
** This header file defines methods for coding and decoding strings to and
** from binary view
**
**
****************************************************************************/

#ifndef BINARYCODING_H
#define BINARYCODING_H

#include <boost/dynamic_bitset.hpp>

namespace Tools
{

class BinaryCoding
{
private:
    static const unsigned char bits[8];
    static void decode(const boost::dynamic_bitset<unsigned char> &bs, char result[], int bytesCount);

public:
    static boost::dynamic_bitset<unsigned char> Code(const std::string &str);
    static std::string Decode(const boost::dynamic_bitset<unsigned char> &bs);
    static std::string DecodeWithoutLastZeros(const boost::dynamic_bitset<unsigned char> &bs);
    static int CalculateCountOfDifferentBits(const boost::dynamic_bitset<unsigned char> &bits1, const boost::dynamic_bitset<unsigned char> &bits2);
};

}


#endif // BINARYCODING_H
