/****************************************************************************
**
**
** This header file defines some additional functions related to input and
** output
**
**
****************************************************************************/

#ifndef IO_H
#define IO_H

#include <string>
#include <vector>
#include <exception>

namespace Tools
{

bool ReadAllLines(const std::string &path, std::vector<std::string> &result);
bool WriteAllLines(const std::string &path, const std::vector<std::string> &lines);

class ReadingException : public std::exception
{
private:
    std::string message;
public:
    ReadingException(const std::string &path);
    virtual ~ReadingException() throw() {}
    virtual const char* what() const throw();
};

}


#endif // IO_H
