#include "CvMatExtentions.h"

bool Tools::AreEqual(const cv::Mat &floatMat1, const cv::Mat &floatMat2)
{
    if(floatMat1.cols != floatMat2.cols || floatMat1.rows != floatMat2.rows)
        return false;
    return std::equal(floatMat1.begin<float>(), floatMat1.end<float>(), floatMat2.begin<float>());
}
