#include "BAMRepresentationProviderCreater.h"
#include "BAMRepresentationProvider.h"
#include "Exceptions.h"
#include "../../Tools/IO.h"
#include "../../Tools/ContainersExtentions.h"

RepresentationProvider::BAMRepresentationProviderCreater::BAMRepresentationProviderCreater(const std::map<std::string, std::shared_ptr< ICreater<Tools::IRepresentationMaker> > > *representationMakerCreators) :
    representationMakerCreators(representationMakerCreators) {}


std::auto_ptr<RepresentationProvider::IRepresentationProvider> RepresentationProvider::BAMRepresentationProviderCreater::Create(const boost::program_options::variables_map &vm)
{
    auto it = representationMakerCreators->find(vm["repr-maker"].as<std::string>());
    if(it == representationMakerCreators->end())
        throw NotFoundRepresentationMakerException(vm["repr-maker"].as<std::string>());

    std::vector<std::string> teachingSamples;
    if(!Tools::ReadAllLines(vm["teaching-samples"].as<std::string>(), teachingSamples))
        throw Tools::ReadingException(vm["teaching-samples"].as<std::string>());
    std::vector<std::string> testingSamples;
    if(!Tools::ReadAllLines(vm["testing-samples"].as<std::string>(), testingSamples))
        throw Tools::ReadingException(vm["testing-samples"].as<std::string>());

    auto representationMaker = it->second->Create(vm);
    std::vector<std::string> teachingRepresentations;
    for(size_t i = 0; i < teachingSamples.size(); i++)
        teachingRepresentations.push_back(representationMaker->Make(teachingSamples[i]));

    int orignalsMaxSize = std::max<int>(vm["orig-size"].as<int>(), std::max(Tools::GetMaxSizeOfElements<std::string>(teachingSamples.begin(), teachingSamples.end()),
                                                                            Tools::GetMaxSizeOfElements<std::string>(testingSamples.begin(), testingSamples.end())));
    int representationsMaxSize = Tools::GetMaxSizeOfElements<std::string>(teachingRepresentations.begin(), teachingRepresentations.end());
    return std::auto_ptr<IRepresentationProvider>(new BAMRepresentationProvider(teachingSamples, orignalsMaxSize, teachingRepresentations, representationsMaxSize));
}
