#include <cmath>
#include <algorithm>
#include "BAMRepresentationProvider.h"
#include "../../Tools/BinaryCoding.h"
#include "../../Tools/CvMatExtentions.h"
#include "Exceptions.h"

using namespace std;
#include <iostream>

void RepresentationProvider::BAMRepresentationProvider::fillRow(cv::Mat &row, const std::string &value) const
{
    const boost::dynamic_bitset<unsigned char> bits = Tools::BinaryCoding::Code(value);
    row = cv::Mat::ones(row.rows, row.cols, row.type()) * (-1);
    for(size_t i = 0; i < bits.size(); i++)
        if(bits[i])
            row.at<float>(i) = 1.0F;
}

std::string RepresentationProvider::BAMRepresentationProvider::getValue(const cv::Mat &row) const
{
    boost::dynamic_bitset<unsigned char> bits(row.cols);
    for(int i = 0; i < row.cols; i++)
        if(row.at<float>(i) == 1.0F)
            bits.set(i);
    return Tools::BinaryCoding::DecodeWithoutLastZeros(bits);
}

void RepresentationProvider::BAMRepresentationProvider::convertToBipolar(cv::Mat &row, const cv::Mat &oldValue) const
{
    for(int i = 0; i < row.cols; i++)
    {
        if(row.at<float>(i))
            row.at<float>(i) = copysignf(1.0F, row.at<float>(i));
        else
            row.at<float>(i) = oldValue.at<float>(i);
    }
}

cv::Mat RepresentationProvider::BAMRepresentationProvider::findAssociativeVector(const cv::Mat &vector, const cv::Mat &forwardMatrix, const cv::Mat &backwardMatrix) const
{
    cv::Mat beginingVector = vector.clone();
    cv::Mat associativeVector = cv::Mat::zeros(1, forwardMatrix.cols, CV_32FC1);

    cv::Mat calculatedBeginingVector= beginingVector.clone();
    cv::Mat calculatedAssociativeVector = associativeVector.clone();
    while(true)
    {
        convertToBipolar(calculatedAssociativeVector = beginingVector * forwardMatrix, associativeVector);
        if(Tools::AreEqual(associativeVector, calculatedAssociativeVector))
            break;
        swap(associativeVector, calculatedAssociativeVector);
        convertToBipolar(calculatedBeginingVector = associativeVector * backwardMatrix, beginingVector);
        swap(beginingVector, calculatedBeginingVector);
    }
    return associativeVector;
}

RepresentationProvider::BAMRepresentationProvider::BAMRepresentationProvider(const std::vector<std::string> originals, const size_t maxOriginalSize,
                                                     const std::vector<std::string> representations, const size_t maxRepresentationSize) :
    originalRows(originals.size(), maxOriginalSize * 8, CV_32FC1),
    representationRows(representations.size(), maxRepresentationSize * 8, CV_32FC1),
    W(cv::Mat::zeros(originalRows.cols, representationRows.cols, CV_32FC1))
{
    if(originals.size() != representations.size())
        throw RepresentationProvider::OriginalsAndRepresentationsCountsNotEqualException(originals.size(), representations.size());

    for(size_t i = 0; i < originals.size(); i++)
    {
        cv::Mat originalRow = originalRows.row(i), representationRow = representationRows.row(i);
        fillRow(originalRow, originals[i]);
        fillRow(representationRow, representations[i]);

        //cout << "o" << i << ": " << originalRow << endl;
        //cout << "r" << i << ": " << representationRow << endl;
    }

    //cout << endl;
}

void RepresentationProvider::BAMRepresentationProvider::Teach()
{
    for(int i = 0; i < originalRows.rows; i++)
        W += originalRows.row(i).t() * representationRows.row(i);
    W_T = W.t();

    //cout << "W: " << endl << W << endl << endl;
    //W_PInv = W.inv(cv::DECOMP_SVD);
    //W_PInv_T = W_PInv.t();
}

std::auto_ptr<RepresentationProvider::Representation> RepresentationProvider::BAMRepresentationProvider::GetRepresentation(const std::string &original) const
{
    if(original.size() * 8 > originalRows.cols)
        throw TooLargeOriginalException(original.size(), originalRows.cols / 8);
    cv::Mat originalRow(1, originalRows.cols, originalRows.type());
    fillRow(originalRow, original);

    cv::Mat v = findAssociativeVector(originalRow, W, W_T);
    //cout << "o" << originalRow << " --> r" << v << endl;
    return std::auto_ptr<Representation>(new BAMRepresentation(v));
    //return std::auto_ptr<Representation>(new BAMRepresentation(findAssociativeVector(originalRow, W, W_T)));
}

std::string RepresentationProvider::BAMRepresentationProvider::GetOriginal(const Representation *representation) const
{
    cv::Mat v = findAssociativeVector(((BAMRepresentation*)representation)->value, W_T, W);
    //cout << "r" << ((BAMRepresentation*)representation)->value << " --> o" << v << endl;
    return getValue(v);
    //return getValue(findAssociativeVector(((BAMRepresentation*)representation)->value, W_T, W));
}
