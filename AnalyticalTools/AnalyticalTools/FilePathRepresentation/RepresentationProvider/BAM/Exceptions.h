/****************************************************************************
**
**
** This header file defines exceptions related to BAMRepresentationProvider
**
**
****************************************************************************/

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>
#include <sstream>

namespace RepresentationProvider
{

class OriginalsAndRepresentationsCountsNotEqualException : public std::exception
{
private:
    std::string message;

public:
    OriginalsAndRepresentationsCountsNotEqualException(int originalsCount, int representationsCount)
    {
        std::ostringstream ss;
        ss << "Originals count is " << originalsCount << ", representations count is " << representationsCount;
        message = ss.str();
    }
    virtual ~OriginalsAndRepresentationsCountsNotEqualException() throw() {}
    virtual const char* what() const throw()
    {
        return message.c_str();
    }
};

class NotFoundRepresentationMakerException : public std::exception
{
private:
    std::string message;
public:
    NotFoundRepresentationMakerException(const std::string &hashMakerType)
    {
        std::ostringstream ss;
        ss << "Can't find representation maker of the type \"" << hashMakerType << "\"";;
        message = ss.str();
    }
    virtual ~NotFoundRepresentationMakerException() throw() {}
    virtual const char* what() const throw()
    {
        return message.c_str();
    }
};

class TooLargeOriginalException : public std::exception
{
private:
    std::string message;
public:
    TooLargeOriginalException(const size_t originalSize, const size_t maxSize)
    {
        std::ostringstream ss;
        ss << "Original size is " << originalSize << ", but max acceptable size is " << maxSize;
        message = ss.str();
    }
    virtual ~TooLargeOriginalException() throw() {}
    virtual const char* what() const throw()
    {
        return message.c_str();
    }
};

}

#endif // EXCEPTIONS_H
