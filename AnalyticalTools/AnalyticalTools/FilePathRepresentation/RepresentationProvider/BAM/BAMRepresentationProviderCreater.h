/****************************************************************************
**
**
** This header file defines a factory class for BAMRepresentationProvider
**
**
****************************************************************************/

#ifndef BAMREPRESENTATIONPROVIDERCREATER_H
#define BAMREPRESENTATIONPROVIDERCREATER_H

#include <boost/program_options.hpp>
#include "../RepresentationProvider.h"
#include "../../Tools/RepresentationMakers/IRepresentationMaker.h"
#include "../../Core/ICreater.h"

namespace RepresentationProvider
{

class BAMRepresentationProviderCreater : public ICreater<IRepresentationProvider>
{
private:
    const std::map<std::string, std::shared_ptr< ICreater<Tools::IRepresentationMaker> > > *representationMakerCreators;
public:
    BAMRepresentationProviderCreater(const std::map<std::string, std::shared_ptr< ICreater<Tools::IRepresentationMaker> > > *representationMakerCreators);
    virtual std::auto_ptr<IRepresentationProvider> Create(const boost::program_options::variables_map &vm);
};

}

#endif // BAMREPRESENTATIONPROVIDERCREATER_H
