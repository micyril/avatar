/****************************************************************************
**
**
** This header file defines class providing representation for strings and it
** is based on Bidirectorial Associative Memory
**
**
****************************************************************************/

#ifndef BAMREPRESENTATIONPROVIDER_H
#define BAMREPRESENTATIONPROVIDER_H

#include <opencv2/core/core.hpp>
#include <boost/dynamic_bitset.hpp>
#include <exception>
#include <sstream>
#include "../RepresentationProvider.h"

namespace RepresentationProvider
{

class BAMRepresentation : public Representation
{
public:
    cv::Mat value;
    BAMRepresentation(const cv::Mat &value) : value(value) {}
};

class BAMRepresentationProvider : public IRepresentationProvider
{
private:
    const cv::Mat originalRows;
    const cv::Mat representationRows;
    cv::Mat W;
    cv::Mat W_T;
    cv::Mat W_PInv;
    cv::Mat W_PInv_T;

    void fillRow(cv::Mat &row, const std::string& value) const;
    std::string getValue(const cv::Mat &row) const;
    void convertToBipolar(cv::Mat &row, const cv::Mat &oldValue) const;
    cv::Mat findAssociativeVector(const cv::Mat &vector, const cv::Mat &forwardMatrix, const cv::Mat &backwardMatrix) const;

public:
    BAMRepresentationProvider(const std::vector<std::string> originals, const size_t maxOriginalSize, const std::vector<std::string> representations, const size_t maxRepresentationSize);
    virtual void Teach();
    virtual std::auto_ptr<Representation>  GetRepresentation(const std::string& original) const;
    virtual std::string GetOriginal(const Representation* representation) const;
};

}

#endif // BAMREPRESENTATIONPROVIDER_H
