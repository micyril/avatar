/****************************************************************************
**
**
** This header file includes headers of representation providers and their
** factory classes
**
**
****************************************************************************/

#ifndef REPRESENTATIONPROVIDERS_H
#define REPRESENTATIONPROVIDERS_H

#include "BAM/BAMRepresentationProvider.h"
#include "BAM/BAMRepresentationProviderCreater.h"

#endif // REPRESENTATIONPROVIDERS_H
