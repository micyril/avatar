/****************************************************************************
**
**
** This header file defines interfaces for representation providers and
** their factory classes
**
**
****************************************************************************/

#ifndef REPRESENTATIONPROVIDER_H
#define REPRESENTATIONPROVIDER_H

#include <string>
#include <boost/program_options.hpp>

namespace RepresentationProvider
{

class Representation
{
public:
    virtual ~Representation() {}
};

class IRepresentationProvider
{
public:
    virtual ~IRepresentationProvider() {}
    virtual void Teach() = 0;
    virtual std::auto_ptr<Representation> GetRepresentation(const std::string& original) const = 0;
    virtual std::string GetOriginal(const Representation* representation) const = 0;
};

}



#endif // REPRESENTATIONPROVIDER_H
