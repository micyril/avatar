/****************************************************************************
**
**
** This header file defines a factory class, that parameterized by command
** line arguments
**
**
****************************************************************************/

#ifndef ICREATER_H
#define ICREATER_H

#include <boost/program_options.hpp>

template<class CreatedType>
class ICreater
{
public:
    virtual ~ICreater() { }
    virtual std::auto_ptr<CreatedType> Create(const boost::program_options::variables_map &vm) = 0;
};

#endif // ICREATER_H
