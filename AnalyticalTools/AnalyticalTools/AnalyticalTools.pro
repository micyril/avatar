#-------------------------------------------------
#
# Project created by QtCreator 2014-02-23T12:13:10
#
#-------------------------------------------------

QT       += core
QT       -= gui

TARGET = AnalyticalTools
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    FilePathRepresentation/Tools/BinaryCoding.cpp \
    FilePathRepresentation/Tools/CvMatExtentions.cpp \
    FilePathRepresentation/Tools/IO.cpp \
    FilePathRepresentation/Experimenting/ErrorCalculators/BitsErrorCalculator.cpp \
    FilePathRepresentation/Experimenting/ErrorCalculators/LengthErrorCalculator.cpp \
    FilePathRepresentation/Experimenting/Drivers/TestDriver.cpp \
    FilePathRepresentation/RepresentationProvider/BAM/BAMRepresentationProvider.cpp \
    FilePathRepresentation/RepresentationProvider/BAM/BAMRepresentationProviderCreater.cpp \
    FilePathRepresentation/Experimenting/ErrorCalculators/IdentificationErrorCalculator.cpp \
    FilePathRepresentation/Tools/RepresentationMakers/SimpleRepresentationMaker.cpp \
    FilePathRepresentation/Experimenting/ErrorCalculators/LevenshteinDistanceCalculator.cpp \
    FilePathRepresentation/Tools/RepresentationMakers/QtHashMaker.cpp \
    FilePathRepresentation/Tools/RepresentationMakers/RandomRepresentationMaker.cpp \
    FilePathRepresentation/Experimenting/ErrorCalculators/EqualErrorCalculator.cpp \
    Stabilizers/FileAgents/ExtentionUsingStabilizer.cpp \
    Stabilizers/FileAgents/SizeUsingStabilizer.cpp \
    Stabilizers/FileAgents/CreationTimestampUsingStabilizer.cpp \
    Stabilizers/FileAgents/CompositeFileAgentStabilizer.cpp

HEADERS += \
    FilePathRepresentation/Tools/BinaryCoding.h \
    FilePathRepresentation/Tools/CvMatExtentions.h \
    FilePathRepresentation/Tools/IO.h \
    FilePathRepresentation/Experimenting/ErrorCalculators/ErrorCalculator.h \
    FilePathRepresentation/Experimenting/ErrorCalculators/BitsErrorCalculator.h \
    FilePathRepresentation/Experimenting/ErrorCalculators/LengthErrorCalculator.h \
    FilePathRepresentation/Experimenting/ErrorCalculators/ErrorCalculators.h \
    FilePathRepresentation/RepresentationProvider/RepresentationProvider.h \
    FilePathRepresentation/Experimenting/Drivers/TestDriver.h \
    FilePathRepresentation/Experimenting/Drivers/ExperimentDriver.h \
    FilePathRepresentation/Experimenting/TestingResult.h \
    FilePathRepresentation/Experimenting/Drivers/Exceptions.h \
    FilePathRepresentation/RepresentationProvider/RepresentationProviders.h \
    FilePathRepresentation/Experimenting/ErrorCalculators/IdentificationErrorCalculator.h \
    FilePathRepresentation/Tools/Hash/IntBaseHashMaker.h \
    FilePathRepresentation/Tools/Hash/RepresentationMakers.h \
    FilePathRepresentation/Core/ICreater.h \
    FilePathRepresentation/Tools/Hash/IRepresentationMaker.h \
    FilePathRepresentation/Tools/ContainersExtentions.h \
    FilePathRepresentation/Tools/RepresentationMakers/BoostHashMaker.h \
    FilePathRepresentation/Tools/RepresentationMakers/IntBaseHashMaker.h \
    FilePathRepresentation/Tools/RepresentationMakers/IRepresentationMaker.h \
    FilePathRepresentation/Tools/RepresentationMakers/RepresentationMakers.h \
    FilePathRepresentation/RepresentationProvider/BAM/BAMRepresentationProvider.h \
    FilePathRepresentation/RepresentationProvider/BAM/BAMRepresentationProviderCreater.h \
    FilePathRepresentation/RepresentationProvider/BAM/Exceptions.h \
    FilePathRepresentation/Tools/RepresentationMakers/SimpleRepresentationMaker.h \
    FilePathRepresentation/Experimenting/ErrorCalculators/LevenshteinDistanceCalculator.h \
    FilePathRepresentation/Tools/RepresentationMakers/QtHashMaker.h \
    FilePathRepresentation/Tools/RepresentationMakers/RandomRepresentationMaker.h \
    FilePathRepresentation/Experimenting/ErrorCalculators/EqualErrorCalculator.h \
    Stabilizers/IStabilizer.h \
    Stabilizers/NumeralProbabilityStabilizer.h \
    Stabilizers/NonnumeralProbabilityStabilizer.h \
    Stuff/FileInfo.h \
    Stabilizers/FileAgents/IFileAgentStabilizer.h \
    Stabilizers/FileAgents/FileActionInfo.h \
    Stabilizers/FileAgents/ExtentionUsingStabilizer.h \
    Stabilizers/FileAgents/SizeUsingStabilizer.h \
    Stabilizers/FileAgents/CreationTimestampUsingStabilizer.h \
    Stabilizers/FileAgents/CompositeFileAgentStabilizer.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

QMAKE_CXXFLAGS += -std=c++11
