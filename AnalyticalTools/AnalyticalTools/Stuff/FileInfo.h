#ifndef FILEINFO_H
#define FILEINFO_H

#include <string>

struct FileInfo
{
    std::string extention;
    unsigned long long size;
    unsigned long long creationTimestamp;

    FileInfo(std::string extention, unsigned long long size, unsigned long long creationTimestamp) :
        extention(extention), size(size), creationTimestamp(creationTimestamp) { }
};

#endif // FILEINFO_H
