/****************************************************************************
**
**
** This header file defines template for stabilizer, that works with
** numeral input data
**
**
****************************************************************************/

#ifndef NUMERALPROBABILITYSTABILIZER_H
#define NUMERALPROBABILITYSTABILIZER_H

#include <cmath>
#include <boost/unordered_map.hpp>
#include "IStabilizer.h"

template<typename NumeralInputData, class HashableOutputData>
class NumeralProbabilityStabilizer : IStabilizer<NumeralInputData, HashableOutputData>
{
private:
    boost::unordered_map<HashableOutputData, std::vector<NumeralInputData> > data;
    float sigma;

    float calculateProbabilityDensity(const std::vector<NumeralInputData> &gassianFunctionCenters, const NumeralInputData &point)
    {
        float result = 0;
        for(int i = 0; i < gassianFunctionCenters.size(); i++)
        {
            float a = (point - gassianFunctionCenters[i]) / sigma;
            result += exp((-1) * a * a);
        }
        return result;
    }

public:
    NumeralProbabilityStabilizer(float sigma) : //sigma defines width of influence of each data element (NumeralInputData, HashableOutputData)
        sigma(sigma) {}

    virtual void AddData(const NumeralInputData &inputData, const HashableOutputData &outputData)
    {
        auto it = data.find(outputData);
        if(it == data.end())
            it = data.insert(std::make_pair(outputData, std::vector<NumeralInputData>())).first;
        it->second.push_back(inputData);
    }

    virtual bool TryMakeHypothesis(const NumeralInputData &inputData, HashableOutputData &outputData)
    {
        if(data.size() == 0)
            return false;

        std::vector< std::pair<HashableOutputData, float> > probabilityDensities;
        for(auto it = data.begin(); it != data.end(); it++)
        {
            probabilityDensities.push_back(std::make_pair(it->first, calculateProbabilityDensity(it->second, inputData)));
        }
        auto maxElemIt = std::max_element(probabilityDensities.begin(), probabilityDensities.end(),
                                          [](const std::pair<HashableOutputData, float> &a, const std::pair<HashableOutputData, float> &b)
                                          { return a.second < b.second; });
        outputData = maxElemIt->first;
        return true;
    }
};

#endif // NUMERALPROBABILITYSTABILIZER_H
