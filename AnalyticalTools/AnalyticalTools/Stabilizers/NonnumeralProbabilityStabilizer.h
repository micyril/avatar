/****************************************************************************
**
**
** This header file defines template for stabilizer, that works with
** nonnumeral input data
**
**
****************************************************************************/

#ifndef NONNUMERALPROBABILITYSTABILIZER_H
#define NONNUMERALPROBABILITYSTABILIZER_H

#include <boost/unordered_map.hpp>
#include "IStabilizer.h"

template<class HashableInputData, class HashableOutputData>
class NonnumeralProbabilityStabilizer : public IStabilizer<HashableInputData, HashableOutputData>
{
private:
    boost::unordered_map<HashableInputData, boost::unordered_map<HashableOutputData, int> > data;

public:
    virtual void AddData(const HashableInputData &inputData, const HashableOutputData &outputData)
    {
        auto it = data.find(inputData);
        if(it == data.end())
            it = data.insert(std::make_pair(inputData, boost::unordered_map<HashableOutputData, int>())).first;
        auto innerIt = it->second.find(outputData);
        if(innerIt == it->second.end())
            innerIt = it->second.insert(std::make_pair(outputData, 0)).first;
        innerIt->second += 1;
    }

    virtual bool TryMakeHypothesis(const HashableInputData &inputData, HashableOutputData &outputData)
    {
        auto inputDataRowIt = data.find(inputData);
        if(inputDataRowIt == data.end())
            return false;

        auto maxElemIt = std::max_element(inputDataRowIt->second.begin(), inputDataRowIt->second.end(),
                                          [](const std::pair<HashableOutputData, int> &a, const std::pair<HashableOutputData, int> &b)
                                          { return a.second < b.second; });
        outputData = maxElemIt->first;
        return true;
    }
};

#endif // NONNUMERALPROBABILITYSTABILIZER_H
