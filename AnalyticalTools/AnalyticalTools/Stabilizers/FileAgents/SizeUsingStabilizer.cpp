#include "SizeUsingStabilizer.h"

void SizeUsingStabilizer::AddData(const FileInfo &fileInfo, const FileActionInfo &actionInfo)
{
    if(actionInfo.type != FileActionInfo::Moving)
        throw std::logic_error("Can't handle not moving action");
    probabilityStabilizer.AddData(fileInfo.size, ((const MovingActionInfo &)actionInfo).location);
}

bool SizeUsingStabilizer::TryMakeHypothesis(const FileInfo &fileInfo, std::auto_ptr<FileActionInfo> &actionInfoPtr)
{
    std::string resultLocation;
    if(probabilityStabilizer.TryMakeHypothesis(fileInfo.size, resultLocation))
    {
        actionInfoPtr.reset(new MovingActionInfo(resultLocation));
        return true;
    }
    return false;
}
