/****************************************************************************
**
**
** This header file defines file agent stabilizer, that mines connections
** between file creation timestamps and related file actions
**
**
****************************************************************************/

#ifndef CREATIONTIMESTAMPUSINGSTABILIZER_H
#define CREATIONTIMESTAMPUSINGSTABILIZER_H

#include "../NumeralProbabilityStabilizer.h"
#include "IFileAgentStabilizer.h"

class CreationTimestampUsingStabilizer : public IFileAgentStabilizer
{
private:
    NumeralProbabilityStabilizer<unsigned long long, std::string> probabilityStabilizer;

public:
    CreationTimestampUsingStabilizer(float sigma = 0.1F) :
        probabilityStabilizer(sigma) { }
    virtual void AddData(const FileInfo &fileInfo, const FileActionInfo &actionInfo);
    virtual bool TryMakeHypothesis(const FileInfo &fileInfo, std::auto_ptr<FileActionInfo> &actionInfoPtr);
};

#endif // CREATIONTIMESTAMPUSINGSTABILIZER_H
