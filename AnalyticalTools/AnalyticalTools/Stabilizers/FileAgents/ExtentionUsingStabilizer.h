/****************************************************************************
**
**
** This header file defines file agent stabilizer, that mines connections
** between file extenitions and related file actions
**
**
****************************************************************************/

#ifndef FILEEXTENTIONSTABILIZER_H
#define FILEEXTENTIONSTABILIZER_H

#include "../NonnumeralProbabilityStabilizer.h"
#include "IFileAgentStabilizer.h"

class ExtentionUsingStabilizer : public IFileAgentStabilizer
{
private:
    NonnumeralProbabilityStabilizer<std::string, std::string> probabilityStabilizer;

public:
    virtual void AddData(const FileInfo &fileInfo, const FileActionInfo &actionInfo);
    virtual bool TryMakeHypothesis(const FileInfo &fileInfo, std::auto_ptr<FileActionInfo> &actionInfoPtr);
};

#endif // FILEEXTENTIONSTABILIZER_H
