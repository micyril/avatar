#include <random>
#include "CompositeFileAgentStabilizer.h"

void CompositeFileAgentStabilizer::normalizeWeights()
{
    float sum = 0;
    for(int i = 0; i < stabilizersWithWeights.size(); i++)
        sum += stabilizersWithWeights[i].second;
    for(int i = 0; i < stabilizersWithWeights.size(); i++)
        stabilizersWithWeights[i].second /= sum;
}

CompositeFileAgentStabilizer::CompositeFileAgentStabilizer(const std::vector<IFileAgentStabilizer *> &stabilizers, float weightIncreasingCoefficient) :
    weightIncreasingCoefficient(weightIncreasingCoefficient)
{
    for(int i = 0; i < stabilizers.size(); i++)
        stabilizersWithWeights.push_back(std::make_pair(stabilizers[i], 1.0F));
    normalizeWeights();
}

void CompositeFileAgentStabilizer::AddData(const FileInfo &fileInfo, const FileActionInfo &actionInfo)
{
    if(actionInfo.type != FileActionInfo::Moving)
        throw std::logic_error("Can't handle not moving action");
    std::auto_ptr<FileActionInfo> actionInfoPtr;
    for(int i = 0; i < stabilizersWithWeights.size(); i++)
    {
        if(stabilizersWithWeights[i].first->TryMakeHypothesis(fileInfo, actionInfoPtr) && actionInfoPtr.get()->type == FileActionInfo::Moving &&
           ((MovingActionInfo*)actionInfoPtr.get())->location == ((const MovingActionInfo&)actionInfo).location)
            stabilizersWithWeights[i].second *= weightIncreasingCoefficient;
        stabilizersWithWeights[i].first->AddData(fileInfo, actionInfo);
    }
    normalizeWeights();
}

bool CompositeFileAgentStabilizer::TryMakeHypothesis(const FileInfo &fileInfo, std::auto_ptr<FileActionInfo> &actionInfoPtr)
{
    float x = (float)rand() / float(RAND_MAX);
    for(int i = 0; x >= 0; i++)
    {
        i %= stabilizersWithWeights.size();
        if(x < stabilizersWithWeights[i].second && stabilizersWithWeights[i].first->TryMakeHypothesis(fileInfo, actionInfoPtr))
            return true;
        x -= stabilizersWithWeights[i].second;
    }
    return false;
}
