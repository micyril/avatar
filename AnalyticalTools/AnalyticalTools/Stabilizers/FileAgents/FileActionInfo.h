/****************************************************************************
**
**
** This header file defines data types for file action information
**
**
****************************************************************************/

#ifndef FILEACTION_H
#define FILEACTION_H

#include <string>

class FileActionInfo
{
public:
    enum ActionType
    {
        Moving
    };

    ActionType type;

    FileActionInfo(const ActionType type) :
        type(type) { }
    virtual ~FileActionInfo() { }
};

class MovingActionInfo : public FileActionInfo
{
public:
    std::string location;

    MovingActionInfo(const std::string &location) :
        FileActionInfo(FileActionInfo::Moving), location(location) { }
};

#endif // FILEACTION_H
