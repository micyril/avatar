/****************************************************************************
**
**
** This header file defines file agent stabilizer, that mines connections
** between file sizes timestamps and related file actions
**
**
****************************************************************************/

#ifndef SIZEUSINGSTABILIZER_H
#define SIZEUSINGSTABILIZER_H

#include "../NumeralProbabilityStabilizer.h"
#include "IFileAgentStabilizer.h"

class SizeUsingStabilizer : public IFileAgentStabilizer
{
private:
    NumeralProbabilityStabilizer<unsigned long long, std::string> probabilityStabilizer;

public:
    SizeUsingStabilizer(float sigma = 0.1F) :
        probabilityStabilizer(sigma) { }
    virtual void AddData(const FileInfo &fileInfo, const FileActionInfo &actionInfo);
    virtual bool TryMakeHypothesis(const FileInfo &fileInfo, std::auto_ptr<FileActionInfo> &actionInfoPtr);
};

#endif // SIZEUSINGSTABILIZER_H
