/****************************************************************************
**
**
** This header file defines file agent stabilizer, that combines another
** file agent stabilizers
**
**
****************************************************************************/

#ifndef COMPOSITEFILEAGENTSTABILIZER_H
#define COMPOSITEFILEAGENTSTABILIZER_H

#include "IFileAgentStabilizer.h"
#include <vector>

class CompositeFileAgentStabilizer : public IFileAgentStabilizer
{
private:
    std::vector< std::pair<IFileAgentStabilizer*, float> > stabilizersWithWeights;
    float weightIncreasingCoefficient;

    void normalizeWeights();

public:
    CompositeFileAgentStabilizer(const std::vector<IFileAgentStabilizer*> &stabilizers, float weightIncreasingCoefficient);
    virtual void AddData(const FileInfo &fileInfo, const FileActionInfo &actionInfo);
    virtual bool TryMakeHypothesis(const FileInfo &fileInfo, std::auto_ptr<FileActionInfo> &actionInfoPtr);
};

#endif // COMPOSITEFILEAGENTSTABILIZER_H
