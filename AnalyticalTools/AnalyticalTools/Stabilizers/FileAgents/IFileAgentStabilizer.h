/****************************************************************************
**
**
** This header file defines interface for file agent stabilizer - component
** that does data mining with file information and related file actions
**
**
****************************************************************************/

#ifndef IFILEAGENTSTABILIZER_H
#define IFILEAGENTSTABILIZER_H

#include "../../Stuff/FileInfo.h"
#include "FileActionInfo.h"

class IFileAgentStabilizer
{
public:
    virtual void AddData(const FileInfo &fileInfo, const FileActionInfo &actionInfo) = 0;
    virtual bool TryMakeHypothesis(const FileInfo &fileInfo, std::auto_ptr<FileActionInfo> &actionInfoPtr) = 0;
};

#endif // IFILEAGENTSTABILIZER_H
