/****************************************************************************
**
**
** This header file defines interface for stabilizers - components that
** do data mining
**
**
****************************************************************************/

#ifndef ISTABILIZER_H
#define ISTABILIZER_H

template<class InputData, class OutputData>
class IStabilizer
{
public:
    virtual void AddData(const InputData &inputData, const OutputData &outputData) = 0;
    virtual bool TryMakeHypothesis(const InputData &inputData, OutputData &outputData) = 0;
};

#endif // ISTABILIZER_H
