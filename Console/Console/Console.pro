QT += core xml widgets
QT -= gui
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

LIBS += -framework ApplicationServices

SOURCES += main.cpp


macx: LIBS += -L$$PWD/../../UserEventsHandler/Release/ -lUserEventsHandler

INCLUDEPATH += $$PWD/../../UserEventsHandler
DEPENDPATH += $$PWD/../../UserEventsHandler

macx: PRE_TARGETDEPS += $$PWD/../../UserEventsHandler/Release/libUserEventsHandler.a
