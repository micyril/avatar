#include "UserEventsHandler/Mouse/MacMouseConnector.h"
#include "UserEventsHandler/Mouse/MouseXmlLogger.h"
#include "UserEventsHandler/Screenshots//MacScreenshotMaker.h"
#include "UserEventsHandler/Screenshots/ScreenshotLogger.h"
#include "UserEventsHandler/Windows/MacWindowFinder.h"

int main()
{
    MacWindowFinder windowFinder;
    MouseXmlLogger mouseLogger("mouseLog.xml", &windowFinder);
    MacScreenshotMaker screenshotMaker;
    ScreenshotLogger screenshotLogger(&screenshotMaker, "screens");
    int observableMouseEventsMask = MouseEvent::LeftButtonPressed | MouseEvent::LeftButtonReleased |
            MouseEvent::RightButtonPressed | MouseEvent::RightButtonReleased;
    mouseLogger.SetObservableEventsMask(observableMouseEventsMask);
    screenshotLogger.SetObservableEventsMask(observableMouseEventsMask);
    MacMouseConnector::GetConnector()->RegisterObserver(&mouseLogger);
    MacMouseConnector::GetConnector()->RegisterObserver(&screenshotLogger);
    MacMouseConnector::GetConnector()->Run();

    return 0;
}
