#include <sstream>
#include <vector>
#include <map>
#include <boost/assign/list_of.hpp>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <FilePathRepresentation/RepresentationProvider/BAM/BAMRepresentationProviderCreater.h>
#include "FilePathRepresentation/Tools/IO.h"

using namespace ::testing;

class MockRepresentationMaker : public Tools::IRepresentationMaker
{
public:
    MOCK_METHOD1(Make, std::string(const std::string &str));
};

class MockRepresentationMakerCreater : public ICreater<Tools::IRepresentationMaker>
{
public:
    virtual std::auto_ptr<Tools::IRepresentationMaker> Create(const boost::program_options::variables_map &vm)
    {
        return std::auto_ptr<Tools::IRepresentationMaker>(CreateRaw(vm));
    }

    MOCK_METHOD1(CreateRaw, Tools::IRepresentationMaker*(const boost::program_options::variables_map &vm));
};

class BAMRepresentationProviderCreaterTest : public ::testing::Test
{
protected:
    std::string orignalsFileName, testingSamplesFileName;

    BAMRepresentationProviderCreaterTest() :
        orignalsFileName("originals.txt"), testingSamplesFileName("testSamples.txt") { }

    ~BAMRepresentationProviderCreaterTest()
    {
        remove(orignalsFileName.c_str());
        remove(testingSamplesFileName.c_str());
    }
};

TEST_F(BAMRepresentationProviderCreaterTest, CreateTest)
{
    int n = 1;
    std::vector<std::string> originals;
    std::vector<std::string> testingSamples;
    std::vector<std::string> representations;
    for(int i = 0; i < n; i++)
    {
        std::ostringstream ssForOriginals, ssForRepresentations, ssForTestingSamples;
        ssForOriginals << "original string " << i;
        ssForTestingSamples << "long testing string " << i;
        ssForRepresentations << "representation string " << i;
        originals.push_back(ssForOriginals.str());
        testingSamples.push_back(ssForTestingSamples.str());
        representations.push_back(ssForRepresentations.str());
    }

    EXPECT_TRUE(Tools::WriteAllLines(orignalsFileName, originals));
    EXPECT_TRUE(Tools::WriteAllLines(testingSamplesFileName, testingSamples));

    boost::program_options::variables_map vm;
    vm.insert(std::make_pair("repr-maker", boost::program_options::variable_value(std::string("h1"), false)));
    vm.insert(std::make_pair("orig-size", boost::program_options::variable_value(-1, false)));
    vm.insert(std::make_pair("teaching-samples", boost::program_options::variable_value(orignalsFileName, false)));
    vm.insert(std::make_pair("testing-samples", boost::program_options::variable_value(testingSamplesFileName, false)));
    std::map< std::string, std::shared_ptr< ICreater<Tools::IRepresentationMaker> > > reprMakerCreatorsMap =
            boost::assign::map_list_of("h1", std::shared_ptr< ICreater<Tools::IRepresentationMaker > >(new MockRepresentationMakerCreater()));
    RepresentationProvider::BAMRepresentationProviderCreater bamRepresentationProviderCreater(&reprMakerCreatorsMap);
    MockRepresentationMaker *representationMaker = new MockRepresentationMaker;
    EXPECT_CALL(*(MockRepresentationMakerCreater*)(reprMakerCreatorsMap["h1"].get()), CreateRaw(Ref(vm))).WillOnce(Return(representationMaker));
    for(int i = 0; i < n; i++)
        EXPECT_CALL(*representationMaker, Make(originals[i])).WillOnce(::testing::Return(representations[i]));
    auto representationProvider = bamRepresentationProviderCreater.Create(vm);
    representationProvider->Teach();
    for(int i = 0; i < n; i++)
        ASSERT_EQ(originals[i], representationProvider->GetOriginal(representationProvider->GetRepresentation(originals[i]).get()));
}
