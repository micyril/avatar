#include <sstream>
#include <vector>
#include <gtest/gtest.h>
#include <FilePathRepresentation/RepresentationProvider/BAM/BAMRepresentationProvider.h>
#include <iostream>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/variance.hpp>

class BAMRepresentationProviderTest : public ::testing::Test {};

TEST_F(BAMRepresentationProviderTest, GetTest)
{
    int N = 2;
    std::vector<std::string> originals;
    std::vector<std::string> representations;
    for(int i = 0; i < N; i++)
    {
        std::ostringstream ssForOriginals, ssForRepresentations;
        ssForOriginals << "original " << i;
        ssForRepresentations << "rerpresentation " << i;
        originals.push_back(ssForOriginals.str());
        representations.push_back(ssForRepresentations.str());
    }

    RepresentationProvider::BAMRepresentationProvider representationProvider(originals, originals[0].length(), representations, representations[0].length());
    representationProvider.Teach();


    for(int i = 0; i < N; i++)
        EXPECT_EQ(originals[i], representationProvider.GetOriginal(representationProvider.GetRepresentation(originals[i]).get()));

    /*int k = 500;
    int m = 500;
    cv::Mat A(k, m, CV_32FC1);
    for(int i = 0; i < k; i++)
        for(int j = 0; j < m; j++)
            A.at<float>(i, j) = rand() % 1000000;
    cv::Mat A_PInv = A.inv(cv::DECOMP_SVD);
    cv::Mat B = A - A * A_PInv * A;
    cv::Mat C = A_PInv - A_PInv * A * A_PInv;
    //std::cout << B << std::endl;
    //std::cout << C << std::endl;
    using namespace boost::accumulators;
    accumulator_set<float, stats<tag::variance> > errorAccumulator1;
    accumulator_set<float, stats<tag::variance> > errorAccumulator2;
    for(int i = 0; i < k; i++)
        for(int j = 0; j < m; j++)
        {
            errorAccumulator1(B.at<float>(i, j));
            errorAccumulator2(C.at<float>(i, j));
        }
    std::cout << mean(errorAccumulator1) << " " << sqrt(variance(errorAccumulator1)) << std::endl;
    std::cout << mean(errorAccumulator2) << " " << sqrt(variance(errorAccumulator2)) << std::endl;*/
}

//TEST_F(BAMRepresentationProviderTest, GetTest2)
//{
//    int N = 3;
//    int n_begin = 1;
//    int n_end = 1000;
//    int notEqualsCount = 0;
//    for(int n = n_begin; n <= n_end; n++)
//    {
//        std::cout << "n = " << n;
//        std::vector<std::string> originals;
//        std::vector<std::string> representations;
//        for(int i = 0; i < N; i++)
//        {
//            std::ostringstream ssForOriginals, ssForRepresentations;
//            ssForOriginals << std::string(n, 'o') << i;
//            ssForRepresentations << std::string(n, 'r') << i;
//            originals.push_back(ssForOriginals.str());
//            representations.push_back(ssForRepresentations.str());
//        }

//        RepresentationProvider::BAMRepresentationProvider representationProvider(originals, originals[0].length(), representations, representations[0].length());
//        representationProvider.Teach();

//        for(int i = 0; i < N; i++)
//            notEqualsCount += originals[i] != representationProvider.GetOriginal(representationProvider.GetRepresentation(originals[i]).get());
//        std::cout << " end" << std::endl;
//    }

//    EXPECT_EQ((n_end - n_begin + 1) * 2, notEqualsCount);
//}
