#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <boost/assign/list_of.hpp>
#include <FilePathRepresentation/Experimenting/Drivers/TestDriver.h>
#include <FilePathRepresentation/RepresentationProvider/BAM/BAMRepresentationProvider.h>

using namespace RepresentationProvider;

class MockErrorCalculator : public IErrorCalculator
{
public:
    MOCK_METHOD2(CalculateError, float(const std::string &expectedString, const std::string &actualString));
};

class MockRepresentationProvider : public IRepresentationProvider
{
public:
    virtual std::auto_ptr<Representation> GetRepresentation(const std::string &original) const
    {
        return std::auto_ptr<Representation>(GetRepresentationRaw(original));
    }

    MOCK_METHOD0(Teach, void());
    MOCK_CONST_METHOD1(GetRepresentationRaw, Representation*(const std::string& original));
    MOCK_CONST_METHOD1(GetOriginal, std::string(const Representation* representation));
};

class TestDriverTest : public ::testing::Test { };

using namespace ::testing;

TEST_F(TestDriverTest, TestTest)
{
    std::vector< std::shared_ptr<IErrorCalculator> > errorCalculators = boost::assign::list_of(new MockErrorCalculator)(new MockErrorCalculator);
    TestDriver testDriver;
    MockRepresentationProvider representationProvider;

    std::string testingPaths[] = {"path1", "path2", "path3"};
    Representation* representations[] = {new BAMRepresentation(cv::Mat()), new BAMRepresentation(cv::Mat()), new BAMRepresentation(cv::Mat())};
    float errorCalculator1Values[] = {1.0F, 2.0F, 3.0F};
    float errorCalculator2Values[] = {2.0F, 4.0F, 6.0F};
    for(int i = 0; i < 3; i++)
    {
        EXPECT_CALL(representationProvider, GetRepresentationRaw(testingPaths[i])).WillOnce(Return(representations[i]));
        EXPECT_CALL(representationProvider, GetOriginal(representations[i])).WillOnce(Return(testingPaths[i]));
        EXPECT_CALL(*((MockErrorCalculator*)(errorCalculators[0].get())), CalculateError(testingPaths[i], testingPaths[i])).WillOnce(Return(errorCalculator1Values[i]));
        EXPECT_CALL(*((MockErrorCalculator*)(errorCalculators[1].get())), CalculateError(testingPaths[i], testingPaths[i])).WillOnce(Return(errorCalculator2Values[i]));
    }

    TestingResult expectedResults[] = {TestingResult(2.0F, sqrt(2.0F / 3.0F)), TestingResult(4.0F, sqrt(8.0F / 3.0F))};
    std::vector<TestingResult> actualResults = testDriver.Test(&representationProvider,
                                                               std::vector<std::string>(testingPaths, testingPaths + 3),
                                                               errorCalculators);
    ASSERT_EQ(2, actualResults.size());
    for(int i = 0; i < 2; i++)
    {
        ASSERT_EQ(expectedResults[i].meanError, actualResults[i].meanError);
        ASSERT_EQ(expectedResults[i].standardDeviation, actualResults[i].standardDeviation);
    }
}
