#include <boost/assign/list_of.hpp>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string>
#include <FilePathRepresentation/Experimenting/Drivers/ExperimentDriver.h>
#include <FilePathRepresentation/Tools/IO.h>

using namespace RepresentationProvider;

class MockTestDriver
{
public:
    MOCK_METHOD3(Test, std::vector<TestingResult>(const IRepresentationProvider* representationProvider,
                                                  const std::vector<std::string> &testingSamples,
                                                  std::vector< std::shared_ptr<IErrorCalculator> > &errorCalculators));
};

class MockRepresentationProviderCreater : public ICreater<IRepresentationProvider>
{
public:
    virtual std::auto_ptr<IRepresentationProvider> Create(const boost::program_options::variables_map &vm)
    {
        return std::auto_ptr<IRepresentationProvider>(CreateRaw(vm));
    }

    MOCK_METHOD1(CreateRaw, IRepresentationProvider*(const boost::program_options::variables_map &vm));
};

class MockErrorCalculatorCreater : public ICreater<IErrorCalculator>
{
public:
    virtual std::auto_ptr<IErrorCalculator> Create(const boost::program_options::variables_map &vm)
    {
        return std::auto_ptr<IErrorCalculator>(CreateRaw(vm));
    }

    MOCK_METHOD1(CreateRaw, IErrorCalculator*(const boost::program_options::variables_map &vm));
};

class MockRepresentationProvider : public IRepresentationProvider
{
public:
    virtual std::auto_ptr<Representation> GetRepresentation(const std::string &original) const
    {
        return std::auto_ptr<Representation>(GetRepresentationRaw(original));
    }

    MOCK_METHOD0(Teach, void());
    MOCK_CONST_METHOD1(GetRepresentationRaw, Representation*(const std::string& original));
    MOCK_CONST_METHOD1(GetOriginal, std::string(const Representation* representation));
};

class MockErrorCalculator : public IErrorCalculator
{
public:
    MOCK_METHOD2(CalculateError, float(const std::string &expectedString, const std::string &actualString));
};

class ExperimentDriverTest : public ::testing::Test
{
protected:
    std::string testingSamplesFileName;

    ExperimentDriverTest() :
        testingSamplesFileName("testingSamples.txt") { }

    ~ExperimentDriverTest()
    {
        remove(testingSamplesFileName.c_str());
    }
};

using namespace ::testing;

TEST_F(ExperimentDriverTest, StartTest)
{
    MockTestDriver testDriver;
    typedef std::map< std::string, std::shared_ptr< ICreater<IRepresentationProvider> > > RepresentationProviderCreatersMap;
    typedef std::map< std::string, std::shared_ptr< ICreater<IErrorCalculator> > > ErrorCalculatorCreatersMap;
    using namespace RepresentationProvider;
    RepresentationProviderCreatersMap representationProviderCreaters =
            boost::assign::map_list_of("rpc1", std::shared_ptr< ICreater<IRepresentationProvider> >(new MockRepresentationProviderCreater));
    ErrorCalculatorCreatersMap errorCalculatorCreaters =
            boost::assign::map_list_of("ecc1", std::shared_ptr< ICreater<IErrorCalculator> >(new MockErrorCalculatorCreater))
                                      ("ecc2", std::shared_ptr< ICreater<IErrorCalculator> >(new MockErrorCalculatorCreater));
    ExperimentDriver<MockTestDriver> experimentDriver(&testDriver, &representationProviderCreaters, &errorCalculatorCreaters);
    boost::program_options::variables_map vm;
    vm.insert(std::make_pair("method", boost::program_options::variable_value(std::string("rpc1"), false)));
    vm.insert(std::make_pair("output-format", boost::program_options::variable_value(std::string("ecc1:ecc2"), false)));
    vm.insert(std::make_pair("testing-samples", boost::program_options::variable_value(testingSamplesFileName, false)));
    std::vector<std::string> testingSamples = boost::assign::list_of("sample1")("sample2")("sample3");
    Tools::WriteAllLines(testingSamplesFileName, testingSamples);

    IRepresentationProvider *representationProvider = new MockRepresentationProvider();
    IErrorCalculator *errorCalculator1 = new MockErrorCalculator();
    IErrorCalculator *errorCalculator2 = new MockErrorCalculator();
    std::vector<TestingResult> expectedTestingResults = boost::assign::list_of(TestingResult(0, 0))(TestingResult(1, 1))(TestingResult(2, 2));
    EXPECT_CALL(*(MockRepresentationProviderCreater*)(representationProviderCreaters["rpc1"].get()), CreateRaw(Ref(vm))).WillOnce(Return(representationProvider));
    EXPECT_CALL(*(MockErrorCalculatorCreater*)(errorCalculatorCreaters["ecc1"].get()), CreateRaw(Ref(vm))).WillOnce(Return(errorCalculator1));
    EXPECT_CALL(*(MockErrorCalculatorCreater*)(errorCalculatorCreaters["ecc2"].get()), CreateRaw(Ref(vm))).WillOnce(Return(errorCalculator2));
    EXPECT_CALL(*(MockRepresentationProvider*)representationProvider, Teach());
    EXPECT_CALL(testDriver, Test(representationProvider, ElementsAreArray(testingSamples),
                                 ElementsAre(Property(&std::shared_ptr<IErrorCalculator>::get, errorCalculator1),
                                             Property(&std::shared_ptr<IErrorCalculator>::get, errorCalculator2)))).WillOnce(Return(expectedTestingResults));
    std::vector<TestingResult> actualTestingResults = experimentDriver.Start(vm);
    ASSERT_EQ(expectedTestingResults.size(), actualTestingResults.size());
    for(size_t i = 0; i < expectedTestingResults.size(); i++)
    {
        ASSERT_EQ(expectedTestingResults[i].meanError, actualTestingResults[i].meanError);
        ASSERT_EQ(expectedTestingResults[i].standardDeviation, actualTestingResults[i].standardDeviation);
    }
}
