#include <boost/assign/list_of.hpp>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <FilePathRepresentation/Experimenting/ErrorCalculators/IdentificationErrorCalculator.h>
#include <FilePathRepresentation/Tools/IO.h>

using namespace ::testing;

class MockErrorCalculator : public IErrorCalculator
{
public:
    MOCK_METHOD2(CalculateError, float(const std::string &expStr, const std::string &actStr));
};

class MockErrorCalculatorCreater : public ICreater<IErrorCalculator>
{
public:
    virtual std::auto_ptr<IErrorCalculator> Create(const boost::program_options::variables_map &vm)
    {
        return std::auto_ptr<IErrorCalculator>(CreateRaw(vm));
    }

    MOCK_METHOD1(CreateRaw, IErrorCalculator*(const boost::program_options::variables_map &vm));
};

class IdentificationErrorCalculatorCreaterTest : public ::testing::Test
{
protected:
    std::string teachingSamplesFN, testingSamplesFN;

    IdentificationErrorCalculatorCreaterTest() :
        teachingSamplesFN("teaching-samples.txt"), testingSamplesFN("testing-samples.txt") { }

    ~IdentificationErrorCalculatorCreaterTest()
    {
        remove(teachingSamplesFN.c_str());
        remove(testingSamplesFN.c_str());
    }
};

TEST_F(IdentificationErrorCalculatorCreaterTest, CreateTest)
{
    MockErrorCalculatorCreater mockErrorCalculatorCreater;
    IdentificationErrorCalculatorCreater identificationErrorCalculatorCreater(&mockErrorCalculatorCreater);
    boost::program_options::variables_map vm;
    vm.insert(std::make_pair("teaching-samples", boost::program_options::variable_value(teachingSamplesFN, false)));
    vm.insert(std::make_pair("testing-samples", boost::program_options::variable_value(testingSamplesFN, false)));

    std::vector<std::string> teachingSamples = boost::assign::list_of("teaching1")("teaching2");
    std::vector<std::string> testingSamples = boost::assign::list_of("testing1")("testing2");
    EXPECT_TRUE(Tools::WriteAllLines(teachingSamplesFN, teachingSamples));
    EXPECT_TRUE(Tools::WriteAllLines(testingSamplesFN, testingSamples));
    std::vector<std::string> allSamples = boost::assign::list_of("teaching1")("teaching2")("testing1")("testing2");

    IErrorCalculator *errorCalculatorForIdentification = new MockErrorCalculator;
    EXPECT_CALL(mockErrorCalculatorCreater, CreateRaw(Ref(vm))).WillOnce(Return(errorCalculatorForIdentification));

    std::string expectedString = testingSamples[0];
    std::string actualString = "testing10";

    EXPECT_CALL(*(MockErrorCalculator*)errorCalculatorForIdentification, CalculateError(teachingSamples[0], actualString)).WillOnce(Return(0.5F));
    EXPECT_CALL(*(MockErrorCalculator*)errorCalculatorForIdentification, CalculateError(teachingSamples[1], actualString)).WillOnce(Return(0.5F));
    EXPECT_CALL(*(MockErrorCalculator*)errorCalculatorForIdentification, CalculateError(testingSamples[0], actualString)).Times(2).WillRepeatedly(Return(0.25F));
    EXPECT_CALL(*(MockErrorCalculator*)errorCalculatorForIdentification, CalculateError(testingSamples[1], actualString)).WillOnce(Return(0.5F));

    auto identificationErrorCalculator = identificationErrorCalculatorCreater.Create(vm);

    float expected = 0.0F;
    float actual = identificationErrorCalculator->CalculateError(expectedString, actualString);
    ASSERT_EQ(expected, actual);
}
