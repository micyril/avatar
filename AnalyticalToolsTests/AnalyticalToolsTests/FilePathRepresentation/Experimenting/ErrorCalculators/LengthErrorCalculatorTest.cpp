#include <gtest/gtest.h>
#include <FilePathRepresentation/Experimenting/ErrorCalculators/LengthErrorCalculator.h>

class LengthErrorCalculatorTest : public ::testing::Test {};

TEST_F(LengthErrorCalculatorTest, CalculateErrorTest)
{
    std::string str1 = "abc";
    std::string str2 = "abcde";

    LengthErrorCalculator errorCalculator;
    ASSERT_EQ(2.0F, errorCalculator.CalculateError(str1, str2));
    ASSERT_EQ(2.0F, errorCalculator.CalculateError(str2, str1));
}
