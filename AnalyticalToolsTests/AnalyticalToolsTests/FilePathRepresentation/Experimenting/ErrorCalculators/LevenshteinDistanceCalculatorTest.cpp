#include <gtest/gtest.h>
#include <FilePathRepresentation/Experimenting/ErrorCalculators/LevenshteinDistanceCalculator.h>
#include <FilePathRepresentation/Tools/BinaryCoding.h>

class LevenshteinDistanceCalculatorTest : public ::testing::Test { };

TEST_F(LevenshteinDistanceCalculatorTest, CalculateErrorTest)
{
    std::string str1 = Tools::BinaryCoding::Decode(boost::dynamic_bitset<unsigned char>(        std::string("10101010")));  //By these constructors
    std::string str2 = Tools::BinaryCoding::Decode(boost::dynamic_bitset<unsigned char>(        std::string("01010101")));  //bitsets will have bits
    std::string str3 = Tools::BinaryCoding::Decode(boost::dynamic_bitset<unsigned char>(        std::string("10101110")));  //By these constructors
    std::string str4 = Tools::BinaryCoding::Decode(boost::dynamic_bitset<unsigned char>(std::string("1010111000000001")));  //By these constructors

    LevenshteinDistanceCalculator errorCalculator;
    ASSERT_EQ((float)2 / (str1.size() * 8), errorCalculator.CalculateError(str1, str2));
    ASSERT_EQ((float)1 / (str1.size() * 8), errorCalculator.CalculateError(str1, str3));
    ASSERT_EQ((float)9 / (str4.size() * 8), errorCalculator.CalculateError(str1, str4));
}
