#include <gtest/gtest.h>
#include <FilePathRepresentation/Experimenting/ErrorCalculators/BitsErrorCalculator.h>
#include <FilePathRepresentation/Tools/BinaryCoding.h>

class BitsErrorCalculatorTest : public ::testing::Test {};

TEST_F(BitsErrorCalculatorTest, CalculateErrorTest)
{
    std::string str1 = Tools::BinaryCoding::Decode(boost::dynamic_bitset<unsigned char>(        std::string("10011010")));  //By these constructors
    std::string str2 = Tools::BinaryCoding::Decode(boost::dynamic_bitset<unsigned char>(        std::string("11111000")));  //bitsets will have bits
    std::string str3 = Tools::BinaryCoding::Decode(boost::dynamic_bitset<unsigned char>(std::string("0000010111111000")));  //in reverse order than in strings

    BitsErrorCalculator errorCalculator;
    ASSERT_EQ((float)3 / (str1.size() * 8), errorCalculator.CalculateError(str1, str2));
    ASSERT_EQ((float)5 / (str3.size() * 8), errorCalculator.CalculateError(str1, str3));
    ASSERT_EQ((float)2 / (str3.size() * 8), errorCalculator.CalculateError(str2, str3));
}
