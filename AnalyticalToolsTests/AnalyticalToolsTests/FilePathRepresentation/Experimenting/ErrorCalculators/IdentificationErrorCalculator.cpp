#include <boost/assign/std/vector.hpp>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <FilePathRepresentation/Experimenting/ErrorCalculators/IdentificationErrorCalculator.h>
#include <FilePathRepresentation/Tools/IO.h>

using namespace ::testing;

class MockErrorCalculator : public IErrorCalculator
{
public:
    MOCK_METHOD2(CalculateError, float(const std::string &expStr, const std::string &actStr));
};

class IdentificationErrorCalculatorTest : public ::testing::Test
{
protected:
    std::vector<std::string> stringsForComparison;

    IdentificationErrorCalculatorTest()
    {
        using namespace boost::assign;
        stringsForComparison += "string 1", "string 2", "string 3";
    }
};

TEST_F(IdentificationErrorCalculatorTest, CalculateErrorSuccessTest)
{
    IErrorCalculator *errorCalculatorForIdentification = new MockErrorCalculator;
    IdentificationErrorCalculator identificationErrorCalculator(std::auto_ptr<IErrorCalculator>(errorCalculatorForIdentification),
                                                                stringsForComparison);
    std::string expectedString = stringsForComparison[1];
    std::string actualString = "string 20";

    EXPECT_CALL(*(MockErrorCalculator*)errorCalculatorForIdentification, CalculateError(stringsForComparison[0], actualString)).WillOnce(Return(0.5F));
    EXPECT_CALL(*(MockErrorCalculator*)errorCalculatorForIdentification, CalculateError(stringsForComparison[1], actualString)).Times(2).WillRepeatedly(Return(0.25F));
    EXPECT_CALL(*(MockErrorCalculator*)errorCalculatorForIdentification, CalculateError(stringsForComparison[2], actualString)).WillOnce(Return(0.5F));

    float expected = 0.0F;
    float actual = identificationErrorCalculator.CalculateError(expectedString, actualString);
    ASSERT_EQ(expected, actual);
}

TEST_F(IdentificationErrorCalculatorTest, CalculateErrorFailTest)
{
    IErrorCalculator *errorCalculatorForIdentification = new MockErrorCalculator;
    IdentificationErrorCalculator identificationErrorCalculator(std::auto_ptr<IErrorCalculator>(errorCalculatorForIdentification),
                                                                stringsForComparison);
    std::string expectedString = stringsForComparison[0];
    std::string actualString = "string 12";

    EXPECT_CALL(*(MockErrorCalculator*)errorCalculatorForIdentification, CalculateError(stringsForComparison[0], actualString)).Times(2).WillRepeatedly(Return(0.25F));
    EXPECT_CALL(*(MockErrorCalculator*)errorCalculatorForIdentification, CalculateError(stringsForComparison[1], actualString)).WillOnce(Return(0.25F));

    float expected = 1.0F;
    float actual = identificationErrorCalculator.CalculateError(expectedString, actualString);
    ASSERT_EQ(expected, actual);
}


