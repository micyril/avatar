#include <gtest/gtest.h>
#include <FilePathRepresentation/Experimenting/ErrorCalculators/EqualErrorCalculator.h>

class EqualErrorCalculatorTest : public ::testing::Test {};

TEST_F(EqualErrorCalculatorTest, CalculateErrorTest)
{
    std::string str1 = "string";
    std::string str2 = "string";
    std::string str3 = "string2";

    EqualErrorCalculator errorCalculator;
    ASSERT_EQ(0.0F, errorCalculator.CalculateError(str1, str2));
    ASSERT_EQ(1.0F, errorCalculator.CalculateError(str1, str3));
}

