#include <gtest/gtest.h>
#include <FilePathRepresentation/Tools/BinaryCoding.h>

class BinaryCodingTest : public ::testing::Test {};

TEST_F(BinaryCodingTest, CodeDecodeTest)
{
    std::string str = "some string";
    boost::dynamic_bitset<unsigned char> strBits = Tools::BinaryCoding::Code(str);
    std::string decodedStr = Tools::BinaryCoding::Decode(strBits);

    ASSERT_EQ(str, decodedStr);
}

TEST_F(BinaryCodingTest, CodeDecodeWithoutLastZerosTest)
{
    std::string str = "some string";
    boost::dynamic_bitset<unsigned char> strBits = Tools::BinaryCoding::Code(str + std::string("\0\0\0"));
    std::string decodedStr = Tools::BinaryCoding::Decode(strBits);

    ASSERT_EQ(str, decodedStr);
}

TEST_F(BinaryCodingTest, CalculateCountOfDifferentBitsTest)
{
    boost::dynamic_bitset<unsigned char> bits1(   std::string("0011010"));  //By these constructors
    boost::dynamic_bitset<unsigned char> bits2(   std::string("1111000"));  //bitsets will have bits
    boost::dynamic_bitset<unsigned char> bits3(std::string("1011111000"));  //in reverse order than in strings

    ASSERT_EQ(3, Tools::BinaryCoding::CalculateCountOfDifferentBits(bits1, bits2));
    ASSERT_EQ(5, Tools::BinaryCoding::CalculateCountOfDifferentBits(bits1, bits3));
    ASSERT_EQ(2, Tools::BinaryCoding::CalculateCountOfDifferentBits(bits2, bits3));
}
