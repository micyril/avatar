#include <gtest/gtest.h>
#include <FilePathRepresentation/Tools/RepresentationMakers/SimpleRepresentationMaker.h>
#include <FilePathRepresentation/Tools/BinaryCoding.h>

using namespace ::testing;

class SimpleRepresentationMakerTest : public ::testing::Test {};

TEST_F(SimpleRepresentationMakerTest, MakeTest)
{
    Tools::SimpleRepresentationMaker representationMaker(14);

    std::string input = "some string";
    for(int i = 0; i < 14; i++)
    {
        std::string actual = representationMaker.Make(input);
        ASSERT_EQ(2, actual.size());
        const boost::dynamic_bitset<unsigned char> bits = Tools::BinaryCoding::Code(actual);
        ASSERT_TRUE(bits[i]);
        for(int j = 0; j < 14; j++)
            if(i != j)
                ASSERT_FALSE(bits[j]);
    }
}

TEST_F(SimpleRepresentationMakerTest, MakeExceptionTest)
{
    Tools::SimpleRepresentationMaker representationMaker(16);

    std::string input = "some string";
    for(int i = 0; i < 16; i++)
        representationMaker.Make(input);
    ASSERT_THROW(representationMaker.Make(input), Tools::TooMuchMakeCallsException);
}
