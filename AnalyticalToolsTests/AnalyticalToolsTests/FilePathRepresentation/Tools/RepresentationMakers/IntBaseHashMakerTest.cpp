#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <FilePathRepresentation/Tools/RepresentationMakers/IntBaseHashMaker.h>

using namespace ::testing;

class MockIntHashProvider
{
public:
    MOCK_METHOD1(GetHash, int(const std::string &str));
};

class MockedIntHashProvider
{
private:
    MockIntHashProvider *mockIntHashProvider;

public:
    MockedIntHashProvider(MockIntHashProvider *mockIntHashProvider) :
        mockIntHashProvider(mockIntHashProvider) { }

    int operator()(const std::string& str)
    {
        return mockIntHashProvider->GetHash(str);
    }
};

class IntBaseHashMakerTest : public ::testing::Test {};

TEST_F(IntBaseHashMakerTest, MakeHashOfBase2LengthStrTest)
{
    MockIntHashProvider mockIntHashProvider;
    MockedIntHashProvider intHashProvider(&mockIntHashProvider);
    Tools::IntBaseHashMaker<int> hashMaker(intHashProvider, 12);
    std::string str = "1234";
    int hashRes1, hashRes2, hashRes3;
    char *charPtrToHashRes1 = (char *)&hashRes1, *charPtrToHashRes2 = (char *)&hashRes2, *charPtrToHashRes3 = (char *)&hashRes3;
    for(int i = 0; i < sizeof(4); i++)
    {
        charPtrToHashRes1[i] = '1';
        charPtrToHashRes2[i] = '2';
        charPtrToHashRes3[i] = '3';
    }

    EXPECT_CALL(mockIntHashProvider, GetHash("1234")).WillOnce(Return(hashRes1));
    EXPECT_CALL(mockIntHashProvider, GetHash("12")).WillOnce(Return(hashRes2));
    EXPECT_CALL(mockIntHashProvider, GetHash("34")).WillOnce(Return(hashRes3));

    std::string expected = "111122223333";
    std::string actual = hashMaker.Make(str);
    ASSERT_EQ(expected, actual);
}

TEST_F(IntBaseHashMakerTest, MakeHashOfNotBase2LengthStrTest)
{
    MockIntHashProvider mockIntHashProvider;
    MockedIntHashProvider intHashProvider(&mockIntHashProvider);
    Tools::IntBaseHashMaker<int> hashMaker(intHashProvider, 12);
    std::string str = "12345";
    int hashRes1, hashRes2, hashRes3;
    char *charPtrToHashRes1 = (char *)&hashRes1, *charPtrToHashRes2 = (char *)&hashRes2, *charPtrToHashRes3 = (char *)&hashRes3;
    for(int i = 0; i < sizeof(4); i++)
    {
        charPtrToHashRes1[i] = '1';
        charPtrToHashRes2[i] = '2';
        charPtrToHashRes3[i] = '3';
    }

    EXPECT_CALL(mockIntHashProvider, GetHash("12345")).WillOnce(Return(hashRes1));
    EXPECT_CALL(mockIntHashProvider, GetHash("12")).WillOnce(Return(hashRes2));
    EXPECT_CALL(mockIntHashProvider, GetHash("345")).WillOnce(Return(hashRes3));

    std::string expected = "111122223333";
    std::string actual = hashMaker.Make(str);
    ASSERT_EQ(expected, actual);
}

TEST_F(IntBaseHashMakerTest, MakeHashOfBigStrTest)
{
    MockIntHashProvider mockIntHashProvider;
    MockedIntHashProvider intHashProvider(&mockIntHashProvider);
    int size = 4 * 255;
    Tools::IntBaseHashMaker<int> hashMaker(intHashProvider, size - 10);
    std::string str(130, 'S');
    int hashRes;
    char *charPtrToHashRes = (char *)&hashRes;
    for(int i = 0; i < sizeof(4); i++)
        charPtrToHashRes[i] = '1';


    EXPECT_CALL(mockIntHashProvider, GetHash(_)).WillRepeatedly(Return(hashRes));


    std::string actual = hashMaker.Make(str);
    ASSERT_EQ(size, actual.size());
    for(int i = 0; i < size; i++)
        ASSERT_EQ('1', actual[i]);
}
