#include <boost/assign/list_of.hpp>
#include <gtest/gtest.h>
#include <FilePathRepresentation/Tools/RepresentationMakers/SimpleRepresentationMaker.h>
#include <FilePathRepresentation/Tools/BinaryCoding.h>
#include <FilePathRepresentation/Tools/IO.h>

using namespace ::testing;

class SimpleRepresentationMakerCreaterTest : public ::testing::Test
{
protected:
    std::string orignalsFileName;

    SimpleRepresentationMakerCreaterTest() :
        orignalsFileName("originals.txt") { }

    ~SimpleRepresentationMakerCreaterTest()
    {
        remove(orignalsFileName.c_str());
    }
};

TEST_F(SimpleRepresentationMakerCreaterTest, CreateTest)
{
    std::vector<std::string> originals = boost::assign::list_of("o1")("o2")("o3")("o4")("o5");
    EXPECT_TRUE(Tools::WriteAllLines(orignalsFileName, originals));
    boost::program_options::variables_map vm;
    vm.insert(std::make_pair("teaching-samples", boost::program_options::variable_value(orignalsFileName, false)));
    Tools::SimpleRepresentationMakerCreater representationMakerCreater;

    auto representationMaker = representationMakerCreater.Create(vm);

    std::string input = "some string";
    for(int i = 0; i < originals.size(); i++)
    {
        std::string actual = representationMaker->Make(input);
        ASSERT_EQ(1, actual.size());
        const boost::dynamic_bitset<unsigned char> bits = Tools::BinaryCoding::Code(actual);
        ASSERT_TRUE(bits[i]);
        for(int j = 0; j < originals.size(); j++)
            if(i != j)
                ASSERT_FALSE(bits[j]);
    }
}

