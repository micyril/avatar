#include <gtest/gtest.h>
#include <set>
#include <FilePathRepresentation/Tools/RepresentationMakers/RandomRepresentationMaker.h>

class RandomRepresentationhMakerTest : public ::testing::Test {};

TEST_F(RandomRepresentationhMakerTest, MakeTest)
{
    int length = 10;
    int representationsCount = 10;
    Tools::RandomRepresentationMaker representationMaker(length);
    auto o = new Tools::RandomRepresentationMakerCreater();

    std::set<std::string> representations;
    for(int i = 0; i < representationsCount; i++)
    {
        std::string representation = representationMaker.Make("");
        ASSERT_EQ(length, representation.length());
        representations.insert(representation);
    }

    ASSERT_EQ(representationsCount, representations.size());
}
