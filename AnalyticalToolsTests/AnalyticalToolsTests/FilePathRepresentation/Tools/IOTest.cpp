#include <gtest/gtest.h>
#include <boost/assign/list_of.hpp>
#include <FilePathRepresentation/Tools/IO.h>

class IOTest : public ::testing::Test
{
protected:
    std::string fileName;

    IOTest() : fileName("file.txt") { }

    ~IOTest()
    {
        remove(fileName.c_str());
    }
};

TEST_F(IOTest, WriteReadAllLinesTest)
{
    std::vector<std::string> lines = boost::assign::list_of("line 1")("line 2")("line 3");
    ASSERT_TRUE(Tools::WriteAllLines(fileName, lines));
    std::vector<std::string> actualLines;
    ASSERT_FALSE(Tools::ReadAllLines(std::string("notExistingFile.txt"), actualLines));
    ASSERT_EQ(0, actualLines.size());
    ASSERT_TRUE(Tools::ReadAllLines(fileName, actualLines));
    ASSERT_EQ(lines.size(), actualLines.size());
    for(int i = 0; i < lines.size(); i++)
        ASSERT_EQ(lines[i], actualLines[i]);
}
