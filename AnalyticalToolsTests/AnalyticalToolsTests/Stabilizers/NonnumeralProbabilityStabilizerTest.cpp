#include <gtest/gtest.h>
#include <Stabilizers/NonnumeralProbabilityStabilizer.h>

class NonnumeralProbabilityStabilizerTest : public ::testing::Test {};

TEST_F(NonnumeralProbabilityStabilizerTest, AddDataAndTryMakeHypothesisTest)
{
    NonnumeralProbabilityStabilizer<std::string, int> extentionStabilizer;
    int actual;

    ASSERT_FALSE(extentionStabilizer.TryMakeHypothesis("pdf", actual));

    extentionStabilizer.AddData("pdf", 1);
    extentionStabilizer.AddData("doc", 2);
    extentionStabilizer.TryMakeHypothesis("pdf", actual);
    ASSERT_EQ(1, actual);
    ASSERT_FALSE(extentionStabilizer.TryMakeHypothesis("txt", actual));

    extentionStabilizer.AddData("pdf", 2);
    extentionStabilizer.AddData("pdf", 2);
    extentionStabilizer.TryMakeHypothesis("pdf", actual);
    ASSERT_EQ(2, actual);
}

