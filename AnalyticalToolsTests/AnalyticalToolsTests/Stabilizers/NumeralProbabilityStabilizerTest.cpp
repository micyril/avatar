#include <gtest/gtest.h>
#include <Stabilizers/NumeralProbabilityStabilizer.h>

class NumeralProbabilityStabilizerTest : public ::testing::Test {};

TEST_F(NumeralProbabilityStabilizerTest, AddDataAndTryMakeHypothesisTest)
{
    NumeralProbabilityStabilizer<float, int> stabilizer(0.1F);
    int actual;

    ASSERT_FALSE(stabilizer.TryMakeHypothesis(0.3F, actual));

    stabilizer.AddData(0.1F, 1);
    stabilizer.AddData(0.2F, 1);
    stabilizer.TryMakeHypothesis(0.3F, actual);
    ASSERT_EQ(1, actual);

    stabilizer.AddData(0.4F, 2);
    stabilizer.TryMakeHypothesis(0.5F, actual);
    ASSERT_EQ(2, actual);
}
