#include <boost/assign/list_of.hpp>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <Stabilizers/FileAgents/CompositeFileAgentStabilizer.h>

class MockFileAgentStabilizer : public IFileAgentStabilizer
{
public:
    virtual bool TryMakeHypothesis(const FileInfo &fileInfo, std::auto_ptr<FileActionInfo> &actionInfoPtr)
    {
        if(IsAbleToMakeHypothesis(fileInfo))
        {
            actionInfoPtr.reset(new MovingActionInfo(MakeHypothesis(fileInfo)));
            return true;
        }
        return false;
    }

    MOCK_METHOD2(AddData, void(const FileInfo &fileInfo, const FileActionInfo &actionInfo));
    MOCK_METHOD1(IsAbleToMakeHypothesis, bool(const FileInfo &fileInfo));
    MOCK_METHOD1(MakeHypothesis, std::string(const FileInfo &fileInfo));
};

class CompositeFileAgentStabilizerTest : public ::testing::Test {};

using namespace ::testing;

TEST_F(CompositeFileAgentStabilizerTest, AddDataAndTryMakeHypothesisTest)
{
    MockFileAgentStabilizer mockStabilizer1, mockStabilizer2;
    float weightIncreasingCoefficient = 3.0F;
    CompositeFileAgentStabilizer compositeStabilizer(boost::assign::list_of(&mockStabilizer1)(&mockStabilizer2), weightIncreasingCoefficient);
    FileInfo fileInfo("pdf", 10, 100);
    MovingActionInfo actionInfo("aaa/aaa");

    EXPECT_CALL(mockStabilizer1, IsAbleToMakeHypothesis(Ref(fileInfo))).WillOnce(Return(true));
    EXPECT_CALL(mockStabilizer1, MakeHypothesis(Ref(fileInfo))).WillOnce(Return(actionInfo.location));
    EXPECT_CALL(mockStabilizer2, IsAbleToMakeHypothesis(Ref(fileInfo))).WillOnce(Return(false));
    EXPECT_CALL(mockStabilizer1, AddData(Ref(fileInfo), Ref(actionInfo)));
    EXPECT_CALL(mockStabilizer2, AddData(Ref(fileInfo), Ref(actionInfo)));

    compositeStabilizer.AddData(fileInfo, actionInfo);

    FileInfo fileInfo2("doc", 100, 10);
    MovingActionInfo hypothesis1("aaa/aaa");
    MovingActionInfo hypothesis2("aaa/bbb");

    EXPECT_CALL(mockStabilizer1, IsAbleToMakeHypothesis(Ref(fileInfo2))).WillRepeatedly(Return(true));
    EXPECT_CALL(mockStabilizer1, MakeHypothesis(Ref(fileInfo2))).WillRepeatedly(Return(hypothesis1.location));
    EXPECT_CALL(mockStabilizer2, IsAbleToMakeHypothesis(Ref(fileInfo2))).WillRepeatedly(Return(true));
    EXPECT_CALL(mockStabilizer2, MakeHypothesis(Ref(fileInfo2))).WillRepeatedly(Return(hypothesis2.location));

    int mockStabilizer1Calls = 0, mockStabilizer2Calls = 0;
    for(int i = 0; i < 1000; i++)
    {
        std::auto_ptr<FileActionInfo> actionInfoPtr;
        ASSERT_TRUE(compositeStabilizer.TryMakeHypothesis(fileInfo2, actionInfoPtr));
        if(((MovingActionInfo*)actionInfoPtr.get())->location == hypothesis1.location)
        {
            mockStabilizer1Calls++;
            continue;
        }
        if(((MovingActionInfo*)actionInfoPtr.get())->location == hypothesis2.location)
        {
            mockStabilizer2Calls++;
            continue;
        }
        FAIL();
    }

    EXPECT_GT(mockStabilizer1Calls, 700);
    EXPECT_LT(mockStabilizer1Calls, 800);
    EXPECT_GT(mockStabilizer2Calls, 200);
    EXPECT_LT(mockStabilizer2Calls, 300);
}
