#include <gtest/gtest.h>
#include <Stabilizers/FileAgents/CreationTimestampUsingStabilizer.h>

class CreationTimestampUsingStabilizerTest : public ::testing::Test {};

TEST_F(CreationTimestampUsingStabilizerTest, AddDataAndTryMakeHypothesisTest)
{
    CreationTimestampUsingStabilizer stabilizer;
    std::auto_ptr<FileActionInfo> actual;
    std::string location = "aaa/aaa";
    std::string location2 = "aaa/bbb";

    ASSERT_FALSE(stabilizer.TryMakeHypothesis(FileInfo("pdf", 10, 1), actual));

    stabilizer.AddData(FileInfo("pdf", 10, 1), MovingActionInfo(location));
    stabilizer.AddData(FileInfo("pdf", 10, 2), MovingActionInfo(location));
    stabilizer.TryMakeHypothesis(FileInfo("pdf", 10, 3), actual);
    ASSERT_EQ(location, ((MovingActionInfo*)actual.get())->location);

    stabilizer.AddData(FileInfo("pdf", 10, 4), MovingActionInfo(location2));
    stabilizer.TryMakeHypothesis(FileInfo("pdf", 10, 5), actual);
    ASSERT_EQ(location2, ((MovingActionInfo*)actual.get())->location);
}
