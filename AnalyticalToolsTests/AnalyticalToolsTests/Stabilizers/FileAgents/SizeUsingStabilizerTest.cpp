#include <gtest/gtest.h>
#include <Stabilizers/FileAgents/SizeUsingStabilizer.h>

class SizeUsingStabilizerTest : public ::testing::Test {};

TEST_F(SizeUsingStabilizerTest, AddDataAndTryMakeHypothesisTest)
{
    SizeUsingStabilizer stabilizer;
    std::auto_ptr<FileActionInfo> actual;
    std::string location = "aaa/aaa";
    std::string location2 = "aaa/bbb";

    ASSERT_FALSE(stabilizer.TryMakeHypothesis(FileInfo("pdf", 10, 1), actual));

    stabilizer.AddData(FileInfo("pdf", 10, 1), MovingActionInfo(location));
    stabilizer.AddData(FileInfo("pdf", 20, 1), MovingActionInfo(location));
    stabilizer.TryMakeHypothesis(FileInfo("pdf", 30, 3), actual);
    ASSERT_EQ(location, ((MovingActionInfo*)actual.get())->location);

    stabilizer.AddData(FileInfo("pdf", 40, 1), MovingActionInfo(location2));
    stabilizer.TryMakeHypothesis(FileInfo("pdf", 50, 1), actual);
    ASSERT_EQ(location2, ((MovingActionInfo*)actual.get())->location);
}

