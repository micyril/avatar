#include <gtest/gtest.h>
#include <Stabilizers/FileAgents/ExtentionUsingStabilizer.h>

class ExtentionUsingStabilizerTest : public ::testing::Test {};

TEST_F(ExtentionUsingStabilizerTest, AddDataAndTryMakeHypothesisTest)
{
    ExtentionUsingStabilizer stabilizer;
    std::auto_ptr<FileActionInfo> actual;
    std::string location = "aaa/aaa";
    std::string location2 = "aaa/bbb";

    ASSERT_FALSE(stabilizer.TryMakeHypothesis(FileInfo("pdf", 10, 1), actual));

    stabilizer.AddData(FileInfo("pdf", 10, 1), MovingActionInfo(location));
    stabilizer.AddData(FileInfo("doc", 10, 1), MovingActionInfo(location2));
    stabilizer.TryMakeHypothesis(FileInfo("pdf", 10, 1), actual);
    ASSERT_EQ(location, ((MovingActionInfo*)actual.get())->location);

    stabilizer.AddData(FileInfo("pdf", 10, 1), MovingActionInfo(location2));
    stabilizer.AddData(FileInfo("pdf", 10, 1), MovingActionInfo(location2));
    stabilizer.TryMakeHypothesis(FileInfo("pdf", 10, 1), actual);
    ASSERT_EQ(location2, ((MovingActionInfo*)actual.get())->location);
}
