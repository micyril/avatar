TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    FilePathRepresentation/Tools/BinaryCodingTests.cpp \
    FilePathRepresentation/Experimenting/TestDriverTest.cpp \
    FilePathRepresentation/Tools/IOTest.cpp \
    FilePathRepresentation/RepresentationProvider/BAM/BAMRepresentationProviderTest.cpp \
    FilePathRepresentation/RepresentationProvider/BAM/BAMRepresentationProviderCreaterTest.cpp \
    FilePathRepresentation/Experimenting/ExperimentDriverTest.cpp \
    FilePathRepresentation/Experimenting/ErrorCalculators/BitsErrorCalculatorTest.cpp \
    FilePathRepresentation/Experimenting/ErrorCalculators/LengthErrorCalculatorTest.cpp \
    FilePathRepresentation/Experimenting/ErrorCalculators/IdentificationErrorCalculator.cpp \
    FilePathRepresentation/Experimenting/ErrorCalculators/IdentificationErrorCalculatorCreaterTest.cpp \
    FilePathRepresentation/Tools/RepresentationMakers/IntBaseHashMakerTest.cpp \
    FilePathRepresentation/Tools/RepresentationMakers/SimpleRepresentationMakerTest.cpp \
    FilePathRepresentation/Tools/RepresentationMakers/SimpleRepresentationMakerCreaterTest.cpp \
    FilePathRepresentation/Experimenting/ErrorCalculators/LevenshteinDistanceCalculatorTest.cpp \
    FilePathRepresentation/Tools/RepresentationMakers/RandomRepresentationMakerTest.cpp \
    FilePathRepresentation/Experimenting/ErrorCalculators/EqualErrorCalculatorTest.cpp \
    Stabilizers/NumeralProbabilityStabilizerTest.cpp \
    Stabilizers/NonnumeralProbabilityStabilizerTest.cpp \
    Stabilizers/FileAgents/CreationTimestampUsingStabilizerTest.cpp \
    Stabilizers/FileAgents/SizeUsingStabilizerTest.cpp \
    Stabilizers/FileAgents/ExtentionUsingStabilizerTest.cpp \
    Stabilizers/FileAgents/CompositeFileAgentStabilizerTest.cpp


unix: LIBS += -L$$PWD/../../gmock/Release-libc++/ -lGMockBuilder

INCLUDEPATH += $$PWD/../../gmock/gmock-1.7.0/fused-src
DEPENDPATH += $$PWD/../../gmock/gmock-1.7.0/fused-src

unix: PRE_TARGETDEPS += $$PWD/../../gmock/Release-libc++/libGMockBuilder.a


unix: LIBS += -L$$PWD/../../AnalyticalTools/Debug/ -lAnalyticalTools

INCLUDEPATH += $$PWD/../../AnalyticalTools/AnalyticalTools
DEPENDPATH += $$PWD/../../AnalyticalTools/AnalyticalTools

unix: PRE_TARGETDEPS += $$PWD/../../AnalyticalTools/Debug/libAnalyticalTools.a

LIBS += -lboost_program_options -lopencv_core
QMAKE_CXXFLAGS += -std=c++11
