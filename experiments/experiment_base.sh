#!/bin/bash

method=BAM
format=BEC
teachingSs=samples/teachingSamples.txt
testingSs=samples/testingSamples.txt
reprMaker=boost
reprSize=26

PathRepresentationExperimenting/Release/PathRepresentationExperimenting -M $method -F $format -S $teachingSs -T $testingSs --repr-maker $reprMaker --repr-size $reprSize
