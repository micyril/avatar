/****************************************************************************
**
**
** This header file defines common exceptions
**
**
****************************************************************************/

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <sstream>
#include <exception>
#include <string>

class CanNotWriteXmlException : public std::exception
{
private:
    std::string message;
public:
    CanNotWriteXmlException(std::string &path)
    {
        std::ostringstream ss;
        ss << "Error while trying to write the file " << path;
        message = ss.str();
    }

    virtual ~CanNotWriteXmlException() throw() {}

    virtual const char* what() const throw()
    {
        return message.c_str();
    }
};

#endif // EXCEPTIONS_H
