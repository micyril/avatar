#include "../Core/Tools.h"
#include "TaskPerformer.h"

void SingleTaskPerfomer::run()
{
    currentTask->Perform();
}

SingleTaskPerfomer::~SingleTaskPerfomer()
{
    QThread::wait();
}

void SingleTaskPerfomer::StartPerforming(ITask *task)
{
    currentTask = task;
    QThread::start();
}

bool TaskPerformer::tryGetNextTask(ITask *&result)
{
    QMutexLocker queueLocker(&queueMutex);
    if(tasksQueue.size() == 0)
        return false;
    result = tasksQueue.front();
    tasksQueue.pop();
    return true;
}

void TaskPerformer::run()
{
    ITask* nextTask;
    while(tryGetNextTask(nextTask))
    {
        nextTask->Perform();
        delete nextTask;
        QMutexLocker countLocker(&notExecutedTasksCountMutex);
        notExecutedTasksCount--;
    }
}

TaskPerformer::TaskPerformer() :
    notExecutedTasksCount(0), isStopped(false) { }

TaskPerformer::~TaskPerformer()
{
    QThread::wait();
}

void TaskPerformer::AddTask(ITask *task)
{
    QMutexLocker queueLocker(&queueMutex);
    tasksQueue.push(task);
    QMutexLocker countLocker(&notExecutedTasksCountMutex);
    notExecutedTasksCount++;
    if(!QThread::isRunning())
        QThread::start();
}

int TaskPerformer::GetNotExecutedTasksCount()
{
    QMutexLocker countLocker(&notExecutedTasksCountMutex);
    return notExecutedTasksCount;
}
