/****************************************************************************
**
**
** This header file defines functions that can be useful in several places
**
**
****************************************************************************/

#ifndef TOOLS_H
#define TOOLS_H

#include <string>

namespace Tools
{
long long getTimestamp();
void sleepMilliseconds(unsigned long milliseconds);
bool isRelationTrue(const std::string& path, const std::string& derivedPath);
}

#endif // TOOLS_H
