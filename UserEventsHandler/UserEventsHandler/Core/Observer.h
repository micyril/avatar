/****************************************************************************
**
**
** This header file defines the widely used design pattern Observer
**
**
****************************************************************************/

#ifndef OBSERVER_H
#define OBSERVER_H

#include <list>

template <typename T>
class Observer
{
public:
    virtual ~Observer() {}
    virtual void Update(const T& data) = 0;
};

template <typename T>
class Observable
{
private:
    std::list<Observer<T>*> observers;

public:
    virtual ~Observable() {}

    void RegisterObserver(Observer<T> *observer)
    {
        observers.push_back(observer);
    }

    void RemoveObserver(Observer<T> *observer)
    {
        observers.remove(observer);
    }

    void NotifyObservers(const T& data)
    {
        for (auto it = observers.begin(); it != observers.end(); it++)
            (*it)->Update(data);
    }
};

#endif // OBSERVER_H
