/****************************************************************************
**
**
** This header file defines classes providing asynchronous performing tasks
**
**
****************************************************************************/

#ifndef TASKPERFORMER_H
#define TASKPERFORMER_H

#include <queue>
#include <QThread>
#include <QMutex>

class ITask
{
public:
    virtual ~ITask() {}
    virtual void Perform() = 0;
};

class SingleTaskPerfomer : private QThread
{
private:
    ITask *currentTask;

    virtual void run();

public:
    ~SingleTaskPerfomer();
    void StartPerforming(ITask *task);
};

class TaskPerformer : private QThread
{
private:
    int notExecutedTasksCount;
    std::queue<ITask*> tasksQueue;
    QMutex queueMutex;
    QMutex notExecutedTasksCountMutex;
    bool isStopped;

    bool tryGetNextTask(ITask* &result);
    virtual void run();

public:
    TaskPerformer();
    ~TaskPerformer();
    void AddTask(ITask* task);
    int GetNotExecutedTasksCount();
};


#endif // TASKPERFORMER_H
