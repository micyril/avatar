#include <QDir>
#include <sys/time.h>
#include <unistd.h>
#include "Tools.h"

long long Tools::getTimestamp()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (long long)tv.tv_sec * 1000000 + (long long)tv.tv_usec;
}

void Tools::sleepMilliseconds(unsigned long milliseconds)
{
    usleep(milliseconds * 1000);
}

bool Tools::isRelationTrue(const std::string &path, const std::string &derivedPath)
{
    static const char pathSeparator = QDir::separator().toAscii();
    if(derivedPath.length() <= path.length())
        return false;
    if(derivedPath.compare(0, path.length(), path) != 0)
        return false;
    if(derivedPath[path.length()] != pathSeparator)
        return false;
    return true;
}
