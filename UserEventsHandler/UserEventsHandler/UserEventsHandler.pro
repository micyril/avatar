#-------------------------------------------------
#
# Project created by QtCreator 2013-11-28T08:29:04
#
#-------------------------------------------------

QT += core widgets
QT -= gui

TARGET = UserEventsHandler
TEMPLATE = lib
CONFIG += staticlib

LIBS += -framework ApplicationServices -framework CoreServices

SOURCES += \
    FileSystem/MacFSConnector.cpp \
    Mouse/MouseXmlLogger.cpp \
    Mouse/MacMouseConnector.cpp \
    Screenshots/ScreenshotLogger.cpp \
    Screenshots/MacScreenshotMaker.cpp \
    Windows/MacWindowFinder.cpp \
    FileSystem/FileTreeStorage.cpp \
    Core/Tools.cpp \
    Core/TaskPerformer.cpp \
    FileSystem/FSEventsHandler.cpp \
    FileSystem/FSEventsObserver/FileSystemXmlLogger.cpp \
    FileSystem/FSEventsObserver/FSEventsFilter.cpp \
    FileSystem/FSEventsObserver/MacDownloadedFileEventConverter.cpp

HEADERS += \
    Core/Observer.h \
    FileSystem/MacFSConnector.h \
    FileSystem/FileSystemEvent.h \
    Mouse/MouseXmlLogger.h \
    Mouse/MouseEventObserver.h \
    Mouse/MouseEvent.h \
    Mouse/MacMouseConnector.h \
    Screenshots/ScreenshotLogger.h \
    Screenshots/MacScreenshotMaker.h \
    Screenshots/IScreenshotMacker.h \
    Windows/WindowInfo.h \
    Windows/Rectangle.h \
    Windows/MacWindowFinder.h \
    Windows/IWindowFinder.h \
    FileSystem/FileTreeStorage.h \
    FileSystem/FSEventsHandler.h \
    Core/Tools.h \
    Core/Exceptions.h \
    Core/TaskPerformer.h \
    FileSystem/FSEventsObserver/FileSystemXmlLogger.h \
    FileSystem/FSEventsObserver/FSEventsFilter.h \
    FileSystem/FSEventsObserver/FSEventsObserverDecorator.h \
    FileSystem/FSEventsObserver/Exceptions.h \
    FileSystem/FSEventsObserver/MacDownloadedFileEventConverter.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
