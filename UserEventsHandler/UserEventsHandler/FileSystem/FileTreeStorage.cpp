#include <sys/stat.h>
#include <iostream>
#include <QDir>
#include <sstream>
#include <vector>
#include <boost/algorithm/string.hpp>
#include "FileTreeStorage.h"

FileTreeStorage::FileTreeStorage() :
    fileTreeStorageRaw(new FileTreeStorageRaw()) {}

void FileTreeStorage::Init(const std::string &path)
{
    fileTreeStorageRaw->Init(path);
    rootFullPath = path;
}

bool FileTreeStorage::TryGetInode(const std::string &path, ino_t &result) const
{
    FileNode *node;
    if(fileTreeStorageRaw->FindFileNode(path, node) == false)
        return false;
    result = node->inode;
    return true;
}

bool FileTreeStorage::TryGetPaths(const ino_t inode, std::list<std::string> &result) const
{
    const std::list<FileNode *> *nodes = NULL;
    if(fileTreeStorageRaw->FindFileNodes(inode, nodes) == false)
        return false;
    for(auto it = nodes->begin(); it != nodes->end(); it++)
        result.push_back(fileTreeStorageRaw->GetFullPath(*it));
    return true;
}

bool FileTreeStorage::Remove(const std::string &path)
{
    FileNode *node;
    if(fileTreeStorageRaw->FindFileNode(path, node) == false)
        return false;
    fileTreeStorageRaw->DeleteNode(node);  //todo: what should be done if node is root?
    return true;
}

bool FileTreeStorage::Add(const std::string &path, const ino_t inode)
{
    std::vector<std::string> relativePathElements;
    if(fileTreeStorageRaw->CheckPathValidityAndGetRelativePathElements(path, relativePathElements) == false)
        return false;
    if(path.size() == rootFullPath.size())
        return false;
    FileNode *parent;
    if(fileTreeStorageRaw->FindParentFileNode(relativePathElements, parent) == false)
        return false;
    FileNode *newNode;
    return fileTreeStorageRaw->AddFileNode(parent, relativePathElements[relativePathElements.size() - 1], inode, newNode);
}

bool FileTreeStorage::Move(const std::string &path, const std::string &newPath)
{
    FileNode *node;
    if(fileTreeStorageRaw->FindFileNode(path, node) == false)
        return false;
    FileNode *newParent;
    std::vector<std::string> relativePathElements;
    if(fileTreeStorageRaw->CheckPathValidityAndGetRelativePathElements(newPath, relativePathElements) == false)
        return false;
    if(fileTreeStorageRaw->FindParentFileNode(relativePathElements, newParent) == false)
        return false;
    return fileTreeStorageRaw->ChangeFileNode(node, newParent, relativePathElements[relativePathElements.size() - 1]);
}


FileTreeStorageRaw::FileTreeStorageRaw() : root(NULL) {}

FileTreeStorageRaw::~FileTreeStorageRaw()
{
    if(root != NULL)
        DeleteNode(root);
}

void FileTreeStorageRaw::Init(const std::string &path)
{
    if(root != NULL)
        throw InitializationException("Initialization has already been"); //todo: preventing memory leak
    rootFullPath = path;
    root = init(NULL, QFileInfo(QString::fromUtf8(path.c_str())));
}

bool FileTreeStorageRaw::AddFileNode(FileNode * const parent, const std::string &fileName, const ino_t inode, FileNode *&newNode)
{
    newNode = new FileNode(fileName, inode, parent);
    if(parent != NULL)
    {
        if(parent->children == NULL)
            parent->children = new std::map<std::string, FileNode*>();
        std::map<std::string, FileNode*>::iterator it = parent->children->find(fileName);
        if(it != parent->children->end())
            return false;
        parent->children->insert(std::pair<std::string, FileNode*>(fileName, newNode));
    }

    std::pair<inodeToFileNodesIter, bool> insertRes;
    insertRes.first = inodeToFileNodes.find(inode);
    if(insertRes.first == inodeToFileNodes.end())
        insertRes = inodeToFileNodes.insert(std::pair< ino_t, std::list<FileNode*> >(inode, std::list<FileNode*>()));
    insertRes.first->second.push_back(newNode);

    return true;
}

void FileTreeStorageRaw::DeleteNode(FileNode *node)
{
    if(node->children != NULL)
    {
        while(node->children->size() > 0)
            DeleteNode(node->children->begin()->second);
        delete node->children;
    }
    int count;

    inodeToFileNodesIter it = inodeToFileNodes.find(node->inode);
    if(it == inodeToFileNodes.end() || it->second.size() == 0)
    {
        std::stringstream ss;
        ss << "Expected at least 1 entry for inode " << node->inode << ", but actually 0";
        throw DataIntegrityException(ss.str());
    }
    if(it->second.size() == 1)
        inodeToFileNodes.erase(it);
    else
        it->second.remove(node);

    if(node->parent != NULL && (count = node->parent->children->erase(node->fileName)) != 1)
    {
        std::stringstream ss;
        ss << "Expected 1 entry for fileName " << node->fileName << ", but actually " << count;
        throw DataIntegrityException(ss.str());
    }

    delete node;
}

FileNode *FileTreeStorageRaw::init(FileNode * const parent, const QFileInfo &fileInfo)
{
    static struct stat st;
    std::string fullPath = fileInfo.absoluteFilePath().toUtf8().constData();
    if(lstat(fullPath.c_str(), &st))
    {
        std::stringstream ss;
        ss << "Error while getting stat of the file " << fullPath;
        throw InitializationException(ss.str());
    }
    FileNode *node;
    std::string fileName = fileInfo.fileName().toUtf8().constData();
    if(AddFileNode(parent, fileName, st.st_ino, node) == false)
    {
        std::stringstream ss;
        ss << "Error while adding entry for file " << fullPath;
        throw InitializationException(ss.str());
    }
    if(fileInfo.isDir() && !fileInfo.isSymLink())
    {
        QDir dir(fileInfo.absoluteFilePath());
        dir.setFilter(QDir::Dirs | QDir::Files | QDir::Hidden | QDir::NoDotAndDotDot);
        QFileInfoList fileList = dir.entryInfoList();
        for(int i = 0; i < fileList.size(); i++)
            init(node, fileList.at(i));
    }

    return node;
}

void FileTreeStorageRaw::getFullPath(const FileNode * const node, std::ostringstream &ss) const
{
    if(node == root)
    {
        ss << rootFullPath;
        return;
    }
    getFullPath(node->parent, ss);
    ss << pathSeparator << node->fileName;
}

std::string FileTreeStorageRaw::GetFullPath(const FileNode * const node) const
{
    std::ostringstream ss;
    getFullPath(node, ss);
    return ss.str();
}

bool FileTreeStorageRaw::FindFileNode(const std::string &path, FileNode *&result) const
{
    std::vector<std::string> relativePathElements;
    if(CheckPathValidityAndGetRelativePathElements(path, relativePathElements) == false)
        return false;
    if(path.size() == rootFullPath.size())
    {
        result = root;
        return true;
    }
    FileNode *parent;
    if(FindParentFileNode(relativePathElements, parent) == false || parent->children == NULL)
        return false;
    std::map<std::string, FileNode*>::const_iterator nodeIt = parent->children->find(relativePathElements[relativePathElements.size() - 1]);
    if(nodeIt == parent->children->end())
        return false;
    result = nodeIt->second;
    return true;
}

bool FileTreeStorageRaw::FindFileNodes(const ino_t inode, const std::list<FileNode *> *&result) const
{
    inodeToFileNodesConstIter it = inodeToFileNodes.find(inode);
    if(it == inodeToFileNodes.end())
        return false;
    result = &(it->second);
    return true;
}

bool FileTreeStorageRaw::FindParentFileNode(std::vector<std::string> &relativePathElements, FileNode *&result) const
{
    if(relativePathElements.size() == 1)
        return false;
    result = root;
    std::map<std::string, FileNode*>::iterator it;
    for(size_t i = 1, lastIndex = relativePathElements.size() - 2; i <= lastIndex; i++)
    {
        if(result->children == NULL || (it = result->children->find(relativePathElements[i])) == result->children->end())
            return false;
        result = it->second;
    }
    return true;
}

bool FileTreeStorageRaw::CheckPathValidityAndGetRelativePathElements(const std::string &path, std::vector<std::string> &relativePathElements) const
{
    if(path.compare(0, rootFullPath.size(), rootFullPath) != 0)
        return false;
    if(path.size() == rootFullPath.size())
        return true;

    std::string relativePath = path.substr(rootFullPath.size());
    boost::split(relativePathElements, relativePath, boost::is_any_of(pathSeparator));

    if(relativePathElements.size() == 1)
        return false;
    if(relativePathElements[relativePathElements.size() - 1] == "")
        return false;
    return true;
}

bool FileTreeStorageRaw::ChangeFileNode(FileNode *node, FileNode *newParent, const std::string &newFileName)
{
    if(node == root)
        return false;
    int count;
    if(node->parent != NULL && (count = node->parent->children->erase(node->fileName)) != 1)
    {
        std::stringstream ss;
        ss << "Expected 1 entry for fileName " << node->fileName << ", but actually " << count;
        throw DataIntegrityException(ss.str());
    }
    node->fileName = newFileName;
    node->parent = newParent;
    if(newParent->children == NULL)
        newParent->children = new std::map<std::string, FileNode*>();
    newParent->children->insert(std::pair<std::string, FileNode*>(newFileName, node));
    return true;
}


