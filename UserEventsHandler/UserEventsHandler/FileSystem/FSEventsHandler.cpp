#include "../Core/Tools.h"
#include "FSEventsHandler.h"

bool FSEventsHandler::tryGetOldPath(const ino_t inode, const std::string &path, std::string &oldPath)
{
    std::list<std::string> inodePaths;
    if(fileTreeStorage->TryGetPaths(inode, inodePaths))
    {
        struct stat st;
        for(auto it = inodePaths.begin(); it != inodePaths.end(); it++)
            if (lstat(it->c_str(), &st) != 0 || st.st_ino != inode)
            {
                oldPath = *it;
                return true;
            }
        oldPath = path;
    }
    return false;
}

void FSEventsHandler::handleMovedOrCreatedEvent(const std::string &path, const ino_t inode, long long timestamp)
{
    std::string oldPath;
    if(tryGetOldPath(inode, path, oldPath))
    {
        handleRemovedTaskBeforeHandlingMovedEvent(oldPath);
        handleMovedEvent(oldPath, path, timestamp);
    }
    else
        handleCreatedEvent(path, inode, timestamp);
}

void FSEventsHandler::handleMovedOrRemovedEvent(const std::string &path, long long timestamp)
{
    registeringRemovedEventTask->DoWork();
    registeringRemovedEventTask->SetFields(path, timestamp);
    singleTaskPerfomer.StartPerforming(registeringRemovedEventTask.get());
}

void FSEventsHandler::handleRemovedEvent(const std::string &path, long long timestamp)
{
    registeringRemovedEventTask->DoWork();
    if(fileTreeStorage->Remove(path))
        taskPerformer.AddTask(new NotifyFSObserversTask(new FileSystemEvent(FileSystemEvent::Removed, path, timestamp), observable));
}

void FSEventsHandler::handleCreatedEvent(const std::string &path, const ino_t inode, long long timestamp)
{
    registeringRemovedEventTask->DoWork();
    if(fileTreeStorage->Add(path, inode))
        taskPerformer.AddTask(new NotifyFSObserversTask(new FileSystemEvent(FileSystemEvent::Created, path, timestamp), observable));
}

void FSEventsHandler::handleMovedEvent(const std::string &oldPath, const std::string &newPath, long long timestamp)
{
    if(fileTreeStorage->Move(oldPath, newPath))
        taskPerformer.AddTask(new NotifyFSObserversTask(new MovedEvent(oldPath, newPath, timestamp), observable));
}

void FSEventsHandler::handleRemovedTaskBeforeHandlingMovedEvent(const std::string &oldPath)
{
    if(registeringRemovedEventTask->GetPath() == oldPath)
        registeringRemovedEventTask->SetPerformed();
    else
        registeringRemovedEventTask->DoWork();
}

void FSEventsHandler::handleRemovedPlusMovedOrCreatedEvent(const std::string &path, const ino_t inode, long long timestamp)
{
    std::string oldPath;
    if(tryGetOldPath(inode, path, oldPath))
    {
        handleRemovedTaskBeforeHandlingMovedEvent(oldPath);
        handleRemovedEvent(path, timestamp);
        handleMovedEvent(oldPath, path, timestamp);
    }
    else
    {
        handleRemovedEvent(path, timestamp);
        handleCreatedEvent(path, inode, timestamp);
    }
}

FSEventsHandler::FSEventsHandler(FileTreeStorage *fileTreeStorage, Observable<FileSystemEvent> *observable, unsigned long latencyMilliseconds) :
    fileTreeStorage(fileTreeStorage), observable(observable),
    registeringRemovedEventTask(new RegisteringRemovedEventTask(fileTreeStorage, observable, &taskPerformer, 2 * latencyMilliseconds)) {}

void FSEventsHandler::Handle(const std::string &path, long long timestamp)
{
    struct stat st;
    ino_t fileOldInode;
    bool inRealFSExists = lstat(path.c_str(), &st) == 0;
    bool inFileTreeStorageExists = fileTreeStorage->TryGetInode(path, fileOldInode);

    if(inRealFSExists && !inFileTreeStorageExists)
        handleMovedOrCreatedEvent(path, st.st_ino, timestamp);
    if(!inRealFSExists && inFileTreeStorageExists)
        handleMovedOrRemovedEvent(path, timestamp);
    if(inRealFSExists && inFileTreeStorageExists && st.st_ino != fileOldInode)
        handleRemovedPlusMovedOrCreatedEvent(path, st.st_ino, timestamp);
}

NotifyFSObserversTask::NotifyFSObserversTask(FileSystemEvent *event, Observable<FileSystemEvent> *observable) :
    event(event), observable(observable) { }

void NotifyFSObserversTask::Perform()
{
    observable->NotifyObservers(*(event.get()));
}

RegisteringRemovedEventTask::RegisteringRemovedEventTask(FileTreeStorage *fileTreeStorage, Observable<FileSystemEvent> *observable, TaskPerformer *taskPerformer, unsigned long millisecondsSleepPeriod) :
    fileTreeStorage(fileTreeStorage), observable(observable), taskPerformer(taskPerformer), millisecondsSleepPeriod(millisecondsSleepPeriod), isPerformed(true) { }

void RegisteringRemovedEventTask::SetFields(const std::string &path, long long timestamp)
{
    this->path = path;
    this->timestamp = timestamp;
    isPerformed = false;
}

const std::string &RegisteringRemovedEventTask::GetPath() const
{
    return path;
}

void RegisteringRemovedEventTask::SetPerformed()
{
    QMutexLocker locker(&mutex);
    isPerformed = true;
}

bool RegisteringRemovedEventTask::IsPerformed()
{
    QMutexLocker locker(&mutex);
    return isPerformed;
}

void RegisteringRemovedEventTask::Perform()
{
    Tools::sleepMilliseconds(millisecondsSleepPeriod);
    DoWork();
}

void RegisteringRemovedEventTask::DoWork()
{
    QMutexLocker locker(&mutex);
    if(!isPerformed)
    {
        if(fileTreeStorage->Remove(path))
            taskPerformer->AddTask(new NotifyFSObserversTask(new FileSystemEvent(FileSystemEvent::Removed, path, timestamp), observable));
        isPerformed = true;
    }
}
