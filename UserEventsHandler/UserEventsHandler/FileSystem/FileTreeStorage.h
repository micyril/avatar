/****************************************************************************
**
**
** This header file defines classes providing creating, processing and
** changing the file system snapshots
**
**
****************************************************************************/

#ifndef INODESTORAGE_H
#define INODESTORAGE_H

#include <map>
#include <vector>
#include <list>
#include <exception>
#include <boost/unordered_map.hpp>
#include <sys/stat.h>

class FileTreeStorageRaw;

class FileTreeStorage
{
private:
    std::auto_ptr<FileTreeStorageRaw> fileTreeStorageRaw;
    std::string rootFullPath;

public:
    FileTreeStorage();
    void Init(const std::string &path);
    bool TryGetInode(const std::string &path, ino_t& result) const;
    bool TryGetPaths(const ino_t inode, std::list<std::string> &result) const;
    bool Remove(const std::string &path);
    bool Add(const std::string &path, const ino_t inode);
    bool Move(const std::string &path, const std::string &newPath);
};

struct FileNode
{
    std::string fileName;
    ino_t inode;
    std::map<std::string, FileNode*> *children;
    FileNode *parent;

    FileNode(const std::string &fileName, const ino_t inode, FileNode* const parent) :
        fileName(fileName), inode(inode), children(NULL), parent(parent) {}
};

class QFileInfo;

class FileTreeStorageRaw
{
private:
    typedef boost::unordered_map< ino_t, std::list<FileNode*> >::iterator inodeToFileNodesIter;
    typedef boost::unordered_map< ino_t, std::list<FileNode*> >::const_iterator inodeToFileNodesConstIter;

    const std::string pathSeparator = "/";

    FileNode *root;
    std::string rootFullPath;
    boost::unordered_map< ino_t, std::list<FileNode*> > inodeToFileNodes;

    FileNode* init(FileNode* const parent, const QFileInfo &fileInfo);
    void getFullPath(const FileNode * const node, std::ostringstream &ss) const;

public:
    FileTreeStorageRaw();
    ~FileTreeStorageRaw();
    void Init(const std::string &path);
    bool AddFileNode(FileNode* const parent, const std::string &fileName, const ino_t inode, FileNode* &newNode);
    void DeleteNode(FileNode *node);
    std::string GetFullPath(const FileNode* const node) const;
    bool FindFileNode(const std::string &path, FileNode* &result) const;
    bool FindFileNodes(const ino_t inode, const std::list<FileNode *>* &result) const;
    bool FindParentFileNode(std::vector<std::string> &relativePathElements, FileNode* &result) const;
    bool CheckPathValidityAndGetRelativePathElements(const std::string &path, std::vector<std::string>& relativePathElements) const;
    bool ChangeFileNode(FileNode* node, FileNode *newParent, const std::string &newFileName);
};

class InitializationException : public std::exception
{
private:
    std::string message;

public:
    InitializationException(std::string message) :
        message(message) {}
    virtual ~InitializationException() throw() {}

    virtual const char* what() const throw()
    {
        return message.c_str();
    }
};

class DataIntegrityException : public std::exception
{
private:
    std::string message;

public:
    DataIntegrityException(std::string message) :
        message(message) {}
    virtual ~DataIntegrityException() throw() {}

    virtual const char* what() const throw()
    {
        return message.c_str();
    }
};

#endif // INODESTORAGE_H
