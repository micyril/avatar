/****************************************************************************
**
**
** This header file defines classes to provide handling file pathes related
** to file system events
**
**
****************************************************************************/

#ifndef FSEVENTSHANDLER_H
#define FSEVENTSHANDLER_H

#include <QMutex>
#include "../Core/TaskPerformer.h"
#include "../Core/Observer.h"
#include "FileSystemEvent.h"
#include "FileTreeStorage.h"

class RegisteringRemovedEventTask;

class FSEventsHandler
{
private:
    FileTreeStorage *fileTreeStorage;
    Observable<FileSystemEvent> *observable;
    TaskPerformer taskPerformer;
    std::auto_ptr<RegisteringRemovedEventTask> registeringRemovedEventTask;
    SingleTaskPerfomer singleTaskPerfomer;

    bool tryGetOldPath(const ino_t inode, const std::string &path, std::string &oldPath);
    void handleMovedOrCreatedEvent(const std::string &path, const ino_t inode, long long timestamp);
    void handleMovedOrRemovedEvent(const std::string &path, long long timestamp);
    void handleRemovedEvent(const std::string &path, long long timestamp);
    void handleCreatedEvent(const std::string &path, const ino_t inode, long long timestamp);
    void handleMovedEvent(const std::string &oldPath, const std::string &newPath, long long timestamp);
    void handleRemovedTaskBeforeHandlingMovedEvent(const std::string &oldPath);
    void handleRemovedPlusMovedOrCreatedEvent(const std::string &path, const ino_t inode, long long timestamp);

public:
    FSEventsHandler(FileTreeStorage *fileTreeStorage, Observable<FileSystemEvent> *observable, unsigned long latencyMilliseconds);
    void Handle(const std::string& path, long long timestamp);
};

class NotifyFSObserversTask : public ITask
{
private:
    std::auto_ptr<FileSystemEvent> event;
    Observable<FileSystemEvent> *observable;

public:
    NotifyFSObserversTask(FileSystemEvent *event, Observable<FileSystemEvent> *observable);
    virtual void Perform();
};


class RegisteringRemovedEventTask : public ITask
{
private:
    FileTreeStorage *fileTreeStorage;
    Observable<FileSystemEvent> *observable;
    TaskPerformer *taskPerformer;
    unsigned long millisecondsSleepPeriod;
    QMutex mutex;
    bool isPerformed;
    std::string path;
    long long timestamp;

public:
    RegisteringRemovedEventTask(FileTreeStorage *fileTreeStorage, Observable<FileSystemEvent> *observable, TaskPerformer *taskPerformer, unsigned long millisecondsSleepPeriod);
    void SetFields(const std::string &path, long long timestamp);
    const std::string& GetPath() const;
    void SetPerformed();
    bool IsPerformed();
    virtual void Perform();
    void DoWork();
};

#endif // FSEVENTSHANDLER_H
