/****************************************************************************
**
**
** This header file defines a class providng observing file system events
** in Mac OS X
**
**
****************************************************************************/

#ifndef MACFSCONNECTOR_H
#define MACFSCONNECTOR_H

#include <iostream>
#include <QThread>
#include <CoreServices/CoreServices.h>
#include "../Core/Observer.h"
#include "FileSystemEvent.h"
#include "FileTreeStorage.h"

class MacFSConnectorThread;

class MacFSConnector : public Observable<FileSystemEvent>
{
private:
    static std::auto_ptr<MacFSConnector> macFSConnector;
    MacFSConnectorThread *thread;
    MacFSConnector();

public:
    virtual ~MacFSConnector();
    void Run();
    void Stop();
    void SetWatchedPath(std::string path);
    void WaitWhileRunning();
    bool WaitWhileRunning(unsigned long milliseconds);  //return true if not running any more

    static MacFSConnector* GetConnector()
    {
        if(macFSConnector.get() == NULL)
            macFSConnector.reset(new MacFSConnector());
        return macFSConnector.get();
    }
};

class MacFSConnectorThread : public QThread
{
private:
    CFRunLoopRef runLoopRef;
    FSEventStreamRef streamRef;
    std::string wathedPath;
    std::auto_ptr<FileTreeStorage> fileTreeStorage;

protected:
    virtual void run();

public:
    MacFSConnectorThread();
    ~MacFSConnectorThread();
    void SetWathedPath(std::string& watchedPath);
    void Stop();
};

#endif // MACFSCONNECTOR_H
