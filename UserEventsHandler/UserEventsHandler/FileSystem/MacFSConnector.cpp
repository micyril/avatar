#include <unistd.h>
#include <sys/stat.h>
#include <QMutex>
#include "../Core/Tools.h"
#include "MacFSConnector.h"
#include "FSEventsHandler.h"

std::auto_ptr<MacFSConnector> MacFSConnector::macFSConnector(NULL);

static FSEventsHandler *fsEventsHandler;

void mycallback(ConstFSEventStreamRef streamRef, void *clientCallBackInfo, size_t numEvents,
                void *eventPaths, const FSEventStreamEventFlags eventFlags[], const FSEventStreamEventId eventIds[])
{
    char **paths = (char **)eventPaths;
    long long timespamp = Tools::getTimestamp();

    for (size_t i = 0; i < numEvents; i++)
    {
        std::string path = paths[i];
        fsEventsHandler->Handle(path, timespamp);
    }
}

void MacFSConnectorThread::run()
{
    FSEventStreamEventId latestEventIdBeforeInit = FSEventsGetCurrentEventId();
    fileTreeStorage.reset(new FileTreeStorage());
    fileTreeStorage->Init(wathedPath);

    std::cout << "Initialization has finished successfully" << std::endl;

    CFStringRef mypath = CFStringCreateWithCString(NULL, wathedPath.c_str(), kCFStringEncodingUTF8);
    CFArrayRef pathsToWatch = CFArrayCreate(NULL, (const void **)&mypath, 1, NULL);
    void *callbackInfo = NULL; // could put stream-specific data here.
    CFAbsoluteTime latency = 0.1; /* Latency in seconds */

    fsEventsHandler = new FSEventsHandler(fileTreeStorage.get(), MacFSConnector::GetConnector(), latency * 1000);

    /* Create the stream, passing in a callback */
    streamRef = FSEventStreamCreate(NULL, &mycallback, (FSEventStreamContext*) callbackInfo,
                                    pathsToWatch, latestEventIdBeforeInit, latency,
                                    kFSEventStreamCreateFlagNoDefer | kFSEventStreamCreateFlagFileEvents);

    runLoopRef = CFRunLoopGetCurrent();
    FSEventStreamScheduleWithRunLoop(streamRef, runLoopRef, kCFRunLoopDefaultMode);
    FSEventStreamStart(streamRef);

    CFRunLoopRun();
}

MacFSConnectorThread::MacFSConnectorThread() : fileTreeStorage(NULL) {}

MacFSConnectorThread::~MacFSConnectorThread()
{
    if(isRunning())
        Stop();
}

void MacFSConnectorThread::SetWathedPath(std::string &watchedPath)
{
    this->wathedPath = watchedPath;
}

void MacFSConnectorThread::Stop() {
    if(!isRunning())
        return;

    FSEventStreamStop(streamRef);
    FSEventStreamUnscheduleFromRunLoop(streamRef, runLoopRef, kCFRunLoopDefaultMode);
    FSEventStreamInvalidate(streamRef);
    FSEventStreamRelease(streamRef);
    CFRunLoopStop(runLoopRef);

    delete fsEventsHandler;
    fileTreeStorage.reset();
}

MacFSConnector::MacFSConnector() :
    thread(new MacFSConnectorThread()) { }

MacFSConnector::~MacFSConnector()
{
    delete thread;
}

void MacFSConnector::Run()
{
    thread->start();
}

void MacFSConnector::Stop()
{
    if(thread->isRunning())
    {
        thread->Stop();
        thread->wait();
    }
}

void MacFSConnector::SetWatchedPath(std::string path)
{
    thread->SetWathedPath(path);
}

void MacFSConnector::WaitWhileRunning()
{
    thread->wait();
}

bool MacFSConnector::WaitWhileRunning(unsigned long milliseconds)
{
    return thread->wait(milliseconds);
}
