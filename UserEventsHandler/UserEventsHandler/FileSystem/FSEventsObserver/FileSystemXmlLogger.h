/****************************************************************************
**
**
** This header file defines a class used to log to xml file system
** events derived from Observable object
**
**
****************************************************************************/

#ifndef FILESYSTEMXMLLOGGER_H
#define FILESYSTEMXMLLOGGER_H

#include <QXmlStreamWriter>
#include <QFile>
#include "../../Core/Observer.h"
#include "../FileSystemEvent.h"

class FileSystemXmlLogger : public Observer<FileSystemEvent>
{
private:
    QXmlStreamWriter xmlStreamWritter;
    QFile file;

public:
    FileSystemXmlLogger(QString fileName);
    virtual void Update(const FileSystemEvent &fileSystemEvent);
    ~FileSystemXmlLogger();
};

#endif // FILESYSTEMXMLLOGGER_H
