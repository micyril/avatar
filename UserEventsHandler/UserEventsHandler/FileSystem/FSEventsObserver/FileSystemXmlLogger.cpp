#include <iostream>
#include "../../Core/Exceptions.h"
#include "FileSystemXmlLogger.h"

void FileSystemXmlLogger::Update(const FileSystemEvent &fileSystemEvent)
{
    if(xmlStreamWritter.hasError())
    {
        std::string path = file.fileName().toUtf8().constData();
        throw CanNotWriteXmlException(path);
    }
    xmlStreamWritter.writeStartElement("FileSystemEvent");
    xmlStreamWritter.writeAttribute("timestamp", QString::number(fileSystemEvent.timestamp));
    xmlStreamWritter.writeAttribute("eventType", QString::number(fileSystemEvent.type));
    xmlStreamWritter.writeAttribute("path", QString::fromUtf8(fileSystemEvent.path.c_str()));

    if(fileSystemEvent.type == FileSystemEvent::Moved)
    {
        xmlStreamWritter.writeStartElement("MovedEvent");
        xmlStreamWritter.writeAttribute("newPath", QString::fromUtf8(((const MovedEvent&)fileSystemEvent).newPath.c_str()));
        xmlStreamWritter.writeEndElement();
    }

    xmlStreamWritter.writeEndElement();
}

FileSystemXmlLogger::FileSystemXmlLogger(const QString fileName) : file(fileName)
{
    bool fileExisted = file.exists();
    file.open(QIODevice::WriteOnly| QIODevice::Append | QIODevice::Text | QIODevice::Unbuffered);
    xmlStreamWritter.setDevice(&file);
    xmlStreamWritter.setAutoFormatting(true);

    if(!fileExisted)
        xmlStreamWritter.writeStartDocument();
}

FileSystemXmlLogger::~FileSystemXmlLogger()
{
    xmlStreamWritter.writeEndDocument();
    file.close();
}
