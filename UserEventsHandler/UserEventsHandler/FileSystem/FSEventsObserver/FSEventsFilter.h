/****************************************************************************
**
**
** This header file defines decoring classes providing filtering file system
** events
**
**
****************************************************************************/

#ifndef FSEVENTSFILTER_H
#define FSEVENTSFILTER_H

#include <list>
#include <QRegExp>
#include "FSEventsObserverDecorator.h"

class FSEventsFilter : public FSEventsObserverDecorator
{
public:
    virtual ~FSEventsFilter() {}
    FSEventsFilter(Observer<FileSystemEvent> *fsEventsObserver);
    virtual void Update(const FileSystemEvent &data);
    virtual bool Passes(const FileSystemEvent& event) = 0;
};


//Do not allow events which paths don't match regular expression
class AllowWhatAllowedFilter : public FSEventsFilter
{
private:
    QRegExp regExForAllowedPaths;

public:
    AllowWhatAllowedFilter(std::string regEx, Observer<FileSystemEvent> *fsEventsObserver = NULL);
    virtual bool Passes(const FileSystemEvent &event);
};

//Do not allow events which paths match regular expression
class AllowWhatNotRefusedFilter : public FSEventsFilter
{
private:
    QRegExp regExForRefusedPaths;

public:
    AllowWhatNotRefusedFilter(std::string regEx, Observer<FileSystemEvent> *fsEventsObserver = NULL);
    virtual bool Passes(const FileSystemEvent &event);
};

class CompositeFilter : public FSEventsFilter
{
protected:
    std::list<FSEventsFilter *> filters;

public:
    CompositeFilter(Observer<FileSystemEvent> *fsEventsObserver);
    void AddFilter(FSEventsFilter *filter);
};

//Allow events which pass in every added filter
class AndFilter : public CompositeFilter
{
public:
    AndFilter(Observer<FileSystemEvent> *fsEventsObserver = NULL);
    virtual bool Passes(const FileSystemEvent &event);
};

//Allow events which pass in at least one added filter
class OrFilter : public CompositeFilter
{
public:
    OrFilter(Observer<FileSystemEvent> *fsEventsObserver = NULL);
    virtual bool Passes(const FileSystemEvent &event);
};

#endif // FSEVENTSFILTER_H
