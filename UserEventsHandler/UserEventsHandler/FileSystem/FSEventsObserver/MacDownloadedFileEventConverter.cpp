#include "MacDownloadedFileEventConverter.h"

MacDownloadedFileEventConverter::MacDownloadedFileEventConverter(Observer<FileSystemEvent> *fsEventsObserver, std::string pathToDownloadsFolder) :
    FSEventsObserverDecorator(fsEventsObserver)
{
    QString regExBegin = QString::fromUtf8(pathToDownloadsFolder.c_str());
    QString regExForPathEnd = "/(.+).download/\\1";
    QString regExForNewPathEnd = "/(.+)";

    regExForPath.setPattern(regExBegin + regExForPathEnd);
    regExForNewPath.setPattern(regExBegin + regExForNewPathEnd);
}

void MacDownloadedFileEventConverter::Update(const FileSystemEvent &event)
{
    if(event.type == FileSystemEvent::Moved &&
       regExForPath.exactMatch(QString::fromUtf8(event.path.c_str())) &&
       regExForNewPath.exactMatch(QString::fromUtf8(((const MovedEvent &)event).newPath.c_str())) &&
       regExForPath.cap(1) == regExForNewPath.cap(1))
    {
        fsEventsObserver->Update(FileSystemEvent(FileSystemEvent::Created, ((const MovedEvent &)event).newPath, event.timestamp));
        return;
    }

    fsEventsObserver->Update(event);
}
