#ifndef MACDOWNLOADEDFILEEVENTCONVERTER_H
#define MACDOWNLOADEDFILEEVENTCONVERTER_H

/****************************************************************************
**
**
** This header file defines class used to convert MovedEvent with path
** /path/to/downloads/(filename).download/(filename) and new path
** /path/to/downloads/(filename) to CreatedEvent with path
** /path/to/downloads/(filename)
**
**
****************************************************************************/

#include <QRegExp>
#include "FSEventsObserverDecorator.h"

class MacDownloadedFileEventConverter : public FSEventsObserverDecorator
{
private:
    QRegExp regExForPath;
    QRegExp regExForNewPath;

public:
    MacDownloadedFileEventConverter(Observer<FileSystemEvent> *fsEventsObserver, std::string pathToDownloadsFolder);
    virtual void Update(const FileSystemEvent& event);
};

#endif // MACDOWNLOADEDFILEEVENTCONVERTER_H
