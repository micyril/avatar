/****************************************************************************
**
**
** This header file defines exceptions used in classes related to file system
** events observers
**
**
****************************************************************************/

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>

class FSEventsObserverNotDefinedException : public std::exception
{
private:
    const char *message;
public:
    FSEventsObserverNotDefinedException() : message("Can't call the method Update for NULL observer") {}
    virtual const char* what()
    {
        return message;
    }
};

#endif // EXCEPTIONS_H
