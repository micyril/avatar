/****************************************************************************
**
**
** This header file defines class to indicate using the design pattern
** Decorator for Observer<FileSystemEvent>
**
**
****************************************************************************/

#ifndef FSEVENTSOBSERVERDECORATORS_H
#define FSEVENTSOBSERVERDECORATORS_H

#include "../../Core/Observer.h"
#include "../FileSystemEvent.h"

class FSEventsObserverDecorator : public Observer<FileSystemEvent>
{
protected:
    Observer<FileSystemEvent> *fsEventsObserver;

public:
    virtual ~FSEventsObserverDecorator() {}
    FSEventsObserverDecorator(Observer<FileSystemEvent> *fsEventsObserver) :
        fsEventsObserver(fsEventsObserver) { }
};

#endif // FSEVENTSOBSERVERDECORATORS_H
