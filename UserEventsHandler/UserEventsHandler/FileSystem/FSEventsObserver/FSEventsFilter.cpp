#include "FSEventsFilter.h"
#include "Exceptions.h"

FSEventsFilter::FSEventsFilter(Observer<FileSystemEvent> *fsEventsObserver) :
    FSEventsObserverDecorator(fsEventsObserver) {}

void FSEventsFilter::Update(const FileSystemEvent &data)
{
    if(fsEventsObserver == NULL)
        throw FSEventsObserverNotDefinedException();
    if(Passes(data))
        fsEventsObserver->Update(data);
}

AllowWhatAllowedFilter::AllowWhatAllowedFilter(std::string regEx, Observer<FileSystemEvent> *fsEventsObserver) :
    FSEventsFilter(fsEventsObserver), regExForAllowedPaths(QString::fromUtf8(regEx.c_str())) {}

bool AllowWhatAllowedFilter::Passes(const FileSystemEvent &event)
{
    return regExForAllowedPaths.exactMatch(QString::fromUtf8(event.path.c_str()));
}

AllowWhatNotRefusedFilter::AllowWhatNotRefusedFilter(std::string regEx, Observer<FileSystemEvent> *fsEventsObserver) :
    FSEventsFilter(fsEventsObserver), regExForRefusedPaths(QString::fromUtf8(regEx.c_str())) {}

bool AllowWhatNotRefusedFilter::Passes(const FileSystemEvent &event)
{
    return !regExForRefusedPaths.exactMatch(QString::fromUtf8(event.path.c_str()));
}

CompositeFilter::CompositeFilter(Observer<FileSystemEvent> *fsEventsObserver) :
    FSEventsFilter(fsEventsObserver) {}

void CompositeFilter::AddFilter(FSEventsFilter *filter)
{
    filters.push_back(filter);
}

AndFilter::AndFilter(Observer<FileSystemEvent> *fsEventsObserver) :
    CompositeFilter(fsEventsObserver) {}

bool AndFilter::Passes(const FileSystemEvent &event)
{
    for(auto it = filters.begin(); it != filters.end(); it++)
        if(!(*it)->Passes(event))
            return false;

    return true;
}

OrFilter::OrFilter(Observer<FileSystemEvent> *fsEventsObserver) :
    CompositeFilter(fsEventsObserver) {}

bool OrFilter::Passes(const FileSystemEvent &event)
{
    for(auto it = filters.begin(); it != filters.end(); it++)
        if((*it)->Passes(event))
            return true;
    return false;
}
