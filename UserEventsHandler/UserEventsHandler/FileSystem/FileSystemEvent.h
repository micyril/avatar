/****************************************************************************
**
**
** This header file defines data types for file system events
**
**
****************************************************************************/

#ifndef FILESYSTEMEVENT_H
#define FILESYSTEMEVENT_H

#include <string>

class FileSystemEvent
{
public:
    enum EventType
    {
        Created = 1,
        Removed = 2,
        Moved = 3,
        Renamed = 4
    };

    virtual ~FileSystemEvent() {}

    EventType type;
    std::string path;
    long long timestamp;

    FileSystemEvent(EventType type, const std::string &path, long long timestamp) :
        type(type), path(path), timestamp(timestamp) {}
};

class MovedEvent : public FileSystemEvent
{
public:
    std::string newPath;

    MovedEvent(const std::string &path, const std::string &newPath, long long timestamp) :
        FileSystemEvent(FileSystemEvent::Moved, path, timestamp), newPath(newPath) {}
};

class RenamedEvent : public FileSystemEvent
{
public:
    std::string newName;

    RenamedEvent(const std::string &path, const std::string &newName, long long timestamp) :
        FileSystemEvent(FileSystemEvent::Renamed, path, timestamp), newName(newName) {}
};

#endif // FILESYSTEMEVENT_H
