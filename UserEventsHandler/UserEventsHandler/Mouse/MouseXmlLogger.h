/****************************************************************************
**
**
** This header file defines a class used to log to xml mouse events
**
**
****************************************************************************/

#ifndef MOUSEXMLLOGGER_H
#define MOUSEXMLLOGGER_H

#include <QXmlStreamWriter>
#include <QFile>
#include <exception>
#include "MouseEventObserver.h"
#include "../Windows/IWindowFinder.h"

class MouseXmlLogger : public MouseEventObserver
{
private:
    QXmlStreamWriter xmlStreamWritter;
    QFile file;
    IWindowFinder *windowsFinder;

protected:
    virtual void update(const MouseEvent &mouseEvent);

public:
    MouseXmlLogger(QString fileName, IWindowFinder *windowsFinder);
    virtual ~MouseXmlLogger();
};

#endif // MOUSEXMLLOGGER_H
