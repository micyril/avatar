#include "../Core/Exceptions.h"
#include "MouseXmlLogger.h"

MouseXmlLogger::MouseXmlLogger(QString fileName, IWindowFinder *windowsFinder) :
    file(fileName), windowsFinder(windowsFinder)
{
    bool fileExisted = file.exists();
    file.open(QIODevice::WriteOnly | QIODevice::Unbuffered | QIODevice::Append);
    xmlStreamWritter.setDevice(&file);
    xmlStreamWritter.setAutoFormatting(true);

    if(!fileExisted)
        xmlStreamWritter.writeStartDocument();
}

MouseXmlLogger::~MouseXmlLogger() {
    xmlStreamWritter.writeEndDocument();
    file.close();
}

void MouseXmlLogger::update(const MouseEvent &mouseEvent) {
    if(xmlStreamWritter.hasError())
    {
        std::string path = file.fileName().toUtf8().constData();
        throw CanNotWriteXmlException(path);
    }
    xmlStreamWritter.writeStartElement("MouseEvent");
    xmlStreamWritter.writeAttribute("timestamp", QString::number(mouseEvent.timestamp));
    xmlStreamWritter.writeAttribute("eventType", QString::number(mouseEvent.type));
    xmlStreamWritter.writeAttribute("x", QString::number(mouseEvent.x));
    xmlStreamWritter.writeAttribute("y", QString::number(mouseEvent.y));

    WindowInfo winInfo;
    if(windowsFinder->FindTopWindowContainingCoordinate(mouseEvent.x, mouseEvent.y, winInfo))
    {
        xmlStreamWritter.writeStartElement("Window");
        xmlStreamWritter.writeAttribute("id", QString::number(winInfo.id));
        xmlStreamWritter.writeAttribute("name", QString::fromUtf8(winInfo.name.c_str()));
        xmlStreamWritter.writeAttribute("applicationName", QString::fromUtf8(winInfo.applicationName.c_str()));
        xmlStreamWritter.writeStartElement("Rectangle");
        xmlStreamWritter.writeAttribute("x", QString::number(winInfo.bounds.x));
        xmlStreamWritter.writeAttribute("y", QString::number(winInfo.bounds.y));
        xmlStreamWritter.writeAttribute("width", QString::number(winInfo.bounds.width));
        xmlStreamWritter.writeAttribute("height", QString::number(winInfo.bounds.height));
        xmlStreamWritter.writeEndElement();
        xmlStreamWritter.writeEndElement();
    }

    xmlStreamWritter.writeEndElement();
}
