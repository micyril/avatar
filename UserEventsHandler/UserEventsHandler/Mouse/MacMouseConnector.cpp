#include <ApplicationServices/ApplicationServices.h>
#include "../Core/Tools.h"
#include "MacMouseConnector.h"

std::auto_ptr<MacMouseConnector> MacMouseConnector::macMouseConnector(NULL);

static CGEventRef myEventTapCallback (CGEventTapProxy proxy, CGEventType type, CGEventRef event, void * refcon)
{
    CGPoint mouseLocation;

    MouseEvent::EventType eventType;
    switch(type) {
        case kCGEventMouseMoved:
        case kCGEventLeftMouseDragged:
        case kCGEventRightMouseDragged:
            eventType = MouseEvent::MouseMove;
            break;
        case kCGEventLeftMouseDown:
            eventType = MouseEvent::LeftButtonPressed;
            break;
        case kCGEventLeftMouseUp:
            eventType = MouseEvent::LeftButtonReleased;
            break;
        case kCGEventRightMouseDown:
            eventType = MouseEvent::RightButtonPressed;
            break;
        case kCGEventRightMouseUp:
            eventType = MouseEvent::RightButtonReleased;
            break;
        default:
            eventType = MouseEvent::UnknownType;
            break;
    }

    mouseLocation = CGEventGetLocation(event);
    MouseEvent mouseEvent(eventType, mouseLocation.x, mouseLocation.y, Tools::getTimestamp());
    MacMouseConnector::GetConnector()->NotifyObservers(mouseEvent);

    // Pass on the event, we must not modify it anyway, we are a listener
    return event;
}

void MacMouseConnector::Run()
{
    CGEventMask emask;
    CFMachPortRef myEventTap;
    CFRunLoopSourceRef eventTapRLSrc;

    emask = CGEventMaskBit(kCGEventMouseMoved) | CGEventMaskBit(kCGEventLeftMouseDown) | CGEventMaskBit(kCGEventLeftMouseUp) |
            CGEventMaskBit(kCGEventRightMouseDown) | CGEventMaskBit(kCGEventRightMouseUp) | CGEventMaskBit(kCGEventLeftMouseDragged) |
            CGEventMaskBit(kCGEventRightMouseDragged);

    // Create the Tap
    myEventTap = CGEventTapCreate (
        kCGSessionEventTap, // Catch all events for current user session
        kCGTailAppendEventTap, // Append to end of EventTap list
        kCGEventTapOptionListenOnly, // We only listen, we don't modify
        emask,
        &myEventTapCallback,
        NULL // We need no extra data in the callback
    );

    // Create a RunLoop Source for it
    eventTapRLSrc = CFMachPortCreateRunLoopSource(
        kCFAllocatorDefault,
        myEventTap,
        0
    );

    // Add the source to the current RunLoop
    CFRunLoopAddSource(
        CFRunLoopGetCurrent(),
        eventTapRLSrc,
        kCFRunLoopDefaultMode
    );

    // Keep the RunLoop running forever
    CFRunLoopRun();
}
