/****************************************************************************
**
**
** This header file defines a data type for mouse events
**
**
****************************************************************************/

#ifndef MOUSEEVENT_H
#define MOUSEEVENT_H

class MouseEvent
{
public:
    enum EventType
    {
        MouseMove = 0x1,
        LeftButtonPressed = 0x2,
        LeftButtonReleased = 0x4,
        RightButtonPressed = 0x8,
        RightButtonReleased = 0x10,
        UnknownType = 0x20
    };

    EventType type;
    int x;
    int y;
    long long timestamp;

    MouseEvent(EventType type, int x, int y, long long timestamp) :
        type(type), x(x), y(y), timestamp(timestamp) {}
};

#endif // MOUSEEVENT_H
