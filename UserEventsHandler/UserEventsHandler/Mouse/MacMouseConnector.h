/****************************************************************************
**
**
** This header file defines a class providng observing mouse events in
** Mac OS X
**
**
****************************************************************************/

#ifndef MACMOUSECONNECTOR_H
#define MACMOUSECONNECTOR_H

#include "MouseEvent.h"
#include "../Core/Observer.h"
#include <iostream>

class MacMouseConnector : public Observable<MouseEvent>
{
private:
    static std::auto_ptr<MacMouseConnector> macMouseConnector;
    MacMouseConnector() { }

public:
    void Run();

    static MacMouseConnector* GetConnector()
    {
        if(macMouseConnector.get() == NULL)
            macMouseConnector.reset(new MacMouseConnector());
        return macMouseConnector.get();
    }
};

#endif // MACMOUSECONNECTOR_H
