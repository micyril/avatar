/****************************************************************************
**
**
** This header file defines a class providing observing just user-defined
** mouse events
**
**
****************************************************************************/

#ifndef IMOUSEEVENTOBSERVER_H
#define IMOUSEEVENTOBSERVER_H

#include "MouseEvent.h"
#include "../Core/Observer.h"

class MouseEventObserver : public Observer<MouseEvent>
{
private:
    int observableEventsMask = -1;

protected:
    virtual void update(const MouseEvent &mouseEvent) = 0;

public:
    virtual ~MouseEventObserver() {}

    void SetObservableEventsMask(int observableEventsMask)  // For example: parameter observableEventsMask can be
    {                                                       // MouseEvent::LeftButtonPressed | MouseEvent::LeftButtonReleased
        this->observableEventsMask = observableEventsMask;
    }

    virtual void Update(const MouseEvent& mouseEvent)
    {
        if(mouseEvent.type & observableEventsMask)
            update(mouseEvent);
    }
};

#endif // IMOUSEEVENTOBSERVER_H
