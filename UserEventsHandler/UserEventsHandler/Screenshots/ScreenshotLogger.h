/****************************************************************************
**
**
** This header file defines a class used to make screenshots when mouse
** mouse events occur
**
**
****************************************************************************/

#ifndef WINDOWSCREENSHOTLOGGER_H
#define WINDOWSCREENSHOTLOGGER_H

#include <string>
#include <QThread>
#include "../Mouse/MouseEventObserver.h"
#include "IScreenshotMacker.h"

class ScreenshotMakingThread;

class ScreenshotLogger : public MouseEventObserver
{
private:
    ScreenshotMakingThread *screanshotMakingThread;
    std::string prefixForLogsNames;

protected:
    virtual void update(const MouseEvent &mouseEvent);

public:
    ScreenshotLogger(IScreenshotMacker *screenshotMaker, std::string folder, std::string separator = "/");
    virtual ~ScreenshotLogger();
};

class ScreenshotMakingThread : public QThread {
private:
    std::string screenshotName;
    int xOfPointInsideWindow;
    int yOfPointInsideWindow;
    IScreenshotMacker *screenshotMaker;

protected:
    virtual void run();

public:
    ScreenshotMakingThread(IScreenshotMacker *screenshotMaker);
    void SetParameters(std::string &screenshotName, const int &xOfPointInsideWindow, const int &yOfPointInsideWindow);
};

#endif // WINDOWSCREENSHOTLOGGER_H
