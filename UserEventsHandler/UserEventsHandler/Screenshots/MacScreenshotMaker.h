/****************************************************************************
**
**
** This header file defines a class providing making screenshots in Mac OS X
**
**
****************************************************************************/

#ifndef MACSCREENSHOTMAKER_H
#define MACSCREENSHOTMAKER_H

#include "IScreenshotMacker.h"

class MacScreenshotMaker : public IScreenshotMacker
{
private:
    std::string command = "screencapture -x -t jpg ";
    std::string commandWithRect = "screencapture -x -t jpg -R";
    std::string rectParamsSeparator = ",";
    std::string paramsSeparator = " ";

public:
    virtual void Make(const std::string &fileName);
    virtual void Make(const std::string &fileName, const Rectangle &areaForCutting);
};

#endif // MACSCREENSHOTMAKER_H
