#include <sstream>
#include "MacScreenshotMaker.h"

void MacScreenshotMaker::Make(const std::string &fileName)
{
    std::stringstream stream;
    stream << command << fileName;
    system(stream.str().c_str());
}

void MacScreenshotMaker::Make(const std::string &fileName, const Rectangle &areaForCutting)
{
    std::stringstream stream;
    stream << commandWithRect << areaForCutting.x << rectParamsSeparator << areaForCutting.y << rectParamsSeparator <<
              areaForCutting.width << rectParamsSeparator << areaForCutting.height << paramsSeparator << fileName;

    system(stream.str().c_str());
}
