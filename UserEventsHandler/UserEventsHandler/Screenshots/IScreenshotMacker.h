/****************************************************************************
**
**
** This header file defines an interface providing making screenshots
**
**
****************************************************************************/

#ifndef ISCREENSHOTMACKER_H
#define ISCREENSHOTMACKER_H

#include <string>
#include "../Windows/Rectangle.h"

class IScreenshotMacker
{
public:
    virtual ~IScreenshotMacker() {}
    virtual void Make(const std::string &fileName) = 0;
    virtual void Make(const std::string &fileName, const Rectangle &areaForCutting) = 0;
};

#endif // ISCREENSHOTMACKER_H
