#include <QDir>
#include <ApplicationServices/ApplicationServices.h>
#include "ScreenshotLogger.h"

ScreenshotLogger::ScreenshotLogger(IScreenshotMacker *screenshotMaker, std::string folder, std::string separator) :
    screanshotMakingThread(new ScreenshotMakingThread(screenshotMaker)), prefixForLogsNames(folder + separator)
{
    QDir dir(folder.c_str());
    if (!dir.exists())
    {
        dir.mkpath(".");
    }
}

ScreenshotLogger::~ScreenshotLogger()
{
    delete screanshotMakingThread;
}

void ScreenshotLogger::update(const MouseEvent &mouseEvent)
{
    if(screanshotMakingThread->isRunning() == false)
    {
        std::string screenshotName(prefixForLogsNames + QString::number(mouseEvent.timestamp).toStdString());
        screanshotMakingThread->SetParameters(screenshotName, mouseEvent.x, mouseEvent.y);
        screanshotMakingThread->start();
    }
}


void ScreenshotMakingThread::run()
{
    screenshotMaker->Make(screenshotName);
}

ScreenshotMakingThread::ScreenshotMakingThread(IScreenshotMacker *screenshotMaker) :
    screenshotMaker(screenshotMaker) {}

void ScreenshotMakingThread::SetParameters(std::string &screenshotName, const int &xOfPointInsideWindow, const int &yOfPointInsideWindow)
{
    this->screenshotName = screenshotName;
    this->xOfPointInsideWindow = xOfPointInsideWindow;
    this->yOfPointInsideWindow = yOfPointInsideWindow;
}
