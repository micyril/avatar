/****************************************************************************
**
**
** This header file defines an interface providing finding the most top gui
** window containing an appropriate coordinates
**
**
****************************************************************************/

#ifndef IWINDOWFINDER_H
#define IWINDOWFINDER_H

#include "WindowInfo.h"

class IWindowFinder
{
public:
    virtual ~IWindowFinder() {}
    virtual bool FindTopWindowContainingCoordinate(int xCoord, int yCoord, WindowInfo &winInfo) = 0;
};

#endif // IWINDOWFINDER_H
