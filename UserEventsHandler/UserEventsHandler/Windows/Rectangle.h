/****************************************************************************
**
**
** This header file defines a data type descrbing rectangles
**
**
****************************************************************************/

#ifndef RECTANGLE_H
#define RECTANGLE_H

class Rectangle
{
public:
    int x;
    int y;
    int width;
    int height;

    Rectangle() {}

    Rectangle(int x, int y, int width, int height) :
        x(x), y(y), width(width), height(height) {}
};

#endif // RECTANGLE_H
