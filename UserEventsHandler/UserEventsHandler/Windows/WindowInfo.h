/****************************************************************************
**
**
** This header file defines a data type providing information about a gui
** window
**
**
****************************************************************************/

#ifndef WINDOWINFO_H
#define WINDOWINFO_H

#include <string>
#include "Rectangle.h"

class WindowInfo
{
public:
    int id;
    std::string name;
    std::string applicationName;
    Rectangle bounds;
};

#endif // WINDOWINFO_H
