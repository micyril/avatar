#include <ApplicationServices/ApplicationServices.h>
#include <string>
#include <iostream>
#include "MacWindowFinder.h"

bool CFStringCopyToStdString(CFStringRef aString, std::string &result) {
    if (aString == NULL)
        return false;
    CFIndex length = CFStringGetLength(aString);
    CFIndex maxSize = CFStringGetMaximumSizeForEncoding(length, kCFStringEncodingUTF8);
    char *buffer = (char *)malloc(maxSize);
    if (CFStringGetCString(aString, buffer, maxSize, kCFStringEncodingUTF8))
    {
        result = buffer;
        free(buffer);
        return true;
    }
    free(buffer);
    return false;
}

bool MacWindowFinder::FindTopWindowContainingCoordinate(int xCoord, int yCoord, WindowInfo &winInfo)
{
    CFArrayRef windowsArray = CGWindowListCopyWindowInfo(kCGWindowListOptionOnScreenOnly || kCGWindowListExcludeDesktopElements, kCGNullWindowID); // Get all windows
    CFIndex windowsCount = CFArrayGetCount(windowsArray);

    int winLayer;
    int winId;
    std::string winName;
    std::string winApplicationName;
    CGRect windowRect;
    for(CFIndex i = 0; i < windowsCount; i++)  //For each founded window
    {
        const CFDictionaryRef windowDictionary = (const CFDictionaryRef)CFArrayGetValueAtIndex(windowsArray, i);  //Get dictionary with information about window

        CFNumberRef layerRaw = (CFNumberRef)CFDictionaryGetValue(windowDictionary, kCGWindowLayer);  //Get information about the window layer in mac os type
        if(CFNumberGetValue(layerRaw, kCFNumberIntType, &winLayer) && winLayer < 0) //Convert the window layer to int and check it
            continue;

        if(CFDictionaryContainsKey(windowDictionary, kCGWindowName) &&
           CFStringCopyToStdString((CFStringRef)CFDictionaryGetValue(windowDictionary, kCGWindowName), winName))  //Check if the window has name
        {
            if(winName == "Dock" || winName == "Backstop Menubar")  //Check if the window is some of the systems windows
                continue;
        }
        else
            continue;

        const CFDictionaryRef windowBounds = (const CFDictionaryRef)CFDictionaryGetValue(windowDictionary, kCGWindowBounds);  //Get the window bounds in raw type
        if(CGRectMakeWithDictionaryRepresentation(windowBounds, &windowRect) && //Convert raw window bounds in rectangle
           windowRect.origin.x <= xCoord && windowRect.origin.y <= yCoord &&
           windowRect.origin.x + windowRect.size.width >= xCoord && windowRect.origin.y + windowRect.size.height >= yCoord)  //Check if the point coordinates are inside the window
        {
            SInt32 id32;
            CFNumberRef idRaw = (CFNumberRef)CFDictionaryGetValue(windowDictionary, kCGWindowNumber);  //Get information about the window id in mac os type
            if(CFNumberGetValue(idRaw, kCGWindowIDCFNumberType, &id32)) //Convert the window id to mac os 32 bit int
                winId = id32;
            else
                continue;

            if(!CFDictionaryContainsKey(windowDictionary, kCGWindowOwnerName) ||
               !CFStringCopyToStdString((CFStringRef)CFDictionaryGetValue(windowDictionary, kCGWindowOwnerName), winApplicationName))  //Check if the window has application name
                continue;

            winInfo.id = winId;
            winInfo.name = winName;
            winInfo.applicationName = winApplicationName;
            winInfo.bounds.x = (int)windowRect.origin.x;
            winInfo.bounds.y = (int)windowRect.origin.y;
            winInfo.bounds.width = (int)windowRect.size.width;
            winInfo.bounds.height = (int)windowRect.size.height;
            return true;
        }
    }

    return false;
}
