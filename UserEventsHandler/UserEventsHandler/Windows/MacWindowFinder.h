/****************************************************************************
**
**
** This header file defines a class providing finding the most top gui
** window containing an appropriate coordinates in Mac OS X
**
**
****************************************************************************/

#ifndef MACWINDOWFINDER_H
#define MACWINDOWFINDER_H

#include "IWindowFinder.h"

class MacWindowFinder : public IWindowFinder
{
public:
    virtual bool FindTopWindowContainingCoordinate(int xCoord, int yCoord, WindowInfo &winInfo);
};

#endif // MACWINDOWFINDER_H
